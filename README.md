# PyTechsas

Set of tools and python API use to parse and display Techsas file. Techsas files are netcdf time series datasets of scientific sensors recorded on vessels of the French Oceanographic Fleet

## Documentation about Techsas format
Documentation can be found in the docs subdirectory

## Deployment

pyTechsas project can be pip installable. Use the following commands (with gitlab-pytechsas pointing to package repository):

```
python -m build
python -m twine upload --repository gitlab-pytechsas dist/*
```


Note : id of server is defined in ~/.pypirc. An example of a pypirc file is given below :

````
[distutils]
index-servers =
	gitlab-pytechsas

[srv_conda_env]
repository = https://gitlab.ifremer.fr/api/v4/projects/fleet%2Fformats%2FpyTechsas/packages/pypi
username = Private-xxx
password = XXXXX
````





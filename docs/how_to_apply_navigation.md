### How to apply navigation from NVI (NetCDF navigation file (.nvi.nc))

### Install pyTechsas and pyNvi

```
pip install pytechsas --index-url https://gitlab.ifremer.fr/api/v4/projects/2127/packages/pypi/simple
pip install pynvi --extra-index-url https://gitlab.ifremer.fr/api/v4/projects/2736/packages/pypi/simple
```

### Apply navigation

```python
import pynvi.nvi_driver as nvi
from pytechsas.sensor.techsas_apply_navigation import apply_navigation

with nvi.open_nvi(NVI_FILE_PATH, filtered=True) as nvi_file:
    apply_navigation(techsas_file_path=TECHSAS_FILE_PATH, nav= nvi_file)
```

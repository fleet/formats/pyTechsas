# Peacetime sample dataset

The Techsas sample dataset is an extract of the Peacetime survey.
It can be downloaded on seanoe website : https://doi.org/10.17882/90052[10.17882/90052]

The Peacetime survey is an oceanographic cruise on the http://www.flotteoceanographique.fr/La-flotte/Navires/Navires-hauturiers/Pourquoi-pas[Pourquoi Pas?] done in 2017. Techsas data were processed with the Globe software to cut them and keep only a few values from 2017/06/10 11:20:00 to 2017/06/10 11:25:00. They serve as an example of a set of data files that can be archived by Techsas.

More details about the survey can be found here
https://doi.org/10.17600/17000300[10.17600/17000300]

## Content
The Techsas directory contains a set of subdirectories described below. Each subdirectory aggregate a coherent set of data of the same kind. Theses directories are described below and   details on the sensors can be found on Genavir's madida website https://madida.genavir.fr

[cols="25,~"]
|===
| sub directory | description
| ADCP
| This directory contains adcp data in a techsas file format. This function is now disable from techsas archiving services since ADCP data need post processing and can be downloaded at the NETCDF OceanSite file format on the SISMER website

| ATT |
This directory contains attitude data available on the ship. In this case these are Octans link:https://madida.genavir.fr/#/outil/detail/24/364[489] & link:https://madida.genavir.fr/#/outil/detail/25/371[495] and Phins link:https://madida.genavir.fr/#/outil/detail/412/2241[2197] & link:https://madida.genavir.fr/#/outil/detail/25/371[3457_108]

| DEPTH |
The depth directory contains data of single depth sensors used for vertical depth reference. The ship has a Kongsberg link:https://madida.genavir.fr/#/outil/detail/119/762[EA600] system with 3 transducer 12,38, and 200kHz. One file per transducer is generated and a file for the used reference depth is recorded too.

| ENG |
This folder contains data about the use and power of the 4 engines of the ship

| FLOW |
This folder contains data about the flow measure of the pump of the thermo salinometer (SBE 21)

| GPS |
This folder contains data recorded from all the GPS sensors

| GRAVI |
Contains KSS31 data set, for gravimetry processing details see Globe software

| GYR |
This folder contains gyrocompas (ie heading) data

|HSS|
This folder contains hull sound speed data

|INFO |
This contains informative data (alarm code, textual data ) about the winch of the CTD

| KHXDR |
This directory contains information about the transversal thrust power and rudder angles.

| LOCH |
This directory contains data of the Loch SKIPPER DL2

|MET |
This directory contains meteorological datasets anemometers, mercury station.


| NAV |
This directory contains dataset related to the Cinna system, containing synthetic navigation data for the

|  QAL & QCF |
These directories  contains Genavir Ifremer's quarto software configuration and alarms


| SUBNAV |
This directory contains subsea navigation data. Could be empty if no engine is currently diving

| THS |
This directory contains link:https://madida.genavir.fr/#/outil/detail/434/2490[thermosalinometer] data (SBE38 and SBE21)

| WINCH |
This directory contains all winches datasets
|===

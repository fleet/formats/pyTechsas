# pylint: skip-file

from dash import Dash, html, dcc, Input, Output
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import pandas as pd
import pyat.core.techsas.pytechsas.sensor.mgd77_driver as mgd_rd
import base64
import io


app = Dash(__name__)

record_list = ['ALL RECORD TYPE', 'LATITUDE', 'LONGITUDE', 'BATHYMETRY_CORRECTED_DEPTH', 'MAGNETICS_TOTAL_FIELD_1', 'OBSERVED_GRAVITY', 'FREE_AIR_ANOMALY']
mode = ['lines', 'markers', 'lines+markers']
display = ['same_graph', 'differente_graph']
colors = {
    'title': '#000000',
    'text': '#000000',
    'background': '#FFFFFF'
}

app.layout = html.Div(
    style={
        'backgroundColor': colors['background'],
        'margin': '0px'
    },
    children=[

# Header.
    html.H1(
        children='Tool to compare mgd77 files',
        style={'textAlign': 'center', 'color': colors['text']}
    ),

# First column.
    html.Div(

        children=[

            html.H3(
                children='Enter your mgd77 reference file here',
                style={'textAlign': 'center', 'color': colors['text']}
            ),

            dcc.Upload(
                id="upload-data-ref",
                children=html.Div(["Drag and Drop or ", html.A("Select File")]),
                style={
                    "width": "700px",
                    "height": "60px",
                    "lineHeight": "60px",
                    "borderWidth": "1px",
                    "borderStyle": "dashed",
                    "borderRadius": "15px",
                    "textAlign": "center",
                    "margin": "15px"
                },
                multiple=False,
            ),

            html.H3(
                children='Enter your mgd77 file here',
                style={'textAlign': 'center', 'color': colors['text']}
            ),

            dcc.Upload(
                id="upload-data",
                children=html.Div(["Drag and Drop or ", html.A("Select File")]),
                style={
                    "width": "700px",
                    "height": "60px",
                    "lineHeight": "60px",
                    "borderWidth": "1px",
                    "borderStyle": "dashed",
                    "borderRadius": "15px",
                    "textAlign": "center",
                    "margin-left": "15px"
                },
                multiple=False,
            ),

        ],

        style={
            'display': 'inline-block',
            'vertical-align': 'top'
        }

    ),

# Second column.
    html.Div(

        children=[

            html.H3(
                children='Select the type of record you want to compare',
                style={'textAlign': 'center', 'color': colors['text']}
            ),

            dcc.Dropdown(value=record_list, id='datatype-dropdown', placeholder="Select a record type",
                style =dict(width="300px", height="30px", margin="15px")
            ),

            html.H3(
                children='Select the type of style you want for the graph',
                style={'textAlign': 'center', 'color': colors['text']}
            ),

            dcc.Dropdown(value=mode, id='mode-dropdown', placeholder="Select a style mode",
                style={
                    "width": "300px",
                    "height": "30px",
                    "margin": "15px",
                }
            ),

            html.H3(
                children='Select the type of graph display you want',
                style={'textAlign': 'center', 'color': colors['text']}
            ),

            dcc.Dropdown(value=display, id='graph-dropdown', placeholder="Select a graph display mode",
                         style={
                             "width": "300px",
                             "height": "30px",
                             "margin": "15px",
                }
            ),
        ],

        style={
                    'display': 'inline-block',
                    'vertical-align': 'top',
                    'margin-left': '100px'
                }
    ),

    dcc.Graph(id="Mygraph-1",
        style={
            "height": "100%",
            "width": "95%",
        },
    ),

])


@app.callback(
    Output("Mygraph-1", "figure"),
    [Input("upload-data-ref", "contents"), Input("upload-data-ref", "filename")],
    [Input("upload-data", "contents"), Input("upload-data", "filename")],
    Input('datatype-dropdown', 'value'),
    Input('mode-dropdown', 'value'),
    Input('graph-dropdown', 'value')
)
def update_graph(upload_data_ref_contents, upload_data_ref_filename, upload_data_contents, upload_data_filename,
                 datatype_dropdown_value, mode_dropdown_value, graph_dropdown_value):

    if datatype_dropdown_value != 'ALL RECORD TYPE':

        if graph_dropdown_value == 'differente_graph':

            fig = make_subplots(rows=2, cols=2,
                                  shared_yaxes=True, shared_xaxes='all',
                                specs=[[{}, {}], [{"colspan": 2}, None]])
            fig.update_layout(xaxis_showticklabels=True, xaxis2_showticklabels=True)
            if upload_data_contents:
                df2 = parse_data(upload_data_contents, upload_data_filename)
                fig.add_trace(go.Scattergl(x=df2['DATE_TIME'], y=df2[datatype_dropdown_value], mode=mode_dropdown_value, name='MGD77'),
                              row=1, col=1)
            if upload_data_ref_contents:
                df1 = parse_data(upload_data_ref_contents, upload_data_ref_filename)
                fig.add_trace(go.Scattergl(x=df1['DATE_TIME'], y=df1[datatype_dropdown_value], mode=mode_dropdown_value, name='Reference MGD77'),
                              row=1, col=2)
            if upload_data_contents and upload_data_ref_contents:
                fig.add_trace(go.Scattergl(x=df2['DATE_TIME'], y=df1[datatype_dropdown_value]-df2[datatype_dropdown_value], mode=mode_dropdown_value, name='Difference between both MGD77'),
                                  row=2, col=1)
            else:
                fig = px.scatter()
            fig.update_layout(width=1800, height=900)

        elif graph_dropdown_value == 'same_graph':

            fig = make_subplots(rows=2, cols=1, shared_xaxes='all')
            fig.update_layout(xaxis_showticklabels=True, xaxis2_showticklabels=True)
            if upload_data_contents:
                df2 = parse_data(upload_data_contents, upload_data_filename)
                fig.add_trace(go.Scattergl(x=df2['DATE_TIME'], y=df2[datatype_dropdown_value], mode=mode_dropdown_value, name='MGD77'),
                              row=1, col=1)
            if upload_data_ref_contents:
                df1 = parse_data(upload_data_ref_contents, upload_data_ref_filename)
                fig.add_trace(go.Scattergl(x=df1['DATE_TIME'], y=df1[datatype_dropdown_value], mode=mode_dropdown_value, name='Reference MGD77'),
                              row=1, col=1)
            if upload_data_contents and upload_data_ref_contents:
                df1[datatype_dropdown_value] = df1[datatype_dropdown_value] - df2[datatype_dropdown_value]
                fig.add_trace(go.Scattergl(x=df1['DATE_TIME'], y=df1[datatype_dropdown_value], mode=mode_dropdown_value, name='Difference between both MGD77'),
                                  row=2, col=1)
            else:
                fig = px.scatter()
            fig.update_layout(width=1800, height=900)

        else:
            fig = px.scatter()

    else:

        if graph_dropdown_value == 'differente_graph':
            fig = make_subplots(rows=2 * (len(record_list)-1), cols=2,
                                shared_xaxes='all',)
            if upload_data_ref_contents:
                df1 = parse_data(upload_data_ref_contents, upload_data_ref_filename)
            if upload_data_contents:
                df2 = parse_data(upload_data_contents, upload_data_filename)
            for i in range(1, len(record_list)):
                if upload_data_contents:
                    fig.add_trace(go.Scattergl(x=df2['DATE_TIME'], y=df2[record_list[i]], mode=mode_dropdown_value,
                                               name=record_list[i] + 'MGD77'),
                                  row=2*i-1, col=1)
                    fig.update_xaxes(title_text="Mgd77 " + record_list[i], row=2*i-1, col=1)
                if upload_data_ref_contents:
                    fig.add_trace(go.Scattergl(x=df1['DATE_TIME'], y=df1[record_list[i]], mode=mode_dropdown_value,
                                               name='Reference MGD77'),
                                  row=2*i, col=1)
                    fig.update_xaxes(title_text="Reference mgd77 " + record_list[i], row=2*i, col=1)
                if upload_data_contents and upload_data_ref_contents:
                    fig.add_trace(
                        go.Scattergl(x=df2['DATE_TIME'], y=df1[record_list[i]] - df2[record_list[i]],
                                     mode=mode_dropdown_value, name=record_list[i] + 'Difference between both MGD77'),
                        row=2*i-1, col=2)
                    fig.update_xaxes(title_text="Difference " + record_list[i], row=2*i-1, col=2)

            fig.update_layout(xaxis_showticklabels=True, width=1800, height=900 * (len(record_list)-1),)

        elif graph_dropdown_value == 'same_graph':

            fig = make_subplots(rows=2 * (len(record_list)-1), cols=1, shared_xaxes='all')
            if upload_data_ref_contents:
                df1 = parse_data(upload_data_ref_contents, upload_data_ref_filename)
            if upload_data_contents:
                df2 = parse_data(upload_data_contents, upload_data_filename)
            for i in range(1, len(record_list)):
                if upload_data_contents:
                    fig.add_trace(go.Scattergl(x=df2['DATE_TIME'], y=df2[record_list[i]], mode=mode_dropdown_value,
                                               name=record_list[i] + 'MGD77'),
                                  row=2*i-1, col=1)

                    fig.update_xaxes(title_text="Reference and mgd77 " + record_list[i], row=2*i-1, col=1)
                if upload_data_ref_contents:
                    fig.add_trace(go.Scattergl(x=df1['DATE_TIME'], y=df1[record_list[i]], mode=mode_dropdown_value,
                                               name=record_list[i] + 'Reference MGD77'),
                                  row=2*i-1, col=1)

                    fig.update_xaxes(title_text="Reference and mgd77 " + record_list[i], row=2*i-1, col=1)
                if upload_data_contents and upload_data_ref_contents:
                    fig.add_trace(go.Scattergl(x=df1['DATE_TIME'], y=df1[record_list[i]] - df2[record_list[i]],
                                               mode=mode_dropdown_value, name=record_list[i] + 'Difference between both MGD77'),
                                  row=2*i, col=1)

                    fig.update_xaxes(title_text="Difference " + record_list[i], row=2*i, col=1)

            fig.update_layout(xaxis_showticklabels=True, width=1800, height=900 * (len(record_list)-1),)
        else:
            fig = px.scatter()
    return fig


def parse_data(contents, filename):
    content_type, content_string = contents.split(",")
    decoded = base64.b64decode(content_string)

    if ".mgd77" in filename:
        # Assume that the user uploaded a mgd77 file
        df = mgd_rd.read_mgd77_as_df(io.StringIO(decoded.decode("ISO-8859-1")))
    else:
        df = pd.DataFrame()

    return df


if __name__ == '__main__':
    app.run_server(debug=True)

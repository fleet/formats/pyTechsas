"""
ConvertToMGD77
"""
import os
from datetime import datetime
from typing import List

from . import mgd77_driver
from .mgd77_constants import Mgd77Header


class ConvertToMGD77:
    """Converter to MGB77 format"""

    def __init__(
        self,
        i_paths: List[str],
        o_path: str,
        overwrite: bool = False,
        survey_identifier: str = None,
        data_center_file_number: str = None,
        parameters_surveyed_code: str = None,
        source_institution: str = None,
        country: str = None,
        platform_name: str = None,
        platform_type_code: str = None,
        platform_type: str = None,
        chief_scientist: str = None,
        project: str = None,
        funding: str = None,
        survey_departure_date: str = None,
        port_of_departure: str = None,
        survey_arrival_date: str = None,
        port_of_arrival: str = None,
        navigation_instrumentation: str = None,
        position_determination_method: str = None,
        bathymetry_instrumentation: str = None,
        additional_form_of_bathymetric_data: str = None,
        magnetics_instrumentation: str = None,
        additional_form_of_magnetics_data: str = None,
        gravity_instrumentation: str = None,
        additional_form_of_gravity_data: str = None,
        seismic_instrumentation: str = None,
        formats_of_seismic_data: str = None,
        general_digitizing_rate_of_bathymetry: str = None,
        general_sampling_rate_of_bathymetry: str = None,
        assumed_sound_velocity: str = None,
        bathymetric_datum_code: str = None,
        interpolation_scheme: str = None,
        general_digitizing_rate_of_magnetics: str = None,
        general_sampling_rate_of_magnetics: str = None,
        magnetics_tow_sensor_distance: str = None,
        sensor_depth: str = None,
        horizontal_sensor_separation: str = None,
        reference_field_code: str = None,
        reference_field: str = None,
        method_of_applying_residual_field: str = None,
        general_digitizing_rate_of_gravity: str = None,
        general_sampling_rate_of_gravity: str = None,
        theoretical_gravity_formula_code: str = None,
        theoretical_gravity_formula: str = None,
        reference_system_code: str = None,
        reference_system: str = None,
        correction_applied: str = None,
        departure_base_station_gravity: str = None,
        departure_base_station_description: str = None,
        arrival_base_station_gravity: str = None,
        arrival_base_station_description: str = None,
        number_of_10_degree_identifiers: str = None,
        ten_degree_identifier: str = "",
        sequence18: str = None,
        sequence19: str = None,
        sequence20: str = None,
        sequence21: str = None,
        sequence22: str = None,
        sequence23: str = None,
        sequence24: str = None,
        #    monitor: ProgressMonitor = DefaultMonitor,
    ):
        """
        Constructor.
        """
        self.i_paths: List[str] = i_paths
        self.o_path: str = o_path
        self.overwrite: bool = overwrite
        self.header_dic: dict = {
            Mgd77Header.SURVEY_IDENTIFIER.name: survey_identifier,
            Mgd77Header.DATA_CENTER_FILE_NUMBER.name: data_center_file_number,
            Mgd77Header.PARAMETERS_SURVEYED_CODE.name: parameters_surveyed_code,
            Mgd77Header.SOURCE_INSTITUTION.name: source_institution,
            Mgd77Header.COUNTRY.name: country,
            Mgd77Header.PLATFORM_NAME.name: platform_name,
            Mgd77Header.PLATFORM_TYPE_CODE.name: platform_type_code,
            Mgd77Header.PLATFORM_TYPE.name: platform_type,
            Mgd77Header.CHIEF_SCIENTIST.name: chief_scientist,
            Mgd77Header.PROJECT.name: project,
            Mgd77Header.FUNDING.name: funding,
            Mgd77Header.SURVEY_DEPARTURE_DATE.name: datetime.strptime(
                survey_departure_date, "%Y-%m-%dT%H:%M:%SZ"
            ).strftime("%Y%m%d"),
            Mgd77Header.PORT_OF_DEPARTURE.name: port_of_departure,
            Mgd77Header.SURVEY_ARRIVAL_DATE.name: datetime.strptime(survey_arrival_date, "%Y-%m-%dT%H:%M:%SZ").strftime(
                "%Y%m%d"
            ),
            Mgd77Header.PORT_OF_ARRIVAL.name: port_of_arrival,
            Mgd77Header.NAVIGATION_INSTRUMENTATION.name: navigation_instrumentation,
            Mgd77Header.POSITION_DETERMINATION_METHOD.name: position_determination_method,
            Mgd77Header.BATHYMETRY_INSTRUMENTATION.name: bathymetry_instrumentation,
            Mgd77Header.ADDITIONAL_FORMS_OF_BATHYMETRY_DATA.name: additional_form_of_bathymetric_data,
            Mgd77Header.MAGNETICS_INSTRUMENTATION.name: magnetics_instrumentation,
            Mgd77Header.ADDITIONAL_FORMS_OF_MAGNETICS_DATA.name: additional_form_of_magnetics_data,
            Mgd77Header.GRAVITY_INSTRUMENTATION.name: gravity_instrumentation,
            Mgd77Header.ADDITIONAL_FORMS_OF_GRAVITY_DATA.name: additional_form_of_gravity_data,
            Mgd77Header.SEISMIC_INSTRUMENTATION.name: seismic_instrumentation,
            Mgd77Header.FORMATS_OF_SEISMIC_DATA.name: formats_of_seismic_data,
            Mgd77Header.GENERAL_DIGITIZING_RATE_OF_BATHYMETRY.name: general_digitizing_rate_of_bathymetry,
            Mgd77Header.GENERAL_SAMPLING_RATE_OF_BATHYMETRY.name: general_sampling_rate_of_bathymetry,
            Mgd77Header.ASSUMED_SOUND_VELOCITY.name: assumed_sound_velocity,
            Mgd77Header.BATHYMETRICS_DATUM_CODE.name: bathymetric_datum_code,
            Mgd77Header.INTERPOLATION_SCHEME.name: interpolation_scheme,
            Mgd77Header.GENERAL_DIGITIZING_RATE_OF_MAGNETICS.name: general_digitizing_rate_of_magnetics,
            Mgd77Header.GENERAL_SAMPLING_RATE_OF_MAGNETICS.name: general_sampling_rate_of_magnetics,
            Mgd77Header.MAGNETICS_SENSOR_TOW_DISTANCE.name: magnetics_tow_sensor_distance,
            Mgd77Header.SENSOR_DEPTH.name: sensor_depth,
            Mgd77Header.HORIZONTAL_SENSOR_SEPARATION.name: horizontal_sensor_separation,
            Mgd77Header.REFERENCE_FIELD_CODE.name: reference_field_code,
            Mgd77Header.REFERENCE_FIELD.name: reference_field,
            Mgd77Header.METHOD_OF_APPLYING_RESIDUAL_FIELD.name: method_of_applying_residual_field,
            Mgd77Header.GENERAL_DIGITIZING_RATE_OF_GRAVITY.name: general_digitizing_rate_of_gravity,
            Mgd77Header.GENERAL_SAMPLING_RATE_OF_GRAVITY.name: general_sampling_rate_of_gravity,
            Mgd77Header.THEORETICAL_GRAVITY_FORMULA_CODE.name: theoretical_gravity_formula_code,
            Mgd77Header.THEORETICAL_GRAVITY_FORMULA.name: theoretical_gravity_formula,
            Mgd77Header.REFERENCE_SYSTEM_CODE.name: reference_system_code,
            Mgd77Header.REFERENCE_SYSTEM.name: reference_system,
            Mgd77Header.CORRECTIONS_APPLIED.name: correction_applied,
            Mgd77Header.DEPARTURE_BASE_STATION_GRAVITY.name: departure_base_station_gravity,
            Mgd77Header.DEPARTURE_BASE_STATION_DESCRIPTION.name: departure_base_station_description,
            Mgd77Header.ARRIVAL_BASE_STATION_GRAVITY.name: arrival_base_station_gravity,
            Mgd77Header.ARRIVAL_BASE_STATION_DESCRIPTION.name: arrival_base_station_description,
            Mgd77Header.NUMBER_OF_10_DEGREE_IDENTIFIERS.name: number_of_10_degree_identifiers,
            Mgd77Header.TEN_DEGREE_IDENTIFIER_SEQUENCE_16.name: ten_degree_identifier[:75],
            Mgd77Header.TEN_DEGREE_IDENTIFIER_SEQUENCE_17.name: ten_degree_identifier[75:],
            Mgd77Header.SEQUENCE_18.name: sequence18,
            Mgd77Header.SEQUENCE_19.name: sequence19,
            Mgd77Header.SEQUENCE_20.name: sequence20,
            Mgd77Header.SEQUENCE_21.name: sequence21,
            Mgd77Header.SEQUENCE_22.name: sequence22,
            Mgd77Header.SEQUENCE_23.name: sequence23,
            Mgd77Header.SEQUENCE_24.name: sequence24,
        }

    def __call__(self) -> None:
        # Copy input file to output path"
        if not self.overwrite and os.path.exists(self.o_path):
            print(f"File {self.o_path} already exists, skipping it")
        elif os.path.exists(self.o_path):
            os.remove(self.o_path)

        mgd77_driver.export_techsas_to_mgd77(
            techsas_files=self.i_paths, output_path=self.o_path, header_dict=self.header_dic
        )

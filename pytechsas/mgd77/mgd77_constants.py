from datetime import date
from enum import Enum
from typing import NamedTuple, Callable

import numpy as np
import pandas as pd

NAN_VALUE = (
    999999  # during write operation, nan values has been replaced by this one to allow the use of write_formatter
)

"""
HEADER
"""


class Mgd77HeaderRecord(NamedTuple):
    row: int  # Ligne correspondante au type de donnée
    length: int = None  # Longueur en caractère de la donnée
    desc: str = None  # Description de la donnée
    set_value: str = None  # Valeur de la donnée si elle reste constante
    write_formatter: Callable = str  # Format de la donnée


class Mgd77Header(Mgd77HeaderRecord, Enum):
    """
    Enumeration of MGD77 descriptor in header
    """

    RECORD_TYPE = Mgd77HeaderRecord(
        1,
        1,
        set_value="4",
        desc="""RECORD TYPE Set to "4" (Header)""",
    )

    SURVEY_IDENTIFIER = Mgd77HeaderRecord(
        1,
        8,
        desc="""SURVEY IDENTIFIER Identifier supplied by the contributing organization, else given by NGDC in a manner
        which represents the data. Identical to that in data record.""",
        write_formatter=lambda value: f"{value:0>8}" if value is not None else f'{"":8}',
    )

    FORMAT_ACRONYM = Mgd77HeaderRecord(
        1,
        5,
        set_value="MGD77",
        desc="""FORMAT ACRONYM Set to "MGD77" """,
    )

    DATA_CENTER_FILE_NUMBER = Mgd77HeaderRecord(
        1,
        8,
        desc="""DATA CENTER FILE NUMBER
                                            Survey identifier bestowed by the data
                                            center. First 2 chars indicate the
                                            source, first 4 indicate platform.""",
        write_formatter=lambda value: f"{int(value):8}" if value is not None else f'{"":8}',
    )

    BLANK_SPACE_1 = Mgd77HeaderRecord(
        1, 4, desc="""Just a blank space in MGD77 format""", write_formatter=lambda value: f'{"":4}'
    )

    PARAMETERS_SURVEYED_CODE = Mgd77HeaderRecord(
        1,
        5,
        desc="""PARAMETERS SURVEYED CODE Status of geophysical parameters for this survey.""",
        write_formatter=lambda value: f"{int(value):05}" if value is not None else f'{"":5}',
    )

    FILE_CREATION_DATE = Mgd77HeaderRecord(
        1,
        8,
        set_value=date.today().strftime("%Y%m%d"),
        desc="""FILE CREATION DATE (YYYYMMDD)
                                            Date data records were last
                                            altered (including century).""",
    )

    SOURCE_INSTITUTION = Mgd77HeaderRecord(
        1,
        39,
        desc="""SOURCE INSTITUTION
                                            Organization which collected the
                                            data. Include contributor if different
                                            from collector.""",
        write_formatter=lambda value: f"{{:39}}".format(value) if value is not None else f'{"":39}',
    )

    COUNTRY = Mgd77HeaderRecord(
        2,
        18,
        desc="""COUNTRY""",
        write_formatter=lambda value: f"{{:18}}".format(value) if value is not None else f'{"":18}',
    )

    PLATFORM_NAME = Mgd77HeaderRecord(
        2,
        21,
        desc="""PLATFORM NAME""",
        write_formatter=lambda value: f"{{:21}}".format(value) if value is not None else f'{"":21}',
    )

    PLATFORM_TYPE_CODE = Mgd77HeaderRecord(
        2,
        1,
        desc="""PLATFORM TYPE CODE
                                            0 - Unspecified
                                            1 - Surface ship
                                            2 - Submersible ship
                                            3 - Aircraft
                                            4 - Buoy
                                            5 - Mobile land
                                            6 - Fixed land
                                            7 - Deep tow
                                            8 - Anchored seafloor instrument
                                            9 - Other, specify""",
        write_formatter=lambda value: f"{int(value):1}" if value is not None else f'{"":1}',
    )

    PLATFORM_TYPE = Mgd77HeaderRecord(
        2,
        6,
        desc="""PLATFORM TYPE(e.g., "SHIP","PLANE", "SUB", etc.)""",
        write_formatter=lambda value: f"{{:6}}".format(value) if value is not None else f'{"":6}',
    )

    CHIEF_SCIENTIST = Mgd77HeaderRecord(
        2,
        32,
        desc="""CHIEF SCIENTIST(S)""",
        write_formatter=lambda value: f"{{:32}}".format(value) if value is not None else f'{"":32}',
    )

    PROJECT = Mgd77HeaderRecord(
        3,
        58,
        desc="""PROJECT(e.g., "SURVOPS 6-69","INDOPAC, Leg3")""",
        write_formatter=lambda value: f"{{:58}}".format(value) if value is not None else f'{"":58}',
    )

    FUNDING = Mgd77HeaderRecord(
        3,
        20,
        desc="""FUNDING(i.e. agency or institution)""",
        write_formatter=lambda value: f"{{:20}}".format(value) if value is not None else f'{"":20}',
    )

    SURVEY_DEPARTURE_DATE = Mgd77HeaderRecord(
        4,
        8,
        desc="""SURVEY DEPARTURE DATE (YYYYMMDD)""",
        write_formatter=lambda value: f"{value:8}" if value is not None else f'{"":8}',
    )

    PORT_OF_DEPARTURE = Mgd77HeaderRecord(
        4,
        32,
        desc="""PORT OF DEPARTURE(i.e. city, country)""",
        write_formatter=lambda value: f"{{:32}}".format(value) if value is not None else f'{"":32}',
    )

    SURVEY_ARRIVAL_DATE = Mgd77HeaderRecord(
        4,
        8,
        desc="""SURVEY ARRIVAL DATE (YYYYMMDD)""",
        write_formatter=lambda value: f"{value:8}" if value is not None else f'{"":8}',
    )

    PORT_OF_ARRIVAL = Mgd77HeaderRecord(
        4,
        30,
        desc="""PORT OF ARRIVAL (i.e. city, country)""",
        write_formatter=lambda value: f"{{:30}}".format(value) if value is not None else f'{"":30}',
    )

    NAVIGATION_INSTRUMENTATION = Mgd77HeaderRecord(
        5,
        40,
        desc="""NAVIGATION INSTRUMENTATION (e.g. "SAT/LORAN A/SEXTANT")""",
        write_formatter=lambda value: f"{{:40}}".format(value) if value is not None else f'{"":40}',
    )

    POSITION_DETERMINATION_METHOD = Mgd77HeaderRecord(
        5,
        38,
        desc="""GEODETIC DATUM/POSITION DETERMINATION METHOD (e.g. "WGS84/PRIM - SATELLITE, SEC-LORAN A")""",
        write_formatter=lambda value: f"{{:38}}".format(value) if value is not None else f'{"":38}',
    )

    BATHYMETRY_INSTRUMENTATION = Mgd77HeaderRecord(
        6,
        40,
        desc="""BATHYMETRY INSTRUMENTATION (Include information such as frequency, beam width, and sweep speed of recorder.""",
        write_formatter=lambda value: f"{{:40}}".format(value) if value is not None else f'{"":40}',
    )

    ADDITIONAL_FORMS_OF_BATHYMETRY_DATA = Mgd77HeaderRecord(
        6,
        38,
        desc="""COUNTRYADDITIONAL FORMS OF BATHYMETRIC DATA (e.g., "MICROFILM","ANALOG RECORDS")""",
        write_formatter=lambda value: f"{{:38}}".format(value) if value is not None else f'{"":38}',
    )

    MAGNETICS_INSTRUMENTATION = Mgd77HeaderRecord(
        7,
        40,
        desc="""MAGNETICS INSTRUMENTATION(e.g., "PROTON PRECESSION MAG-GEOMETRICS G-801")""",
        write_formatter=lambda value: f"{{:40}}".format(value) if value is not None else f'{"":40}',
    )

    ADDITIONAL_FORMS_OF_MAGNETICS_DATA = Mgd77HeaderRecord(
        7,
        38,
        desc="""ADDITIONAL FORMS OF MAGNETICS DATA(e.g., "PUNCH TAPE","ANALOG RECORDS")""",
        write_formatter=lambda value: f"{{:38}}".format(value) if value is not None else f'{"":38}',
    )

    GRAVITY_INSTRUMENTATION = Mgd77HeaderRecord(
        8,
        40,
        desc="""GRAVITY INSTRUMENTATION(e.g., "L and R S-26")""",
        write_formatter=lambda value: f"{{:40}}".format(value) if value is not None else f'{"":40}',
    )

    ADDITIONAL_FORMS_OF_GRAVITY_DATA = Mgd77HeaderRecord(
        8,
        38,
        desc="""ADDITIONAL FORMS OF GRAVITY DATA(e.g., "MICROFILM", "ANALOG RECORDS")""",
        write_formatter=lambda value: f"{{:38}}".format(value) if value is not None else f'{"":38}',
    )

    SEISMIC_INSTRUMENTATION = Mgd77HeaderRecord(
        9,
        40,
        desc="""SEISMIC INSTRUMENTATION Include the size of the sound source, the recording frequency filters, and the
        number of channels (e.g., "1700 cu. in., AIRGUN, 8-62 Hz, 36 CHANNELS")""",
        write_formatter=lambda value: f"{{:40}}".format(value) if value is not None else f'{"":40}',
    )

    FORMATS_OF_SEISMIC_DATA = Mgd77HeaderRecord(
        9,
        38,
        desc="""FORMATS OF SEISMIC DATA(e.g., "DIGITAL", "MICROFILM", NEGATIVES", etc.)""",
        write_formatter=lambda value: f"{{:38}}".format(value) if value is not None else f'{"":38}',
    )

    FORMAT_TYPE = Mgd77HeaderRecord(
        10,
        1,
        set_value="A",
        desc="""FORMAT TYPE Set to "A", which means format contains integers, floating points, and alphanumerics""",
    )

    FORMAT_DESCRIPTION_SEQUENCE_10 = Mgd77HeaderRecord(
        10,
        74,
        set_value="(I1,A8,F5.2,4I2,F5.3,F8.5,F9.5,I1,F6.4,F6.1,I2,I1,3F6.1,I1,F5.1,F6.0,F7.1,",
        desc="""FORMAT DESCRIPTION
                                            This is one method of reading
                                            (not writing) the data in FORTRAN.
                                            Set to the following:
                                            "(I1,A8,F5.2,4I2,F5.3,F8.5,F9.5,I1,F6.4,
                                            F6.1,I2,I1,3F6.1,I1,F5.1,F6.0,F7.1,"
                                            (NOTE: continued in sequence no. 11)""",
    )

    BLANK_SPACE_2 = Mgd77HeaderRecord(
        10, 3, desc="""Just a blank space in MGD77 format""", write_formatter=lambda value: f'{"":3}'
    )

    FORMAT_DESCRIPTION_SEQUENCE_11 = Mgd77HeaderRecord(
        11,
        17,
        set_value="F6.1,F5.1,A5,A6,I1)",
        desc="""FORMAT DESCRIPTION Continued, set to following: "F6.1,F5.1,A5,A6,I1)" """,
    )

    BLANK_SPACE_3 = Mgd77HeaderRecord(
        11, 21, desc="""Just a blank space in MGD77 format""", write_formatter=lambda value: f'{"":21}'
    )

    TOP_MOST_LATITUDE_OF_SURVEY = Mgd77HeaderRecord(
        11,
        3,
        desc="""TOPMOST LATITUDE OF SURVEY **(to next whole degree)""",
        write_formatter=lambda value: f"{int(value):0=+3}" if value is not None else f'{"":3}',
    )

    BOTTOM_MOST_LATITUDE_OF_SURVEY = Mgd77HeaderRecord(
        11,
        3,
        desc="""BOTTOMMOST LATITUDE""",
        write_formatter=lambda value: f"{int(value):0=+3}" if value is not None else f'{"":3}',
    )

    LEFT_MOST_LONGITUDE_OF_SURVEY = Mgd77HeaderRecord(
        11,
        4,
        desc="""LEFTMOST LONGITUDE""",
        write_formatter=lambda value: f"{int(value):0=+4}" if value is not None else f'{"":4}',
    )

    RIGHT_MOST_LONGITUDE_OF_SURVEY = Mgd77HeaderRecord(
        11,
        4,
        desc="""RIGHTMOST LONGITUDE""",
        write_formatter=lambda value: f"{int(value):0=+4}" if value is not None else f'{"":4}',
    )

    BLANK_SPACE_4 = Mgd77HeaderRecord(
        11, 24, desc="""Just a blank space in MGD77 format""", write_formatter=lambda value: f'{"":24}'
    )

    GENERAL_DIGITIZING_RATE_OF_BATHYMETRY = Mgd77HeaderRecord(
        12,
        3,
        desc="""GENERAL DIGITIZING RATE OF BATHYMETRY
                                            In tenths of minutes.
                                            The rate which is present within
                                            the data records (e.g., if values were
                                            coded every 5 minutes, set to "050")""",
        write_formatter=lambda value: f"{float(value) * 10:3.0f}" if value is not None else f'{"":3}',
    )

    GENERAL_SAMPLING_RATE_OF_BATHYMETRY = Mgd77HeaderRecord(
        12,
        12,
        desc="""GENERAL SAMPLING RATE OF BATHYMETRY This rate is instrumentation dependent (e.g., "1/SECOND")""",
        write_formatter=lambda value: f"{{:12}}".format(value) if value is not None else f'{"":12}',
    )

    ASSUMED_SOUND_VELOCITY = Mgd77HeaderRecord(
        12,
        5,
        desc="""ASSUMED SOUND VELOCITY
                                            In tenths of meters per second.
                                            Historically, in the U.S., this
                                            speed has been 800 fathoms/sec,
                                            which equals 1463.0 meters/sec.;
                                            however, some recorders have a
                                            calibration of 1500 meters/sec
                                            (e.g., "14630")""",
        write_formatter=lambda value: f"{float(value) * 10:5.0f}" if value is not None else f'{"":5}',
    )

    BATHYMETRICS_DATUM_CODE = Mgd77HeaderRecord(
        12,
        2,
        desc="""BATHYMETRIC DATUM CODE
                                            00 - No correction applied (sea level)
                                            01 - Lowest normal low water
                                            02 - Mean lower low water
                                            03 - Lowest low water
                                            04 - Mean lower low water spring
                                            05 - Indian spring low water
                                            06 - Mean low water spring
                                            07 - Mean sea level
                                            08 - Mean low water
                                            09 - Equatorial spring low water
                                            10 - Tropic lower low water
                                            11 - Lowest astronomical tide
                                            88 - Other, specify in
                                            additional documentation""",
        write_formatter=lambda value: f"{int(value):02}" if value is not None else f'{"":2}',
    )

    INTERPOLATION_SCHEME = Mgd77HeaderRecord(
        12,
        56,
        desc="""INTERPOLATION SCHEME
                                            This field allows for a description
                                            of the interpolation scheme used,
                                            should some of the data records contain
                                            interpolated values (e.g., "5-MINUTE
                                            INTERVALS AND PEAKS AND TROUGHS").""",
        write_formatter=lambda value: f"{{:56}}".format(value) if value is not None else f'{"":56}',
    )

    GENERAL_DIGITIZING_RATE_OF_MAGNETICS = Mgd77HeaderRecord(
        13,
        3,
        desc="""GENERAL DIGITIZING RATE OF MAGNETICS
                                            In tenths of minutes.
                                            The rate which is present
                                            within the data records.""",
        write_formatter=lambda value: f"{float(value) * 10:3.0f}" if value is not None else f'{"":3}',
    )

    GENERAL_SAMPLING_RATE_OF_MAGNETICS = Mgd77HeaderRecord(
        13,
        2,
        desc="""GENERAL SAMPLING RATE OF MAGNETICS
                                            In seconds.
                                            This rate isinstrumentation dependent
                                            (e.g., if the pulse rate is every 3 sec,
                                            set to "03")""",
        write_formatter=lambda value: f"{int(value):02}" if value is not None else f'{"":2}',
    )

    MAGNETICS_SENSOR_TOW_DISTANCE = Mgd77HeaderRecord(
        13,
        4,
        desc="""MAGNETIC SENSOR TOW DISTANCE
                                            In meters.
                                            The distance from the navigation
                                            reference to the leading sensor.""",
        write_formatter=lambda value: f"{int(value):4}" if value is not None else f'{"":4}',
    )

    SENSOR_DEPTH = Mgd77HeaderRecord(
        13,
        5,
        desc="""SENSOR DEPTH In tenths of meters. This is the estimated depth of the lead magnetic sensor.""",
        write_formatter=lambda value: f"{float(value) * 10:5.0f}" if value is not None else f'{"":5}',
    )

    HORIZONTAL_SENSOR_SEPARATION = Mgd77HeaderRecord(
        13,
        3,
        desc="""HORIZONTAL SENSOR SEPARATION In meters. If two sensors are used.""",
        write_formatter=lambda value: f"{int(value):3}" if value is not None else f'{"":3}',
    )

    REFERENCE_FIELD_CODE = Mgd77HeaderRecord(
        13,
        2,
        desc="""REFERENCE FIELD CODE
                                            This is the reference field used to determine
                                            the residual magnetics:
                                            00 - Unused
                                            01 - AWC 70
                                            02 - AWC 75
                                            03 - IGRF-65
                                            04 - IGRF-75
                                            05 - GSFC-1266
                                            06 - GSFC (POGO) 0674
                                            07 - UK 75
                                            08 - POGO 0368
                                            09 - POGO 1068
                                            10 - POGO 0869
                                            11 - IGRF-80
                                            12 - IGRF-85
                                            13 - IGRF-90
                                            88 - Other, specify""",
        write_formatter=lambda value: f"{int(value):02}" if value is not None else f'{"":2}',
    )

    REFERENCE_FIELD = Mgd77HeaderRecord(
        13,
        12,
        desc="""REFERENCE FIELD(e.g., "IGRF-85")""",
        write_formatter=lambda value: f"{{:12}}".format(value) if value is not None else f'{"":12}',
    )

    METHOD_OF_APPLYING_RESIDUAL_FIELD = Mgd77HeaderRecord(
        13,
        47,
        desc="""METHOD OF APPLYING RESIDUAL FIELD
                                            The procedure used in applying
                                            this reduction to the data
                                            (e.g., "LINEAR INTERP. in
                                            60-mile SQUARE")""",
        write_formatter=lambda value: f"{{:47}}".format(value) if value is not None else f'{"":47}',
    )

    GENERAL_DIGITIZING_RATE_OF_GRAVITY = Mgd77HeaderRecord(
        14,
        3,
        desc="""GENERAL DIGITIZING RATE OF GRAVITY In tenths of minutes. The rate present within the data records""",
        write_formatter=lambda value: f"{float(value) * 10:3.0f}" if value is not None else f'{"":3}',
    )

    GENERAL_SAMPLING_RATE_OF_GRAVITY = Mgd77HeaderRecord(
        14,
        2,
        desc="""GENERAL SAMPLING RATE OF GRAVITY In seconds. This rate is instrumentation dependent.
                                                    If recordingis continuous, set to "00" """,
        write_formatter=lambda value: f"{int(value):02}" if value is not None else f'{"":2}',
    )

    THEORETICAL_GRAVITY_FORMULA_CODE = Mgd77HeaderRecord(
        14,
        1,
        desc="""THEORETICAL GRAVITY FORMULA CODE
                                            1 - Heiskanen 1924
                                            2 - International 1930
                                            3 - IAG System 1967
                                            4 - IAG System 1980
                                            8 - Other, specify""",
        write_formatter=lambda value: f"{int(value):1}" if value is not None else f'{"":1}',
    )

    THEORETICAL_GRAVITY_FORMULA = Mgd77HeaderRecord(
        14,
        17,
        desc="""THEORETICAL GRAVITY FORMULA (e.g., "INTERNATIONAL '30", "IAG SYSTEM (1967)", etc.)""",
        write_formatter=lambda value: f"{{:17}}".format(value) if value is not None else f'{"":17}',
    )

    REFERENCE_SYSTEM_CODE = Mgd77HeaderRecord(
        14,
        1,
        desc="""REFERENCE SYSTEM CODE Identifies the reference field:
                1 - Local system, specify
                2 - Potsdam system
                3 - System IGSN 71
                9 - Other, specify""",
        write_formatter=lambda value: f"{int(value):1}" if value is not None else f'{"":1}',
    )

    REFERENCE_SYSTEM = Mgd77HeaderRecord(
        14,
        16,
        desc="""REFERENCE SYSTEM (e.g., "POTSDAM SYSTEM","SYSTEM IGSN 71", etc.)""",
        write_formatter=lambda value: f"{{:16}}".format(value) if value is not None else f'{"":16}',
    )

    CORRECTIONS_APPLIED = Mgd77HeaderRecord(
        14,
        38,
        desc="""CORRECTIONS APPLIED Drift, tare and bias corrections applied. (e.g., "+0.075 MGAL PER DAY")""",
        write_formatter=lambda value: f"{{:38}}".format(value) if value is not None else f'{"":38}',
    )

    DEPARTURE_BASE_STATION_GRAVITY = Mgd77HeaderRecord(
        15,
        7,
        desc="""DEPARTURE BASE STATION GRAVITY In tenths of milligals. At sea level (Network value preferred.)""",
        write_formatter=lambda value: f"{float(value) * 10:7.0f}" if value is not None else f'{"":7}',
    )

    DEPARTURE_BASE_STATION_DESCRIPTION = Mgd77HeaderRecord(
        15,
        33,
        desc="""DEPARTURE BASE STATION DESCRIPTION
                                            Indicates name and number of station""",
        write_formatter=lambda value: f"{{:33}}".format(value) if value is not None else f'{"":33}',
    )

    ARRIVAL_BASE_STATION_GRAVITY = Mgd77HeaderRecord(
        15,
        7,
        desc="""ARRIVAL BASE STATION GRAVITY
                                            In tenths of milligals.
                                            At sea level (Network value preferred.)""",
        write_formatter=lambda value: f"{float(value) * 10:7.0f}" if value is not None else f'{"":7}',
    )

    ARRIVAL_BASE_STATION_DESCRIPTION = Mgd77HeaderRecord(
        15,
        31,
        desc="""ARRIVAL BASE STATION DESCRIPTION
                                            Indicates name and number of station""",
        write_formatter=lambda value: f"{{:31}}".format(value) if value is not None else f'{"":31}',
    )

    NUMBER_OF_10_DEGREE_IDENTIFIERS = Mgd77HeaderRecord(
        16,
        2,
        desc="""NUMBER OF 10-DEGREE IDENTIFIERS **
                                            This is the number of 4-digit
                                            10-degree identifiers, excluding
                                            the "9999" flag, which will
                                            follow this field. (see APPENDIX B)""",
        write_formatter=lambda value: f"{int(value):<2}" if value is not None else f'{"":2}',
    )

    BLANK_SPACE_5 = Mgd77HeaderRecord(
        16, 1, desc="""Just a blank space in MGD77 format""", write_formatter=lambda value: f'{"":1}'
    )

    TEN_DEGREE_IDENTIFIER_SEQUENCE_16 = Mgd77HeaderRecord(
        16,
        75,
        desc="""10-DEGREE IDENTIFIERS This is a
                                            series of 4-digit codes,separated by
                                            commas, which identify the 10-degree
                                            squares through which the survey
                                            collected data (see APPENDIX B).
                                            Code "9999" after last identifier.""",
        write_formatter=lambda value: f"{value:<75}" if value is not None else f'{"":75}',
    )

    TEN_DEGREE_IDENTIFIER_SEQUENCE_17 = Mgd77HeaderRecord(
        17,
        75,
        desc="""10-DEGREE IDENTIFIERS Continued""",
        write_formatter=lambda value: f"{value:<75}" if value is not None else f'{"":75}',
    )

    BLANK_SPACE_6 = Mgd77HeaderRecord(
        17, 3, desc="""Just a blank space in MGD77 format""", write_formatter=lambda value: f'{"":3}'
    )

    SEQUENCE_18 = Mgd77HeaderRecord(
        18,
        78,
        desc="""ADDITIONAL DOCUMENTATION; information concerning this survey not contained in header fields.""",
        write_formatter=lambda value: f"{{:78}}".format(value) if value is not None else f'{"":78}',
    )

    SEQUENCE_19 = Mgd77HeaderRecord(
        19,
        78,
        desc="""ADDITIONAL DOCUMENTATION; information concerning this survey not contained in header fields.""",
        write_formatter=lambda value: f"{{:78}}".format(value) if value is not None else f'{"":78}',
    )

    SEQUENCE_20 = Mgd77HeaderRecord(
        20,
        78,
        desc="""ADDITIONAL DOCUMENTATION; information concerning this survey not contained in header fields.""",
        write_formatter=lambda value: f"{{:78}}".format(value) if value is not None else f'{"":78}',
    )

    SEQUENCE_21 = Mgd77HeaderRecord(
        21,
        78,
        desc="""ADDITIONAL DOCUMENTATION; information concerning this survey not contained in header fields.""",
        write_formatter=lambda value: f"{{:78}}".format(value) if value is not None else f'{"":78}',
    )

    SEQUENCE_22 = Mgd77HeaderRecord(
        22,
        78,
        desc="""ADDITIONAL DOCUMENTATION; information concerning this survey not contained in header fields.""",
        write_formatter=lambda value: f"{{:78}}".format(value) if value is not None else f'{"":78}',
    )

    SEQUENCE_23 = Mgd77HeaderRecord(
        23,
        78,
        desc="""ADDITIONAL DOCUMENTATION; information concerning this survey not contained in header fields.""",
        write_formatter=lambda value: f"{{:78}}".format(value) if value is not None else f'{"":78}',
    )

    SEQUENCE_24 = Mgd77HeaderRecord(
        24,
        78,
        desc="""ADDITIONAL DOCUMENTATION; information concerning this survey not contained in header fields.""",
        write_formatter=lambda value: f"{{:78}}".format(value) if value is not None else f'{"":78}',
    )


class Mgd77DataRecord(NamedTuple):
    """
    This class represents a field of MGD77 data line.
    """

    start: int
    end: int = None
    desc: str = None
    default: str = None
    read_formatter: Callable[[str], str] = int
    write_formatter: Callable = str


class Mgd77Data(Mgd77DataRecord, Enum):
    """
    Enumeration of MGD77 data record.
    """

    DATA_RECORD_TYPE = Mgd77DataRecord(
        1,
        1,
        desc="Data record type : set to '5' for data record.",
        write_formatter=lambda value: f"{value:01.0f}" if value != NAN_VALUE else "9",
    )

    SURVEY_IDENTIFIER = Mgd77DataRecord(
        2,
        9,
        desc="Survey identifier supplied by the contributing organization, else given by NGDC in a manner which "
        "represents the data.",
        write_formatter=lambda value: f"{value:08}" if value != NAN_VALUE else "99999999",
    )

    TIME_ZONE_CORRECTION = Mgd77DataRecord(
        10,
        12,
        desc="""Corrects time (in characters 13-27) to GMT when added: equals zero when time is GMT.
                            Timezone normally falls between -13 and +12 inclusively.""",
        write_formatter=lambda value: f"{value:+03.0f}" if value != NAN_VALUE else "+00",
    )

    DATE_TIME = Mgd77DataRecord(
        13,
        27,
        desc="Date time : YYYYMMDD + HOUR + MINUTES x 1000",
        # parse MGD77 datetime (YYYYMMDD + HOUR + MINUTES x 1000)
        read_formatter=lambda date: pd.to_datetime(
            date[:-3] + str(int(int(date[-3:]) / 1000 * 60)), format="%Y%m%d%H%M%S"
        ),
        write_formatter=lambda date_time: date_time.strftime("%Y%m%d%H")
        + f"{(date_time.minute + date_time.second) * 1000:05}",
    )

    LATITUDE = Mgd77DataRecord(
        28,
        35,
        desc="Latitude x 100000; + = North; - = South; Between -9000000 and 9000000",
        read_formatter=lambda lat_str: int(lat_str) / 100_000 if lat_str != "+9999999" else np.nan,
        write_formatter=lambda lat: f"{lat * 100_000:+08.0f}" if lat != NAN_VALUE else "+9999999",
    )

    LONGITUDE = Mgd77DataRecord(
        36,
        44,
        desc="Longitude x 100000; + = East; - = West; Between -18000000 and 18000000",
        read_formatter=lambda lon_str: int(lon_str) / 100_000 if lon_str != "+99999999" else np.nan,
        write_formatter=lambda lon: f"{lon * 100_000:+09.0f}" if lon != NAN_VALUE else "+99999999",
    )

    POSITION_TYPE_CODE = Mgd77DataRecord(
        45,
        45,
        desc="Position type code : indicates how lat/lon was obtained: "
        "1 = Observed fix; 3 = Interpolated; 9 = Unspecified",
        write_formatter=lambda value: f"{value:01.0f}" if value != NAN_VALUE else "9",
    )

    BATHYMETRY_2_WAY_TRAVELTIME = Mgd77DataRecord(
        46,
        51,
        desc="""Bathymetry 2 way traveltime in ten-thousandths of seconds. Corrected for transducer depth and other
        such corrections, especially in shallow water""",
        read_formatter=lambda sting_value: int(sting_value) / 100 if sting_value != "999999" else np.nan,
        write_formatter=lambda value: f"{value * 100:06.0f}" if value != NAN_VALUE else "999999",
    )

    BATHYMETRY_CORRECTED_DEPTH = Mgd77DataRecord(
        52,
        57,
        "Bathymetry corrected depth in tenths of meters.",
        read_formatter=lambda sting_value: int(sting_value) / 10 if sting_value != "999999" else np.nan,
        write_formatter=lambda value: f"{value * 10:06.0f}" if value != NAN_VALUE else "999999",
    )

    BATHYMETRIC_CORRECTION_CODE = Mgd77DataRecord(
        58,
        59,
        desc="""Bathymetric correction code : details the procedure used for determining the sound velocity correction to depth:
                                    01-55 Matthews' Zones with zone;
                                    59 Matthews' Zones, no zone;
                                    60 S. Kuwahara Formula;
                                    61 Wilson Formula;
                                    62 Del Grosso Formula;
                                    63 Carter's Tables;
                                    88 Other (see Add. Doc.);
                                    99 Unspecified""",
        write_formatter=lambda value: str(value) if value != NAN_VALUE else "99",
    )  # why not 99 ?

    BATHYMETRIC_TYPE_CODE = Mgd77DataRecord(
        60,
        60,
        """Bathymetric type code : indicates how the data record's bathymetric value was obtained:
                                    1 = Observed;
                                    3 = Interpolated (Header Seq. 12);
                                    9 = Unspecified""",
        write_formatter=lambda value: f"{value:01.0f}" if value != NAN_VALUE else "9",
    )

    MAGNETICS_TOTAL_FIELD_1 = Mgd77DataRecord(
        61,
        66,
        desc="""Magnetics total field for 1st sensor in tenths of nanoteslas (gammas).
                                For leading sensor. Use this field for single sensor.""",
        read_formatter=lambda sting_value: int(sting_value) / 10 if sting_value != "999999" else np.nan,
        write_formatter=lambda value: f"{value * 10:06.0f}" if value != NAN_VALUE else "999999",
    )

    MAGNETICS_TOTAL_FIELD_2 = Mgd77DataRecord(
        67,
        72,
        desc="Magnetics total field for 2nd sensor in tenths of nanoteslas (gammas). For trailing sensor.",
        read_formatter=lambda sting_value: int(sting_value) / 10 if sting_value != "999999" else np.nan,
        write_formatter=lambda value: f"{value * 10:06.0f}" if value != NAN_VALUE else "999999",
    )

    MAGNETICS_RESIDUAL_FIELD = Mgd77DataRecord(
        73,
        78,
        desc="Magnetics residual field In tenths of nanoteslas (gammas). The reference field used is in Header Seq. 13.",
        read_formatter=lambda sting_value: int(sting_value) / 10 if sting_value != "999999" else np.nan,
        write_formatter=lambda value: f"{value * 10:06.0f}" if value != NAN_VALUE else "999999",
    )

    SENSOR_FOR_RESIDUAL_FIELD = Mgd77DataRecord(
        79,
        79,
        """Sensor for residual field :
                                    1 = 1st or leading sensor;
                                    2 = 2nd or trailing sensor;
                                    9 = Unspecified""",
        write_formatter=lambda value: f"{value:01.0f}" if value != NAN_VALUE else "9",
    )

    MAGNETICS_DIURNAL_CORRECTION = Mgd77DataRecord(
        80,
        84,
        desc="""Magnetics diurnal correction in tenths of nanoteslas (gammas). (In nanoteslas)
                    if 9-filled (i.e., set to +9999), total and residual fields are assumed to be uncorrected;
                    if used, total and residuals are assumed to have been already corrected.""",
        read_formatter=lambda sting_value: int(sting_value) / 10 if sting_value != "+9999" else np.nan,
        write_formatter=lambda value: f"{value * 10:+04.0f}" if value != NAN_VALUE else "+9999",
    )

    DEPTH_OR_ALTITUDE_OF_MAGNETICS_SENSOR = Mgd77DataRecord(
        85,
        90,
        desc="Depth or altitude of magnetics sensor in meters. + = Below sealevel - = Above sealevel",
        read_formatter=lambda sting_value: int(sting_value) if sting_value != "+99999" else np.nan,
        write_formatter=lambda value: f"{value:+06.0f}" if value != NAN_VALUE else "+99999",
    )

    OBSERVED_GRAVITY = Mgd77DataRecord(
        91,
        97,
        desc="Observed gravitry in tenths of milligals. Corrected for Eotvos, drift, and tares.",
        read_formatter=lambda sting_value: (
            int(sting_value) / 10 if sting_value not in ("9999999", "+999999") else np.nan
        ),
        write_formatter=lambda value: f"{value * 10:07.0f}" if value != NAN_VALUE else "9999999",
    )

    EOTVOS_CORRECTION = Mgd77DataRecord(
        98,
        103,
        "Eotvos correciton in tenths of milligals. E = 7.5 V cos phi sin alpha + 0.0042 V*V",
        read_formatter=lambda sting_value: (int(sting_value) / 10) if sting_value != "999999" else np.nan,
        write_formatter=lambda value: f"{value * 10:+06.0f}" if value != NAN_VALUE else "999999",
    )

    FREE_AIR_ANOMALY = Mgd77DataRecord(
        104,
        108,
        "Free air anomaly in tenths of milligals. Free-air Anomaly = G(observed) - G(theoretical)",
        read_formatter=lambda sting_value: int(sting_value) / 10 if sting_value not in ("99999", "+9999") else np.nan,
        write_formatter=lambda value: f"{value * 10:+05.0f}" if value != NAN_VALUE else "99999",
    )

    SEISMIC_LINE_NUMBER = Mgd77DataRecord(
        109,
        113,
        "Seismic line number used for cross referencing with seismic data.",
        read_formatter=lambda sting_value: int(sting_value) if sting_value != "99999" else np.nan,
        write_formatter=lambda value: str(value) if value != NAN_VALUE else "99999",
    )

    SEISMIC_SHOT_POINT_NUMBER = Mgd77DataRecord(
        114,
        119,
        "Seismic shot point number.",
        read_formatter=lambda sting_value: int(sting_value) if sting_value != "999999" else np.nan,
        write_formatter=lambda value: f"{value:06.0f}" if value != NAN_VALUE else "999999",
    )

    QUALITY_CODE_FOR_NAVIGATION = Mgd77DataRecord(
        120,
        120,
        """Quality code for navigation :
                                        5 = Suspected, by the originating institution;
                                        6 = Suspected, by the data center;
                                        9 = No identifiable problem	found""",
        write_formatter=lambda value: f"{value:01.0f}" if value != NAN_VALUE else "9",
    )

import logging as log
import math
import re
from typing import List

import numpy as np
import pandas as pd
import xarray as xr

from pytechsas.mgd77.mgd77_constants import Mgd77Header, Mgd77Data
from pytechsas.sensor.techsas_constant import (
    TECHSAS_LONGITUDE,
    TECHSAS_LATITUDE,
    TECHSAS_TIME,
    TECHSAS_STATUS,
    TECHSAS_MAG,
)
from pytechsas.sensor.techsas_gravi_process import GRAVITY_NAME, EOTVOS_CORRECTION_NAME, FREE_AIR_ANOMALY_NAME
from pytechsas.sensor.techsas_mag_process import MAGNETIC_ANOMALY_NAME

logger = log.getLogger()

# Constants fields
GRAVI_EXTENSION = ".gravi.nc"
MAG_EXTENSION = ".mag.nc"
DEPTH_EXTENSION = ".depth.nc"


def read_mgd77_as_df(mgd77_file: str) -> pd.DataFrame:
    """
    Reads an MGD77 file and return an Pandas Dataframe
    :param mgd77_file: input MGD77 file
    :return:Pandas Dataframe with MGD77 data
    """
    # get column names, positions and converters from Mgd77Data enum
    col_names = []
    col_specs = []
    converters = {}
    for record in Mgd77Data:
        col_names.append(record.name)
        col_specs.append((record.start - 1, record.end))
        converters[record.name] = record.read_formatter
    # read with pandas
    df = pd.read_fwf(
        mgd77_file,
        colspecs=col_specs,
        header=None,
        skiprows=24,
        names=col_names,
        converters=converters,
        encoding="latin1",
    )
    return df


def read_mgd77(mgd77_file: str) -> xr.Dataset:
    """
    Reads an MGD77 file and return an Xarray Dataset
    :param mgd77_file: input MGD77 file
    :return: Xarray Dataset with MGD77 data
    """
    df = read_mgd77_as_df(mgd77_file)
    # convert to xarray
    ds = xr.Dataset.from_dataframe(df)
    # index by datetime
    ds = ds.set_index(index=[Mgd77Data.DATE_TIME.name])
    ds = ds.rename(index=Mgd77Data.DATE_TIME.name)
    return ds


def export_techsas_to_mgd77(techsas_files: List[str], output_path: str, header_dict: dict) -> None:
    """
    Exports TECHSAS files to MGD77 files.
    :param techsas_files: input TECHSAS files
    :param output_path: output path
    :param header_dict: MGD77 header parameters
    """
    ds = create_mgd77_ds(techsas_files, period="60S")
    write_mgd77(input_ds=ds, output_file_path=output_path, header_dict=header_dict)


def write_mgd77(input_ds: xr.Dataset, header_dict: dict, output_file_path: str) -> None:
    """
    Writes a new MGD77 file.
    :param input_ds: input MGD77 dataset
    :param header_dict: MGD77 header parameters
    :param output_file_path: output path
    """
    logger.info(f"Write MGD77 file : {output_file_path}...")
    # column name & formatters
    columns = []
    formatters = {}
    nan_arrray = np.full(input_ds.dims[Mgd77Data.DATE_TIME.name], np.nan)
    for record in Mgd77Data:
        columns.append(record.name)
        formatters[record.name] = record.write_formatter
        # fill dataset with missing array filled with NaN
        if record.name not in input_ds:
            input_ds[record.name] = xr.DataArray(data=nan_arrray, dims=input_ds.dims)

    # convert to pandas dataframe
    df = input_ds.to_dataframe()
    df[Mgd77Data.DATE_TIME.name] = input_ds[Mgd77Data.DATE_TIME.name].to_pandas()
    top_latitude = math.ceil(df[Mgd77Data.LATITUDE.name].max(skipna=True))
    header_dict[Mgd77Header.TOP_MOST_LATITUDE_OF_SURVEY.name] = f"{top_latitude}"
    bottom_latitude = math.floor(df[Mgd77Data.LATITUDE.name].min(skipna=True))
    header_dict[Mgd77Header.BOTTOM_MOST_LATITUDE_OF_SURVEY.name] = f"{bottom_latitude}"
    right_longitude = math.ceil(df[Mgd77Data.LONGITUDE.name].max(skipna=True))
    header_dict[Mgd77Header.RIGHT_MOST_LONGITUDE_OF_SURVEY.name] = f"{right_longitude}"
    left_longitude = math.floor(df[Mgd77Data.LONGITUDE.name].min(skipna=True))
    header_dict[Mgd77Header.LEFT_MOST_LONGITUDE_OF_SURVEY.name] = f"{left_longitude}"
    df = df.replace(np.nan, 999999)  # replace NaN values because they are not handled by formatters

    # header
    header = write_mgd77_header(header_dict)

    # data
    data_lines = df.to_string(
        header=False, index=False, index_names=False, columns=columns, justify="justify-all", formatters=formatters
    ).replace(" ", "")

    with open(output_file_path, "w", encoding="utf-8") as f:
        f.write(header)
        f.write(data_lines)
        f.flush()


def write_mgd77_header(var_dic: dict) -> str:
    """
    Builds the MGD77 header with parameters.
    :param var_dic: header parameters
    :return: header as string
    """
    header = ""
    i = 1
    for values in Mgd77Header:
        if values.row != i:
            header = header + f"{i:02}" + "\n"
            i += 1
        if values.set_value:
            header = header + values.set_value
        elif values.name in var_dic:
            if var_dic[values.name] and len(var_dic[values.name]) > values.length:
                print(values.name, type(var_dic[values.name]), len(var_dic[values.name]), var_dic[values.name])
                raise Exception(f"error character in {values.name} higher than max character expected")
            header = header + values.write_formatter(var_dic[values.name])
        else:
            header = header + values.write_formatter(None)
    return header + f"{i:02}" + "\n"


def create_mgd77_ds(techsas_files: List[str], period: str = None) -> xr.Dataset:
    """
     Creates a new MGD77 dataset from TECHSAS files (gravimetry, magnetism & depth).
    :param techsas_files: input TECHSAS files
    :param period:  resampling period (optional)
    :return: MGD77 Xarray Dataset
    """
    result_ds = xr.Dataset()

    # get data from gravimetry files
    for gravi_file in [x for x in techsas_files if re.search(GRAVI_EXTENSION, x)]:
        logger.info(f"Read gravity from : {gravi_file}...")
        with xr.open_dataset(gravi_file) as gravi_ds:
            # check file content
            if (
                GRAVITY_NAME not in gravi_ds
                or EOTVOS_CORRECTION_NAME not in gravi_ds
                or FREE_AIR_ANOMALY_NAME not in gravi_ds
            ):
                logger.error(
                    f"Invalid gravimetry file {gravi_file} : theoretic gravimetry, computed eotvos correction and free air anomaly variables are required."
                )
                continue

            gravi_mgd77_ds = xr.Dataset(
                data_vars={
                    Mgd77Data.LONGITUDE.name: ([Mgd77Data.DATE_TIME.name], gravi_ds[TECHSAS_LONGITUDE].data),
                    Mgd77Data.LATITUDE.name: ([Mgd77Data.DATE_TIME.name], gravi_ds[TECHSAS_LATITUDE].data),
                    Mgd77Data.OBSERVED_GRAVITY.name: (
                        [Mgd77Data.DATE_TIME.name],
                        gravi_ds[GRAVITY_NAME].data,
                    ),
                    Mgd77Data.EOTVOS_CORRECTION.name: (
                        [Mgd77Data.DATE_TIME.name],
                        gravi_ds[EOTVOS_CORRECTION_NAME].data,
                    ),
                    Mgd77Data.FREE_AIR_ANOMALY.name: ([Mgd77Data.DATE_TIME.name], gravi_ds[FREE_AIR_ANOMALY_NAME].data),
                },
                coords={Mgd77Data.DATE_TIME.name: gravi_ds[TECHSAS_TIME].data},
            )

            if TECHSAS_STATUS in gravi_ds:
                gravi_ds = gravi_ds.rename(time=Mgd77Data.DATE_TIME.name)
                size_before = gravi_mgd77_ds[Mgd77Data.OBSERVED_GRAVITY.name].size
                gravi_mgd77_ds = gravi_mgd77_ds.where(gravi_ds[TECHSAS_STATUS] == 0.0, drop=True)
                logger.info(
                    f"Gravimetry filtered from status : {gravi_mgd77_ds[Mgd77Data.OBSERVED_GRAVITY.name].size} / {size_before} valid data"
                )
            result_ds = xr.merge([result_ds, gravi_mgd77_ds])

    # get data from magnetism files
    for mag_file in [x for x in techsas_files if re.search(MAG_EXTENSION, x)]:
        logger.info(f"Read magnetism from : {mag_file}...")
        with xr.open_dataset(mag_file) as mag_ds:
            mag_mgd77_ds = xr.Dataset(
                data_vars={
                    Mgd77Data.LONGITUDE.name: ([Mgd77Data.DATE_TIME.name], mag_ds[TECHSAS_LONGITUDE].data),
                    Mgd77Data.LATITUDE.name: ([Mgd77Data.DATE_TIME.name], mag_ds[TECHSAS_LATITUDE].data),
                    Mgd77Data.MAGNETICS_TOTAL_FIELD_1.name: ([Mgd77Data.DATE_TIME.name], mag_ds[TECHSAS_MAG].data),
                    Mgd77Data.MAGNETICS_RESIDUAL_FIELD.name: (
                        [Mgd77Data.DATE_TIME.name],
                        mag_ds[MAGNETIC_ANOMALY_NAME].data,
                    ),
                },
                coords={Mgd77Data.DATE_TIME.name: mag_ds[TECHSAS_TIME].data},
            )

            if TECHSAS_STATUS in mag_ds:
                mag_ds = mag_ds.rename(time=Mgd77Data.DATE_TIME.name)
                mag_mgd77_ds = mag_mgd77_ds.where(mag_ds[TECHSAS_STATUS] == 0.0, drop=True)

            result_ds = xr.merge([result_ds, mag_mgd77_ds])

    # get data from depth files
    for depth_file in [x for x in techsas_files if re.search(DEPTH_EXTENSION, x)]:
        logger.info(f"Read depth from : {depth_file}...")
        with xr.open_dataset(depth_file) as depth_ds:
            depth_mgd77_ds = xr.Dataset(
                data_vars={
                    Mgd77Data.LONGITUDE.name: ([Mgd77Data.DATE_TIME.name], depth_ds[TECHSAS_LONGITUDE].data),
                    Mgd77Data.LATITUDE.name: ([Mgd77Data.DATE_TIME.name], depth_ds[TECHSAS_LATITUDE].data),
                    Mgd77Data.BATHYMETRY_CORRECTED_DEPTH.name: ([Mgd77Data.DATE_TIME.name], depth_ds["snd"].data),
                },
                coords={Mgd77Data.DATE_TIME.name: depth_ds[TECHSAS_TIME].data},
            )

            if TECHSAS_STATUS in depth_ds:
                depth_ds = depth_ds.rename(time=Mgd77Data.DATE_TIME.name)
                depth_mgd77_ds = depth_mgd77_ds.where(depth_ds[TECHSAS_STATUS] == 0.0, drop=True)

            result_ds = xr.merge([result_ds, depth_mgd77_ds])

    # resampling
    if period:
        logger.info(f"Resampling with period : {period}...")
        result_ds = result_ds.resample({Mgd77Data.DATE_TIME.name: period}).mean()

    return result_ds

from abc import abstractmethod
from os import PathLike
from typing import runtime_checkable, Protocol, Optional

import numpy as np


@runtime_checkable
class AbstractTechsasNavigation(Protocol):
    """
    Interface for navigation data from TECHSAS files.

    WARNING : this protocol shall match the AbstractNavigation protocol of PyAT project.
    """

    @abstractmethod
    def get_name(self) -> str: ...

    @abstractmethod
    def get_file_path(self) -> PathLike | str | None: ...

    @abstractmethod
    def get_times(self) -> np.ndarray: ...

    @abstractmethod
    def get_latitudes(self) -> np.ndarray: ...

    @abstractmethod
    def get_longitudes(self) -> np.ndarray: ...

    @abstractmethod
    def get_headings(self) -> Optional[np.ndarray]: ...

    @abstractmethod
    def get_altitudes(self) -> Optional[np.ndarray]: ...

    @abstractmethod
    def get_speeds(self) -> Optional[np.ndarray]: ...

    @abstractmethod
    def get_courses_over_ground(self) -> Optional[np.ndarray]: ...

    @abstractmethod
    def get_sensor_quality_indicators(self) -> Optional[np.ndarray]: ...

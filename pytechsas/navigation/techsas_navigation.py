import os
from os import PathLike
from typing import List, Optional

import numpy as np
import pandas as pd
import xarray as xr

from pytechsas.navigation.abstract_techsas_navigation import AbstractTechsasNavigation
from pytechsas.sensor.techsas_constant import (
    TECHSAS_ALTITUDE,
    TECHSAS_ALTITUDE_2,
    TECHSAS_DEPTH,
    TECHSAS_HEADING,
    TECHSAS_LATITUDE,
    TECHSAS_LONGITUDE,
    TECHSAS_SPEED,
    TECHSAS_TIME,
    TECHSAS_GND_COURSE,
    TECHSAS_GND_SPEED_2,
    TECHSAS_LONGITUDE_2,
    TECHSAS_MODE,
    TECHSAS_GND_SPEED,
)


class TechsasDatasetNavigation(AbstractTechsasNavigation):
    """Navigation from TECHSAS xarray Dataset."""

    def __init__(self, ds: xr.Dataset, file_path: PathLike | str | None = None):
        self.file_path = file_path
        self.dataset = ds

    def get_name(self) -> str:
        return os.path.basename(self.file_path)

    def get_file_path(self) -> PathLike | str | None:
        return self.file_path

    def close(self):
        self.dataset.close()

    def get_times(self) -> np.ndarray:
        return self.dataset[TECHSAS_TIME].data

    def get_latitudes(self) -> np.ndarray:
        return self.dataset[TECHSAS_LATITUDE].data

    def get_longitudes(self) -> np.ndarray:
        return self.dataset[TECHSAS_LONGITUDE].data

    def get_headings(self) -> Optional[np.ndarray]:
        return self.dataset[TECHSAS_HEADING].data if TECHSAS_HEADING in self.dataset else None

    def get_speeds(self) -> Optional[np.ndarray]:
        return (
            self.dataset[TECHSAS_GND_SPEED].data
            if TECHSAS_GND_SPEED in self.dataset
            else self.dataset[TECHSAS_SPEED].data if TECHSAS_SPEED in self.dataset else None
        )

    def get_courses_over_ground(self) -> Optional[np.ndarray]:
        return self.dataset[TECHSAS_GND_COURSE].data if TECHSAS_GND_COURSE in self.dataset else None

    def get_altitudes(self) -> Optional[np.ndarray]:
        return self.dataset[TECHSAS_ALTITUDE].data if TECHSAS_ALTITUDE in self.dataset else None

    def get_sensor_quality_indicators(self) -> Optional[np.ndarray]:
        return self.dataset[TECHSAS_MODE].data if TECHSAS_MODE in self.dataset else None


class TechsasNavFileNavigation(TechsasDatasetNavigation):
    """Navigation from '.nav' TECHSAS file."""

    def __init__(self, file_path: PathLike | str):
        super().__init__(xr.open_dataset(file_path), file_path)

    def get_longitudes(self) -> np.ndarray:
        return self.dataset[TECHSAS_LONGITUDE_2].data

    def get_speeds(self) -> Optional[np.ndarray]:
        return self.dataset[TECHSAS_GND_SPEED_2].data


class TechsasSubnavFileNavigation(TechsasDatasetNavigation):
    """Navigation from '.subnav' TECHSAS file."""

    def __init__(self, file_path: PathLike | str):
        super().__init__(xr.open_dataset(file_path), file_path)

    def get_longitudes(self) -> np.ndarray:
        return self.dataset[TECHSAS_LONGITUDE_2].data

    def get_altitudes(self) -> np.ndarray:
        return -self.dataset[TECHSAS_DEPTH].data

    def get_speeds(self) -> Optional[np.ndarray]:
        return self.dataset[TECHSAS_GND_SPEED_2].data


class TechsasGpsFileNavigation(TechsasDatasetNavigation):
    """Navigation from '.gps' TECHSAS file."""

    def __init__(self, file_path: PathLike | str):
        super().__init__(xr.open_dataset(file_path), file_path)

    def get_longitudes(self) -> np.ndarray:
        return self.dataset[TECHSAS_LONGITUDE_2].data

    def get_altitudes(self) -> np.ndarray:
        return self.dataset[TECHSAS_ALTITUDE_2].data

    def get_speeds(self) -> Optional[np.ndarray]:
        return self.dataset[TECHSAS_GND_SPEED_2].data


class TechsasFileNavigation(TechsasDatasetNavigation):
    """Navigation from any TECHSAS file but '.nav, .subnav, .gps.'"""

    def __init__(self, file_path: PathLike | str):
        super().__init__(xr.open_dataset(file_path), file_path)


def nav_to_dataset(nav: AbstractTechsasNavigation) -> xr.Dataset:
    """@return this navigation supplier as a Xarray Dataset."""
    time_coord = {TECHSAS_TIME: nav.get_times()}
    ds = xr.Dataset(
        data_vars={
            TECHSAS_LONGITUDE: ([TECHSAS_TIME], nav.get_longitudes()),
            TECHSAS_LATITUDE: ([TECHSAS_TIME], nav.get_latitudes()),
        },
        coords=time_coord,
    )
    heading = nav.get_headings()
    if heading is not None:
        ds[TECHSAS_HEADING] = xr.DataArray(data=heading, coords=time_coord)
    speed = nav.get_speeds()
    if speed is not None:
        ds[TECHSAS_SPEED] = xr.DataArray(data=speed, coords=time_coord)
    course_over_ground = nav.get_courses_over_ground()
    if course_over_ground is not None:
        ds[TECHSAS_GND_COURSE] = xr.DataArray(data=course_over_ground, coords=time_coord)
    return ds


def merge(navigation_list: List[AbstractTechsasNavigation]) -> AbstractTechsasNavigation:
    """
    Uses xarray to merge several navigation data.
    """
    merged_ds = xr.merge([nav_to_dataset(nav) for nav in navigation_list])
    return TechsasDatasetNavigation(merged_ds)


def get_heading_as_series(nav: AbstractTechsasNavigation) -> pd.Series:
    """Convert the heading array into a pandas.Series"""
    return pd.Series(data=nav.get_headings(), index=nav.get_times(), copy=False)


def get_speed_as_series(nav: AbstractTechsasNavigation) -> pd.Series:
    """Convert the speed array into a pandas.Series"""
    return pd.Series(data=nav.get_speeds(), index=nav.get_times(), copy=False)


def get_gnd_course_as_series(nav: AbstractTechsasNavigation) -> pd.Series:
    """Convert the course over ground array into a pandas.Series"""
    return pd.Series(data=nav.get_courses_over_ground(), index=nav.get_times(), copy=False)

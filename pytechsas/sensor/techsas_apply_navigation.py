import logging as log
import os
from abc import abstractmethod
from typing import runtime_checkable, Protocol, Optional

import numpy as np
import xarray as xr

from pytechsas.sensor.techsas_constant import (
    TECHSAS_LONGITUDE,
    TECHSAS_LATITUDE,
    TECHSAS_HEADING,
    TECHSAS_GND_SPEED,
    TECHSAS_GND_COURSE,
    TECHSAS_LATITUDE_STANDARD_NAME,
    TECHSAS_LATITUDE_LONG_NAME,
    TECHSAS_LATITUDE_UNITS,
    TECHSAS_LATITUDE_VALID_RANGE,
    TECHSAS_LONGITUDE_STANDARD_NAME,
    TECHSAS_LONGITUDE_LONG_NAME,
    TECHSAS_LONGITUDE_UNITS,
    TECHSAS_LONGITUDE_VALID_RANGE,
    TECHSAS_HEADING_STANDARD_NAME,
    TECHSAS_HEADING_LONG_NAME,
    TECHSAS_HEADING_UNITS,
    TECHSAS_HEADING_VALID_RANGE,
    TECHSAS_GND_SPEED_STANDARD_NAME,
    TECHSAS_GND_SPEED_LONG_NAME,
    TECHSAS_GND_SPEED_UNITS,
    TECHSAS_GND_SPEED_VALID_MIN,
    TECHSAS_GND_COURSE_STANDARD_NAME,
    TECHSAS_GND_COURSE_LONG_NAME,
    TECHSAS_GND_COURSE_UNITS,
    TECHSAS_GND_COURSE_VALID_RANGE,
    TECHSAS_TIME,
    STANDARD_NAME_ATTR,
    LONG_NAME_ATTR,
    UNITS_ATTR,
    VALID_RANGE_ATTR,
    VALID_MIN,
)
from pytechsas.sensor.techsas_file import add_history

logger = log.getLogger()


@runtime_checkable
class ApplyNavigationData(Protocol):
    """
    Expected navigation data to localize a TECHSAS file with 'apply navigation' process.

    Navigation data is expected valid (already filtered).
    """

    @abstractmethod
    def get_name(self) -> str: ...

    @abstractmethod
    def get_times(self) -> np.ndarray: ...

    @abstractmethod
    def get_latitudes(self) -> np.ndarray: ...

    @abstractmethod
    def get_longitudes(self) -> np.ndarray: ...

    @abstractmethod
    def get_headings(self) -> Optional[np.ndarray]: ...

    @abstractmethod
    def get_speeds(self) -> Optional[np.ndarray]: ...

    @abstractmethod
    def get_courses_over_ground(self) -> Optional[np.ndarray]: ...


def __get_nav_dataset__(nav: ApplyNavigationData) -> xr.Dataset:
    """Builds xarray Dataset from navigation data."""
    time_coord = {TECHSAS_TIME: nav.get_times()}
    ds = xr.Dataset(
        data_vars={
            TECHSAS_LONGITUDE: xr.DataArray(
                data=nav.get_longitudes(),
                coords=time_coord,
                attrs={
                    STANDARD_NAME_ATTR: TECHSAS_LONGITUDE_STANDARD_NAME,
                    LONG_NAME_ATTR: TECHSAS_LONGITUDE_LONG_NAME,
                    UNITS_ATTR: TECHSAS_LONGITUDE_UNITS,
                    VALID_RANGE_ATTR: TECHSAS_LONGITUDE_VALID_RANGE,
                },
            ),
            TECHSAS_LATITUDE: xr.DataArray(
                data=nav.get_latitudes(),
                coords=time_coord,
                attrs={
                    STANDARD_NAME_ATTR: TECHSAS_LATITUDE_STANDARD_NAME,
                    LONG_NAME_ATTR: TECHSAS_LATITUDE_LONG_NAME,
                    UNITS_ATTR: TECHSAS_LATITUDE_UNITS,
                    VALID_RANGE_ATTR: TECHSAS_LATITUDE_VALID_RANGE,
                },
            ),
        },
        coords=time_coord,
    )

    heading = nav.get_headings()
    if heading is not None:
        ds[TECHSAS_HEADING] = xr.DataArray(
            data=heading,
            coords=time_coord,
            attrs={
                STANDARD_NAME_ATTR: TECHSAS_HEADING_STANDARD_NAME,
                LONG_NAME_ATTR: TECHSAS_HEADING_LONG_NAME,
                UNITS_ATTR: TECHSAS_HEADING_UNITS,
                VALID_RANGE_ATTR: TECHSAS_HEADING_VALID_RANGE,
            },
        )

    speed = nav.get_speeds()
    if speed is not None:
        ds[TECHSAS_GND_SPEED] = xr.DataArray(
            data=speed,
            coords=time_coord,
            attrs={
                STANDARD_NAME_ATTR: TECHSAS_GND_SPEED_STANDARD_NAME,
                LONG_NAME_ATTR: TECHSAS_GND_SPEED_LONG_NAME,
                UNITS_ATTR: TECHSAS_GND_SPEED_UNITS,
                VALID_MIN: TECHSAS_GND_SPEED_VALID_MIN,
            },
        )
    course_over_ground = nav.get_courses_over_ground()
    if course_over_ground is not None:
        ds[TECHSAS_GND_COURSE] = xr.DataArray(
            data=course_over_ground,
            coords=time_coord,
            attrs={
                STANDARD_NAME_ATTR: TECHSAS_GND_COURSE_STANDARD_NAME,
                LONG_NAME_ATTR: TECHSAS_GND_COURSE_LONG_NAME,
                UNITS_ATTR: TECHSAS_GND_COURSE_UNITS,
                VALID_RANGE_ATTR: TECHSAS_GND_COURSE_VALID_RANGE,
            },
        )
    return ds


def apply_navigation(techsas_file_path: str, nav: ApplyNavigationData) -> None:
    """
    Add navigation data to a TECHSAS file.
    :param techsas_file_path: input TECHSAS file
    :param nav: navigation data
    """
    logger.info(f"Apply navigation to {techsas_file_path}...")

    # read navigation file
    nav_ds = __get_nav_dataset__(nav)
    nav_start = nav_ds[TECHSAS_TIME].data[0]
    nav_end = nav_ds[TECHSAS_TIME].data[-1]
    logger.info(f"Navigation data from {nav_start} to {nav_end} ")

    # remove duplicate times indexes
    _, unique_indices = np.unique(nav_ds[TECHSAS_TIME], return_index=True)
    duplicate_count = nav_ds[TECHSAS_TIME].size - unique_indices.size
    if duplicate_count != 0:
        logger.warning(f"{duplicate_count} duplicate time indexes in navigation data : keep only first occurrences")
        nav_ds = nav_ds.isel(time=unique_indices)

    with xr.open_dataset(techsas_file_path) as techsas_ds:
        # check date
        techsas_start = techsas_ds[TECHSAS_TIME].data[0]
        techsas_end = techsas_ds[TECHSAS_TIME].data[-1]
        logger.info(f"TECHSAS data from {techsas_start} to {techsas_end}")
        if techsas_start > nav_end or techsas_end < nav_start:
            raise Exception(f"navigation data does not match with input file.")

        # interpolation
        logger.info("Compute interpolated navigation...")

        # use 'unwrap' method to get a correct interpolation of heading/gndCourse
        if TECHSAS_HEADING in nav_ds:
            heading_unwrapped = nav_ds[TECHSAS_HEADING]
            heading_unwrapped[~np.isnan(heading_unwrapped)] = np.unwrap(
                heading_unwrapped[~np.isnan(heading_unwrapped)], period=360
            )
            nav_ds[TECHSAS_HEADING][:] = heading_unwrapped

        if TECHSAS_GND_COURSE in nav_ds:
            gnd_course_unwrapped = nav_ds[TECHSAS_GND_COURSE]
            gnd_course_unwrapped[~np.isnan(gnd_course_unwrapped)] = np.unwrap(
                gnd_course_unwrapped[~np.isnan(gnd_course_unwrapped)], period=360
            )
            nav_ds[TECHSAS_GND_COURSE][:] = gnd_course_unwrapped

        nav_ds = nav_ds.interp_like(techsas_ds)
        # apply modulo on heading/gndCourse (because heading has been unwraped before interpolation)
        if TECHSAS_HEADING in nav_ds:
            nav_ds[TECHSAS_HEADING][:] = nav_ds[TECHSAS_HEADING] % 360
        if TECHSAS_GND_COURSE in nav_ds:
            nav_ds[TECHSAS_GND_COURSE][:] = nav_ds[TECHSAS_GND_COURSE] % 360

        # update history attribute
        add_history(
            origin_ds=techsas_ds,
            new_ds=nav_ds,
            module_name=os.path.basename(__file__),
            history_info=(
                f"Apply navigation with data from : {nav.get_name()}"
                if hasattr(nav, "nav_file")
                else "Apply navigation"
            ),
        )

    # do not save time : already exist
    nav_ds = nav_ds.drop_vars(TECHSAS_TIME)

    # append navigation variables to output file
    logger.info(f"Save interpolated navigation into processed file...")
    nav_ds.to_netcdf(path=techsas_file_path, mode="a")

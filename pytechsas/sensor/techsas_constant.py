# attributes
LONG_NAME = "long_name"
UNITS = "units"
HISTORY = "history"
FIRST_FRAME_DATE = "firstframetime"
LAST_FRAME_DATE = "lastframetime"
CREATION_TIME = "creationtime"

# Units
DEGREE = "degree"
DEGREES_NORTH = "degrees_north"
DEGREES_EAST = "degrees_east"
METER_SECONDS = "m/s"

# variables
TECHSAS_TIME = "time"

TECHSAS_LONGITUDE = "lon"
TECHSAS_LONGITUDE_2 = "long"
TECHSAS_LONGITUDE_STANDARD_NAME = "longitude"
TECHSAS_LONGITUDE_LONG_NAME = "longitude"
TECHSAS_LONGITUDE_UNITS = DEGREES_EAST
TECHSAS_LONGITUDE_VALID_RANGE = [-180.0, 180.0]

TECHSAS_LATITUDE = "lat"
TECHSAS_LATITUDE_STANDARD_NAME = "latitude"
TECHSAS_LATITUDE_LONG_NAME = "latitude"
TECHSAS_LATITUDE_UNITS = DEGREES_NORTH
TECHSAS_LATITUDE_VALID_RANGE = [-90.0, 90.0]

TECHSAS_HEADING = "heading"
TECHSAS_HEADING_STANDARD_NAME = "heading"
TECHSAS_HEADING_LONG_NAME = "heading(true)"
TECHSAS_HEADING_UNITS = DEGREE
TECHSAS_HEADING_VALID_RANGE = [0, 360]

TECHSAS_SPEED = "speed"  # Deprecated speed variable (used by previous version of "Apply Navigation")
TECHSAS_GND_SPEED = "speed_over_ground"
TECHSAS_GND_SPEED_2 = "gndspeed"  # Speed variable of .nav, .gps and .subnav TECHSAS files
TECHSAS_GND_SPEED_STANDARD_NAME = "speed_wrt_ground"
TECHSAS_GND_SPEED_LONG_NAME = "speed over ground"
TECHSAS_GND_SPEED_UNITS = METER_SECONDS
TECHSAS_GND_SPEED_VALID_MIN = 0.0

TECHSAS_GND_COURSE = "gndcourse"
TECHSAS_GND_COURSE_STANDARD_NAME = "course"
TECHSAS_GND_COURSE_LONG_NAME = "course over ground"
TECHSAS_GND_COURSE_UNITS = DEGREE
TECHSAS_GND_COURSE_VALID_RANGE = [0, 360]

TECHSAS_ALTITUDE = "altitude"
TECHSAS_ALTITUDE_2 = "alt"
TECHSAS_MAG = "mag"
TECHSAS_GRAVITY = "gravity"
TECHSAS_STATUS = "status"
TECHSAS_MODE = "mode"
TECHSAS_DEPTH = "depth"


# Variables attributes
STANDARD_NAME_ATTR = "standard_name"
UNITS_ATTR = "units"
LONG_NAME_ATTR = "long_name"
VALID_RANGE_ATTR = "valid_range"
VALID_MIN = "valid_min"

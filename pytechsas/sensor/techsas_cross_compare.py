# pylint: skip-file

import collections
import csv
import logging as log
import os
from builtins import int
from datetime import datetime
from typing import List

import numpy as np
import shapely.geometry as geo
import xarray as xr
from scipy import spatial
from shapely.ops import unary_union

from ..mgd77 import mgd77_driver
from .techsas_constant import TECHSAS_GRAVITY

logger = log.getLogger()

OBSERVED_VAR = "observed"


class IntersectionData:
    """Class to represent data around an intersection point"""

    def __init__(self, value_count: int, start_date: datetime, end_date: datetime, mean: float, nearest: float):
        self.value_count = value_count
        self.start_date = start_date
        self.end_date = end_date
        self.mean = mean
        self.nearest = nearest

    def __str__(self):
        return f"Data count : {self.value_count}; Start : {self.start_date}; End : {self.end_date}; Mean : {self.mean}; Nearest : {self.nearest}"

    def dict(self):
        result = self.__dict__.copy()
        result["start_date"] = str(self.start_date)
        result["end_date"] = str(self.start_date)
        return result


class Intersection:
    """Class to represent an intersection between two data set gravity reference point"""

    def __init__(self, latitude: float, longitude: float, radius: float):
        self.latitude = latitude
        self.longitude = longitude
        self.radius = radius
        self.data: List[IntersectionData] = []

    def __str__(self):
        return f"latitude = {self.latitude}, longitude = {self.longitude},  gap = {self.stdev()}"

    def stdev(self):
        return np.std([d.mean for d in self.data])

    def add_data(self, data: IntersectionData):
        self.data.append(data)

    def dict(self):
        result = self.__dict__.copy()
        result["stdev"] = self.stdev()
        result["data"] = [d.dict() for d in self.data]
        return result


def cross_compare(
    i_path: str,
    i_path_2: str = None,
    intersection_radius: float = 0.001,
    variable_to_compare: str = TECHSAS_GRAVITY,
    o_path: str = None,
    overwrite: bool = False,
):
    """
    Applies cross compare : computes intersections and compares data around.
    @note If only one file is supplied, this method will process intersection of this file with itself.
    @param i_path: first input file
    @param i_path_2: second input file (optional).
    @param intersection_radius: defines the area of an intersection where data will be retrieved for comparison
    @param variable_to_compare: variable used for comparison
    @param o_path: output CSV file path (.csv) (filled with result intersections)
    @param overwrite: option to overwrite output file if already exist
    """
    # analyse cross between two navigation routes
    if i_path_2 is not None and i_path != i_path_2:
        logger.info(f"Compute intersections between {os.path.basename(i_path)} with {os.path.basename(i_path_2)}...")
        ds_1 = __open__(i_path, variable_to_compare)
        ds_2 = __open__(i_path_2, variable_to_compare)
        intersection_coords = __get_intersection_coordinates__(ds_1, ds_2)
        logger.info(f"{len(intersection_coords)} intersections found.")
        logger.info(f"For each intersection, compare data in a radius of {intersection_radius} degrees...")
        intersections = __process_intersections__(intersection_coords, intersection_radius, ds_1, ds_2)

    # or analyse intersections of one navigation route with itself
    else:
        logger.info(f"Compute self intersections in {os.path.basename(i_path)}...")
        ds = __open__(i_path, variable_to_compare)
        logger.info(f"open successfuly")
        intersection_coords = __get_self_intersection_coordinates__(ds)
        logger.info(f"{len(intersection_coords)} intersections found.")
        logger.info(f"For each intersection, compare data in a radius of {intersection_radius} degrees...")
        intersections = __process_self_intersections__(intersection_coords, intersection_radius, ds)

    # print result and write output file
    logger.info("Result : ")
    for intersection in intersections[:100]:
        logger.info(intersection)
    if len(intersections) > 100:
        logger.info("...")
    if len(intersections) > 0 and o_path is not None:
        __write_output_file__(intersections, o_path=o_path, overwrite=overwrite)


def __open__(path: str, variable: str) -> xr.Dataset:
    """
    Opens file (TECHSAS or MGD77) and build a xarray Dataset that will be used for the process.
    """
    if path.endswith("mgd77"):
        with mgd77_driver.read_mgd77(path) as ds:
            time = ds.DATE_TIME.data
            lat = ds.LATITUDE.data
            lon = ds.LONGITUDE.data
            if variable not in ds:
                raise ValueError(
                    f"Variable to compare '{variable}' is missing from input file : {os.path.basename(path)}."
                    f"\n Please choose variable to compare in : {[var for var in ds]}"
                )
            var = ds[variable].data
    else:
        with xr.open_dataset(path) as ds:
            mask = ds.status.data == 0 if "status" in ds else None
            time = ds.time.data[mask] if mask is not None else ds.time.data
            lat = ds.lat.data[mask] if mask is not None else ds.lat.data
            lon = ds.lon.data[mask] if mask is not None else ds.lon.data
            if variable not in ds:
                raise ValueError(
                    f"Variable to compare '{variable}' is missing from input file : {os.path.basename(path)}."
                    f"\n Please choose variable to compare in : {[var for var in ds]}"
                )
            var = ds[variable].data[mask] if mask is not None else ds[variable].data

    return xr.Dataset(
        data_vars={
            "lat": ("time", lat),
            "lon": ("time", lon),
            OBSERVED_VAR: ("time", var),
        },
        coords={"time": time},
    )


# INTERSECTION BETWEEN TWO FILES


def __get_intersection_coordinates__(ds_1: xr.Dataset, ds_2: xr.Dataset) -> (np.ndarray, np.ndarray):
    """
    Computes intersection coordinates between two dataset.
    """
    line_1 = geo.LineString(np.stack((ds_1.lon, ds_1.lat), axis=-1))
    line_2 = geo.LineString(np.stack((ds_2.lon, ds_2.lat), axis=-1))
    intersections = line_1.intersection(line_2)
    intersection_coords = None
    if isinstance(intersections, geo.MultiPoint):
        intersection_coords = [(p.y, p.x) for p in intersections.geoms]  # respect convention "lat ; lon"
    if isinstance(intersections, geo.Point):
        intersection_coords = [(intersections.y, intersections.x)]  # respect convention "lat ; lon"
    if isinstance(intersections, geo.LineString):
        logger.error("Error : intersection can not be computed from two identical navigation.")
    return intersection_coords


def __process_intersections__(intersection_coords, intersection_radius: float, ds_1: xr.Dataset, ds_2: xr.Dataset):
    """
    Processes intersections : get neighbor data from each dataset...
    """
    result: [Intersection] = []
    kd_tree_1 = spatial.KDTree(np.stack((ds_1.lon.data, ds_1.lat.data), axis=-1))
    kd_tree_2 = spatial.KDTree(np.stack((ds_2.lon.data, ds_2.lat.data), axis=-1))
    for idx, (intersection_lat, intersection_lon) in enumerate(intersection_coords):
        if (progress := (100 * (idx + 1) / len(intersection_coords))) % 10 == 0:  # logger.info each 10%
            logger.info(f"Process intersection... {int(progress)}%")
        intersection = Intersection(intersection_lat, intersection_lon, intersection_radius)
        intersection_data_1 = __get_data_in_intersection__(intersection, kd_tree_1, ds_1)
        intersection_data_2 = __get_data_in_intersection__(intersection, kd_tree_2, ds_2)
        if intersection_data_1 is not None and intersection_data_2 is not None:
            intersection.add_data(intersection_data_1)
            intersection.add_data(intersection_data_2)
            result.append(intersection)
    return result


def __get_data_in_intersection__(intersection: Intersection, kd_tree, ds: xr.Dataset) -> IntersectionData | None:
    """
    Finds neighbors data of the specified intersection, compute mean and nearset.
    """
    neighbors_indices = kd_tree.query_ball_point(
        [intersection.longitude, intersection.latitude], intersection.radius, return_sorted=True
    )
    intersection_ds = ds.isel(time=neighbors_indices)
    if intersection_ds.time.size == 0:
        return  # no data near this intersection
    return IntersectionData(
        intersection_ds.time.size,
        intersection_ds.time.data[0],
        intersection_ds.time.data[-1],
        intersection_ds[OBSERVED_VAR].data.mean(),
        intersection_ds[OBSERVED_VAR].data[0],
    )


# INTERSECTION OF ONE FILE WITH ITSELF


def __get_self_intersection_coordinates__(ds: xr.Dataset):
    """
    Computes self intersections from one dataset.
    """
    line = geo.LineString(np.stack((ds.lat, ds.lon), axis=-1))  # respect lat - lon convention
    mls = unary_union(line)
    coords_list = [non_itersecting_ls.coords for non_itersecting_ls in mls.geoms]
    for non_itersecting_ls in mls.geoms:
        coords_list.extend(non_itersecting_ls.coords)
    return [item for item, count in collections.Counter(coords_list).items() if count > 1]


def __process_self_intersections__(intersection_coords: List, intersection_radius: float, ds: xr.Dataset):
    """
    Compares data near the self intersection point of a dataset.
    """
    kd_tree = spatial.KDTree(np.stack((ds.lon.data, ds.lat.data), axis=-1))
    result: [Intersection] = []
    intersection_count = len(intersection_coords)
    for idx, (intersection_lat, intersection_lon) in enumerate(intersection_coords):
        if (progress := (100 * (idx + 1) / intersection_count)) % 10 <= (100 / intersection_count):  # each 10%
            logger.info(f"Process intersection... {int(progress)}%")

        # get data in intersection area
        # kd_tree = spatial.KDTree(np.stack((ds.lon.data, ds.lat.data), axis=-1))
        neighbors_indices = kd_tree.query_ball_point(
            [intersection_lon, intersection_lat], intersection_radius, return_sorted=True
        )

        if len(neighbors_indices) == 0 or len(neighbors_indices) > 1000:
            continue  # not enough (or too many : plateforme in "station" mode ?) value near this intersection

        # group data by line (split when there is a gap > 1 between indexes)
        group_ds = [
            ds.isel(time=group)
            for group in np.split(neighbors_indices, np.where(np.diff(neighbors_indices) != 1)[0] + 1)
        ]
        if len(group_ds) > 1:
            intersection = Intersection(intersection_lat, intersection_lon, intersection_radius)
            # For each data group around the current intersection, compute mean and nearest value
            for g in group_ds:
                data = g[OBSERVED_VAR].data
                if len(data) > 0 and not np.isnan(data[0]):  # check if there is enough data and the nearest is not NAN
                    intersection.add_data(
                        IntersectionData(
                            g.time.size,
                            g.time[0].data,
                            g.time[-1].data,
                            np.nanmean(data),
                            data[0],
                        )
                    )
            if len(intersection.data) > 1:
                result.append(intersection)
    return result


def __write_output_file__(intersections: List[Intersection], o_path: str, overwrite: bool = True):
    """
    Writes intersection in CSV file.
    """
    if not overwrite and os.path.exists(o_path):
        logger.warning(
            f"Can't write output file : {os.path.basename(o_path)} already exists and overwrite option is false."
        )
        return
    with open(o_path, "w", newline="", encoding="utf-8") as csvfile:
        fieldnames = ["latitude", "longitude", "stdev", "data"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, extrasaction="ignore", delimiter=";")
        writer.writeheader()
        writer.writerows([i.dict() for i in intersections])

import os
import traceback
from datetime import datetime
from typing import Optional, List

import dateutil
import pandas as pd

from .techsas_file import read_techsas_file, interpolate_values
from ..utils.numpy_utils import to_utc


class TechsasCSVExporter:
    def __init__(
        self,
        logger,
        input_files: List,
        out_files_dir: str,
        sensors: List,
        start_date: Optional,
        end_date: Optional,
        sampling: int = 0,
        enable_time_interval: bool = False,
    ):
        self.logger = logger
        self.input_files: List = input_files
        self.out_files_dir = out_files_dir
        self.sensors = sensors
        self.start_date = start_date
        self.end_date = end_date
        self.sampling = sampling
        self.enable_time_interval = enable_time_interval

    def parse_and_export(self, nav_latitudes, nav_longitudes, nav_time):
        """
        this function get object and numpy arrays from TechsasCSVExporterLauncher and will parse and export it
        to csv format.
        @param nav_latitudes: latitudes from navigation files
        @param nav_longitudes: longitudes from navigation files
        @param nav_time: time from navigation files
        """
        files_in_error = []
        input_files_dict = {}
        all_df = None
        # order input files by sensor
        for infile in self.input_files:
            if infile.is_file():
                name_fields = infile.name.split("-", 4)
                if len(name_fields) < 4:
                    continue
                (date, time, prefix, sensor) = name_fields
                sensor_name = prefix + "-" + sensor
                file_date = dateutil.parser.isoparse(date)
                if not self.is_date_in_time_interval(file_date):
                    continue
                if self.sensors is None or len(self.sensors) == 0 or sensor_name in self.sensors:
                    input_files_dict.setdefault(sensor_name, []).append(infile)
        # process files
        # pylint:disable = consider-using-dict-items
        for input_files_key in input_files_dict:
            output_file = self.out_files_dir + os.path.sep + input_files_key + ".csv"
            # Create panda dataframe for cvs export
            final_values = {}

            for input_file in input_files_dict[input_files_key]:
                try:
                    self.logger.info(f"Read {input_file}")
                    if read_techsas_file is None:  # implementation error, should never be null
                        raise RuntimeError("Coding error, techsas.read_techsas_file should not be null")

                    (time_sensor, values, frame_period) = read_techsas_file(input_file)

                    if len(time_sensor) == 0:
                        continue

                    time_min = max(time_sensor[0], nav_time[0])
                    time_max = min(time_sensor[-1], nav_time[-1])
                    if self.enable_time_interval:
                        time_min = max(to_utc(self.start_date), time_min)
                        time_max = min(to_utc(self.end_date), time_max)
                    time_mask = (time_sensor[:] >= time_min) & (time_sensor[:] <= time_max)
                    # compute time subset
                    if self.sampling > 0:
                        time_sampling = pd.date_range(
                            start=pd.Timestamp(time_min).ceil(freq=f"{self.sampling}S"),
                            end=pd.Timestamp(time_max).floor(freq=f"{self.sampling}S"),
                            freq=f"{self.sampling}S",
                        ).to_numpy()
                    else:
                        time_sampling = time_sensor[time_mask]

                    if time_sampling.size == 0:
                        self.logger.warning(f"No sample found for the desired sampling step or time interval.")
                        continue

                    final_values.setdefault("date_time", []).extend(time_sampling)

                    interpolated_longitudes = interpolate_values(
                        time_values=nav_time,
                        values=nav_longitudes,
                        time_out=time_sampling,
                        kind="previous",
                        frame_period_limit=5,
                    )
                    interpolated_latitudes = interpolate_values(
                        time_values=nav_time,
                        values=nav_latitudes,
                        time_out=time_sampling,
                        kind="previous",
                        frame_period_limit=5,
                    )

                    final_values.setdefault("long", []).extend(interpolated_longitudes)
                    final_values.setdefault("lat", []).extend(interpolated_latitudes)
                    # fig, axes = plt.subplots(1, 1+len(values))
                    # axes[0].plot(interpolated_longitudes, interpolated_latitudes)
                    # i = 0

                    for k in values.keys():
                        if self.sampling > 0:
                            interpolated_values = interpolate_values(
                                time_values=time_sensor,
                                values=values[k][1],
                                time_out=time_sampling,
                                kind="previous",
                                frame_period_limit=5 * frame_period,
                            )
                        else:
                            interpolated_values = values[k][1][time_mask]

                        final_values.setdefault(values[k][0], []).extend(interpolated_values)

                except Exception as err:
                    self.logger.error(f"Error occurred while processing {input_file}")
                    self.logger.error(f"Error message is {err}")
                    traceback.print_stack()
                    files_in_error.append(f"{input_file}")

            # Export current sensor dataframe to csv
            if final_values:
                df = pd.DataFrame(data=final_values)
                self.logger.info(f"Writing {final_values.keys()} in {output_file}")
                df.to_csv(output_file, index=False)

                # Merge current sensor dataframe in a global dataframe for global export
                if self.sampling > 0:
                    if all_df is None:
                        all_df = df
                    else:
                        all_df = pd.merge(
                            all_df,
                            df,
                            on=["date_time", "long", "lat"],
                            how="outer",
                            suffixes=("", f"({input_files_key}"),
                        )
        # Export global dataframe to all.csv
        if self.sampling > 0 and all_df is not None:
            all_output_file = self.out_files_dir + os.path.sep + "all.csv"
            self.logger.info(f"Writing {final_values.keys()} in {output_file}")
            all_df.to_csv(all_output_file, index=False)
        # print final results
        total_files = sum(len(x) for x in input_files_dict.values())
        if len(files_in_error) == 0:
            self.logger.info(f"{total_files} files converted")
        else:
            self.logger.error(f"{len(files_in_error)}/{total_files} files failed")
            self.logger.error(f"files in error {files_in_error}")

    def is_date_in_time_interval(self, file_date: datetime):
        if self.enable_time_interval:
            return self.start_date.date() <= file_date.date() <= self.end_date.date()
        else:
            return True

import locale
from datetime import datetime
from os import PathLike

import netCDF4 as nc
import numpy as np
import xarray as xr
from scipy import interpolate

from pytechsas.sensor.techsas_constant import HISTORY, TECHSAS_STATUS, TECHSAS_TIME

time_variable_name = "time"


def open_nc_file(file_path: PathLike | bytes | str, mode="r", nc_format="NETCDF4") -> nc.Dataset:
    """
    Opens a netCDF file after checking the path encoding.
    """
    return nc.Dataset(file_path, mode=mode, format=nc_format, encoding=locale.getpreferredencoding())


def read_techsas_file(file_name: str):
    """Reads a techsas file.
    :return a time variable, and a list of tuple containing the name of a column and the associated data
    """
    dictionary = {}
    with open_nc_file(file_name) as dataset:
        time = dataset.variables[time_variable_name]
        time_masked = time[:]
        time_values = nc.num2date(time_masked.compressed(), calendar=time.calendar, units=time.units)
        time_values = time_values.astype("datetime64[us]")
        frame_period = dataset.frame_period
        for v_key in dataset.variables.keys():
            if v_key != time_variable_name:
                data = dataset.variables[v_key]
                if data.shape[0] == time.size:
                    data.set_auto_mask(False)  # usefull to avoid valid_range parse error
                    cardinality = 1 if len(data.shape) == 1 else data.shape[1]
                    for i in range(cardinality):
                        if cardinality == 1:
                            data_key = v_key
                            data_masked = data[~np.ma.getmaskarray(time_masked)]
                        else:
                            data_key = f"{v_key}#{i}"
                            data_masked = data[~np.ma.getmaskarray(time_masked)][:, i]
                        name = f"{data_key}()"
                        dictionary[data_key] = (name, data_masked)  # add a new entry to the dictionary values
        return time_values, dictionary, frame_period


def read_times(dataset: nc.Dataset) -> np.ndarray:
    """Reads time values from TECHSAS file dataset."""
    time = dataset.variables[time_variable_name]
    time_masked = time[:]
    time_values = nc.num2date(time_masked.compressed(), calendar=time.calendar, units=time.units)
    time_values = time_values.astype("datetime64[us]")
    return time_values


def interpolate_values(
    time_values: np.ndarray, values: np.ndarray, time_out: np.ndarray, kind="previous", frame_period_limit=0
) -> np.ndarray:
    """Interpolates data on the given time_out.
    :param time_values: the sensor time vector
    :param values: the sensor values vector to interpolate
    :param time_out: the wanted interpolated time points to compute
    :param kind: interpolation method (see scipy.interpolate.interp1d)
    :param frame_period_limit: invalidate samples if time between the interpolated value and
        the source value is greater than this limit in seconds
    :return interpolated_values
    """
    if time_values[0] > time_out[0]:
        raise Exception(
            f"Not supported case : Starting date ({time_values[0]}) \
            from input file is higher than starting date of time out ({time_out[0]})"
        )

    if time_values[-1] < time_out[-1]:
        raise Exception(
            f"Not supported case : Last date  ({time_values[-1]}) \
            from input file is less than last date of time out ({time_out[-1]})"
        )

    # We need to change time to float in order to be able to interpolate data
    # We substract the lowest date as a reference date
    reference_date = time_out[0]
    time_out_float = (time_out - reference_date) / np.timedelta64(1, "s")
    time_values_float = (time_values - reference_date) / np.timedelta64(1, "s")
    interpolate_function = interpolate.interp1d(time_values_float, (values, time_values_float), kind=kind, copy=False)
    (interpolated_values, time_refs) = interpolate_function(time_out_float)
    # interpolate_time_function = interpolate.interp1d(time_values_float, time_values_float, kind=kind, copy=False)
    if frame_period_limit > 0:
        time_deltas = time_out_float - time_refs
        interpolated_values[time_deltas[:] > frame_period_limit] = np.nan
    return interpolated_values


def add_variable(techsas_file, name, long_name, datatype, unit, values) -> None:
    """
    Creates or edits variable into the TECHSAS file.
    :param techsas_file: file to edit.
    :param name: variable's name.
    :param long_name: variable's long name (description).
    :param datatype: variable's NetCDF datatype.
    :param unit: variable's unit.
    :param values: data values.
    """
    with open_nc_file(techsas_file, mode="r+") as dataset:
        if name in dataset.variables:
            dataset.variables[name][:] = values
        else:
            variable = dataset.createVariable(name, datatype, ("time",))
            variable.units = unit
            variable.long_name = long_name
            variable[:] = values


def add_heading(techsas_file, heading_values):
    add_variable(techsas_file, "heading", "Platform heading", np.float64, "degrees", heading_values)


def add_speed(techsas_file, speed_values):
    add_variable(techsas_file, "speed", "Platform speed", np.float64, "knot", speed_values)


def add_navigation_to_techsas_file(
    techsas_file, interpolated_latitudes, interpolated_longitudes, interpolated_altitudes=None
):
    """
    add variable lat, lon and alt (optional) to a techsas file (o_path)
    @param:
    interpolated_altitudes: interpolated altitudes
    interpolated_latitudes: interpolated latitudes
    interpolated_longitudes: interpolated longitudes
    techsas_file: file from techsas
    """
    add_variable(techsas_file, "lat", "latitude", np.float64, "degrees_north", interpolated_latitudes)
    add_variable(techsas_file, "lon", "longitude", np.float64, "degrees_north", interpolated_longitudes)
    if interpolated_altitudes:
        add_variable(techsas_file, "alt", "alt", np.float64, "m", interpolated_altitudes)


def add_history(
    new_ds: xr.Dataset | nc.Dataset,
    module_name: str,
    history_info: str,
    origin_ds: xr.Dataset | nc.Dataset | None = None,
) -> None:
    """
    Adds info in history global attribute.
    :param origin_ds: origin dataset, used to keep previous history
    :param new_ds: dataset to edit
    :param module_name: module which applies modification
    :param history_info: info to add
    """
    if origin_ds is None:
        origin_ds = new_ds

    if isinstance(origin_ds, xr.Dataset):
        history = origin_ds.attrs[HISTORY] + "\n" if HISTORY in origin_ds.attrs else ""
    elif isinstance(origin_ds, nc.Dataset):
        history = origin_ds.getncattr(HISTORY) + "\n" if HISTORY in origin_ds.ncattrs() else ""
    else:
        raise ValueError("Invalid input dataset.")

    history += f"{datetime.now().isoformat()} : [{module_name}] {history_info}"

    if isinstance(new_ds, xr.Dataset):
        new_ds.attrs[HISTORY] = history
    elif isinstance(new_ds, nc.Dataset):
        new_ds.setncattr(HISTORY, history)


def set_status(i_path: str, status_arr: np.array) -> None:
    """
    Writes status variable with provided values.
    :param i_path: TECHSAS file to edit.
    :param status_arr: status values.
    """
    # compute status flags (merge with existing variable if exists)
    status_values = np.zeros(len(status_arr), dtype=np.int8)
    with xr.open_dataset(i_path) as ds:
        if TECHSAS_STATUS in ds:
            status_values = ds[TECHSAS_STATUS].values
    status_values = np.where(status_arr, status_values, status_values | status_arr)

    # write status variable
    status_da = xr.DataArray(
        data=status_values,
        dims=TECHSAS_TIME,
        attrs={
            "long_name": "status",
            "flag_meanings": "rejected_by_used, rejected_by_turn_filter",
            "flag_mask": "1b,2b,4b,8b,16b,32b,64b",
            "comment": "status flag : if one of the byte field is set (value != 0) the data are considered as invalid",
        },
    )
    status_ds = xr.Dataset(data_vars={TECHSAS_STATUS: status_da})
    status_ds.to_netcdf(path=i_path, mode="a")

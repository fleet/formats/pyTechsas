import logging as log
import os
import shutil
from datetime import datetime
from typing import List

import numpy as np
import xarray as xr

from pytechsas.sensor.techsas_constant import (
    TECHSAS_SPEED,
    TECHSAS_LATITUDE,
    TECHSAS_HEADING,
    TECHSAS_GRAVITY,
    TECHSAS_GND_COURSE,
    LONG_NAME,
    UNITS,
    TECHSAS_GND_SPEED,
)
from pytechsas.sensor.techsas_file import add_history

# CONSTANTS
THEORETIC_GRAVITY_NAME = "computed_theoretic_gravity"
THEORETIC_GRAVITY_LONG_NAME = "Theoretic gravity computed with PyTechsas"

EOTVOS_CORRECTION_NAME = "computed_eotvos_correction"
EOTVOS_CORRECTION_LONG_NAME = "Eotvos correction computed with PyTechsas"

DRIFTED_GRAVITY_NAME = "computed_drifted_gravity"
DRIFTED_GRAVITY_LONG_NAME = "Drifted gravity computed with PyTechsas"

GRAVITY_NAME = "computed_corrected_gravity"
GRAVITY_LONG_NAME = "Absolute gravity computed with PyTechsas"

FREE_AIR_ANOMALY_NAME = "computed_free_air_anomaly"
FREE_AIR_ANOMALY_LONG_NAME = "Free air anomaly gravity computed with PyTechsas"

MILLIGAL = "mgal"

logger = log.getLogger()


def __compute_drift__(
    ref_date_1: datetime,
    ref_value_1: float,
    ref_measure_1: float,
    ref_date_2: datetime,
    ref_value_2: float,
    ref_measure_2: float,
    spring_stiffness_corr: float = 1,
) -> (datetime, float, float):
    """
    Computes drift parameters from gravity references.
    """
    if spring_stiffness_corr != 1:
        # apply stiffness correction on reference measures
        ref_measure_1 = spring_stiffness_corr * ref_measure_1
        ref_measure_2 = spring_stiffness_corr * ref_measure_2

    if ref_date_1 == ref_date_2:
        raise Exception("Reference points must have different dates.")

    drift = ((ref_value_2 - ref_measure_2) - (ref_value_1 - ref_measure_1)) / (ref_date_2 - ref_date_1).total_seconds()
    g_zero = ref_value_1 - ref_measure_1
    g_zero_date = ref_date_1
    return g_zero_date, g_zero, drift


def __compute_drifted_gravity__(
    g_observed_values: xr.DataArray,
    g_zero,
    g_zero_date: datetime,
    drift: float,
) -> xr.DataArray:
    """
    Computes gravity from a reference (value and date) and a drift value.
    :param g_observed_values: gravity observed by sensor
    :param g_zero: offset to get absolute gravity
    :param g_zero_date: date when the offset has been determined
    :param drift: temporal drift to apply (in seconds)
    :return: absolute gravity
    """
    # compute gravity from  drift since g_zero
    duration_since_g_zero = (g_observed_values.coords["time"] - np.datetime64(g_zero_date)) / np.timedelta64(1, "s")
    g_drifted = drift * duration_since_g_zero[:]
    return g_zero + g_observed_values + g_drifted


def __compute_eotvos_correction__(
    latitude_rad: xr.DataArray,
    heading_rad: xr.DataArray,
    speed: xr.DataArray,
) -> xr.DataArray:
    """Compute Eotvos correction : CE  = 7.5029 x speed x cos(lat) x sin(heading) + 0.0042 x speed²"""
    return 7.5029 * speed * np.cos(latitude_rad) * np.sin(heading_rad) + 0.0042 * speed**2


def __compute_theoretic_gravity__(
    latitude_rad: xr.DataArray,
) -> xr.DataArray:
    """Compute theroretic gravity in mgal : theoretic_gravity  = 978032.7 + 5185.92 x sin²(lat) - 5.67 x sin²(2 x lat)²"""
    return 978032.7 + 5185.92 * np.sin(latitude_rad) ** 2 - 5.67 * np.sin(2 * latitude_rad) ** 2


def process_gravity_ds(
    ds: xr.Dataset,
    drift: float,
    g_zero: float,
    g_zero_date,
    params_dict: dict = None,
    spring_stiffness_corr: float = 1,
    filter_delay: float = 0,
) -> xr.Dataset | None:
    """
    Processes gravity on a TECHSAS gravity file. Computes theroetic gravity, absolute gravity, eotvos and free air anomaly.
    :param ds: input dataset from gravity file.
    :param spring_stiffness_corr: spring stiffness correction to apply on measured values.
    :param filter_delay: filter delay to apply on dates.
    :param drift: drift from g_zero in mgal/sec.
    :param g_zero: g_zero offset, used to compute absolute gravity from measured values.
    :param g_zero_date: g_zero date.
    :param params_dict: dictionary of parameters to be saved in variable attributes.
    """
    # get navigation data
    if TECHSAS_LATITUDE in ds:
        lat = np.deg2rad(ds[TECHSAS_LATITUDE])  # in radian
    else:
        logger.error(f"No {TECHSAS_LATITUDE} variable found.")
        return None

    speed = ds[TECHSAS_GND_SPEED] if TECHSAS_GND_SPEED in ds else ds[TECHSAS_SPEED] if TECHSAS_SPEED in ds else None
    if speed is None:
        logger.error(f"No {TECHSAS_GND_SPEED} or {TECHSAS_SPEED} variable found.")
        return None

    if TECHSAS_GND_COURSE in ds:
        heading = np.deg2rad(ds[TECHSAS_GND_COURSE])  # in radian
    elif TECHSAS_HEADING in ds:
        heading = np.deg2rad(ds[TECHSAS_HEADING])  # in radian
        logger.warning(f"'{TECHSAS_GND_COURSE}' variable not found, '{TECHSAS_HEADING}' used instead.")
    else:
        logger.error(f"No {TECHSAS_GND_COURSE} or {TECHSAS_HEADING} variable found.")
        return None

    result_ds = xr.Dataset()

    # compute theroetic_gravity, eotvos_correction & drifted_gravity
    logger.info(f"Compute theoretic gravity...")
    result_ds[THEORETIC_GRAVITY_NAME] = __compute_theoretic_gravity__(lat)
    result_ds[THEORETIC_GRAVITY_NAME].attrs = {
        LONG_NAME: THEORETIC_GRAVITY_LONG_NAME,
        UNITS: MILLIGAL,
    }

    # compute eotvos correction
    logger.info(f"Compute eotvos correction...")
    result_ds[EOTVOS_CORRECTION_NAME] = __compute_eotvos_correction__(lat, heading, speed)
    result_ds[EOTVOS_CORRECTION_NAME].attrs = {
        LONG_NAME: EOTVOS_CORRECTION_LONG_NAME,
        UNITS: MILLIGAL,
    }

    logger.info(f"Compute gravity...")
    result_ds[TECHSAS_GRAVITY] = ds[TECHSAS_GRAVITY]

    # apply spring stiffness coefficient correction
    if spring_stiffness_corr != 1:
        logger.info(f"Apply spring stiffness correction : {spring_stiffness_corr}")
        result_ds[TECHSAS_GRAVITY] = spring_stiffness_corr * result_ds[TECHSAS_GRAVITY]

    # apply filter delay on observed gravity (in seconds)
    if filter_delay != 0:
        time_delta = np.timedelta64(int(filter_delay), "s")
        logger.info(f"Apply filter delay on measured gravity : {time_delta}...")
        result_ds[TECHSAS_GRAVITY] = result_ds[TECHSAS_GRAVITY].assign_coords(time=ds.time - time_delta)
        result_ds.attrs["filter_delay"] = filter_delay
        result_ds[TECHSAS_GRAVITY] = result_ds[TECHSAS_GRAVITY].interp_like(ds)

    # compute drifted gravity
    drift_per_day = drift * 60 * 60 * 24
    logger.info(f"Drifted with : offset = {g_zero}, date = {g_zero_date}, drift = {drift_per_day} mgal/day")
    result_ds[DRIFTED_GRAVITY_NAME] = __compute_drifted_gravity__(
        result_ds[TECHSAS_GRAVITY], g_zero, g_zero_date, drift
    )
    result_ds[DRIFTED_GRAVITY_NAME].attrs = {
        LONG_NAME: DRIFTED_GRAVITY_LONG_NAME,
        UNITS: MILLIGAL,
    }

    if params_dict is not None:
        for k, v in params_dict.items():
            result_ds[DRIFTED_GRAVITY_NAME].attrs[k] = str(v)

    # corrected_gravity = drifted_gravity + eotvos_correction
    result_ds[GRAVITY_NAME] = result_ds[DRIFTED_GRAVITY_NAME] + result_ds[EOTVOS_CORRECTION_NAME]
    result_ds[GRAVITY_NAME].attrs = {
        LONG_NAME: GRAVITY_NAME,
        UNITS: MILLIGAL,
    }

    # free_air_anomaly = measured_gravity - theoretic_gravity
    logger.info(f"Compute free air anomaly...")
    result_ds[FREE_AIR_ANOMALY_NAME] = result_ds[GRAVITY_NAME] - result_ds[THEORETIC_GRAVITY_NAME]
    result_ds[FREE_AIR_ANOMALY_NAME].attrs = {
        LONG_NAME: FREE_AIR_ANOMALY_LONG_NAME,
        UNITS: MILLIGAL,
    }

    # update history attribute
    add_history(
        origin_ds=ds,
        new_ds=result_ds,
        module_name=os.path.basename(__file__),
        history_info=f"Process gravity with parameters : {params_dict}",
    )

    return result_ds


def process_gravity(
    gravity_file_path: str,
    spring_stiffness_corr: float = 1,
    filter_delay: float = 0,
    drift: float = None,
    g_zero: float = None,
    g_zero_date=None,
    params_dict=None,
):
    """
    Processes gravity on a TECHSAS gravity file. Computes theroetic gravity, absolute gravity, eotvos and free air anomaly.
    :param gravity_file_path: file to process.
    :param spring_stiffness_corr: spring stiffness correction to apply on measured values.
    :param filter_delay: filter delay to apply on dates.
    :param drift: drift from g_zero in mgal/sec.
    :param g_zero: g_zero offset, used to compute absolute gravity from measured values.
    :param g_zero_date: g_zero date.
    :param params_dict: dictionary of parameters to be saved in variable attributes.
    """
    logger.info(f"Start gravity file processing : {gravity_file_path}...")
    with xr.open_dataset(gravity_file_path) as ds:
        result_ds = process_gravity_ds(
            ds=ds,
            spring_stiffness_corr=spring_stiffness_corr,
            filter_delay=filter_delay,
            drift=drift,
            g_zero=g_zero,
            g_zero_date=g_zero_date,
            params_dict=params_dict,
        )
    if result_ds is not None:
        logger.info(f"Save in {gravity_file_path}...")
        result_ds.to_netcdf(path=gravity_file_path, mode="a")


def process_gravity_files_with_ref(
    i_paths: List[str],
    o_paths: List[str],
    ref_date_1: datetime,
    ref_value_1: float,
    ref_measure_1: float,
    ref_date_2: datetime,
    ref_value_2: float,
    ref_measure_2: float,
    filter_delay: int = 0,
    real_spring_stiffness: float = 1,
    install_spring_stiffness: float = 1,
    overwrite: bool = False,
):
    """
    Processes a list of TECHSAS gravity files with references points.
    :param i_paths: input file paths
    :param o_paths: ouput file paths
    :param ref_date_1: date of first gravity reference
    :param ref_value_1: absolute value of first gravity reference
    :param ref_measure_1: sensor measure at the first gravity reference
    :param ref_date_2: date of first gravity reference
    :param ref_value_2: absolute value of first gravity reference
    :param ref_measure_2: sensor measure at the first gravity reference
    :param filter_delay: filter delay to apply
    :param real_spring_stiffness: real value of spring stiffness
    :param install_spring_stiffness: installation value of spring stiffness
    :param overwrite: if true, allows to overwrite output files
    """

    # dictionary to save applied parameters
    params = {
        "reference_datetime_1": ref_date_1.isoformat(),
        "reference_value_1": ref_value_1,
        "reference_measured_value_1": ref_measure_1,
        "reference_datetime_2": ref_date_2.isoformat(),
        "reference_value_2": ref_value_2,
        "reference_measured_value_2": ref_measure_2,
    }

    logger.info(f"Compute drift and offset to apply from reference point...")
    spring_stiffness_corr = real_spring_stiffness / install_spring_stiffness
    g_zero_date, g_zero, drift = __compute_drift__(
        ref_date_1, ref_value_1, ref_measure_1, ref_date_2, ref_value_2, ref_measure_2, spring_stiffness_corr
    )
    drift = drift * (24 * 60 * 60)  # pass drift in  mgal/sec to mgal/day

    process_gravity_files(
        i_paths,
        o_paths,
        g_zero_date,
        g_zero,
        drift,
        filter_delay,
        real_spring_stiffness,
        install_spring_stiffness,
        overwrite,
        params,
    )


def process_gravity_files(
    i_paths: List[str],
    o_paths: List[str],
    g_zero_date: datetime = None,
    g_zero: float = None,
    drift: float = None,
    filter_delay: int = 0,
    real_spring_stiffness: float = 1,
    install_spring_stiffness: float = 1,
    overwrite: bool = False,
    params=None,
):
    """
    Processes a list of TECHSAS gravity files with drift parameters.
    :param i_paths: input file paths
    :param o_paths: ouput file paths
    :param drift: drift to apply from g_zero (mgal/day)
    :param g_zero: g_zero offset, used to compute absolute gravity from measures.
    :param g_zero_date: date indicating when the offset has been computed
    :param filter_delay: filter delay to apply
    :param real_spring_stiffness: real value of spring stiffness
    :param install_spring_stiffness: installation value of spring stiffness
    :param overwrite: if true, allows to overwrite output files
    :param params: optional dict with parameters to save in history
    """
    # dictionary to save applied parameters
    if params is None:
        params = {}
    params["real_spring_stiffness"] = real_spring_stiffness
    params["install_spring_stiffness"] = install_spring_stiffness

    # compute spring stiffness correction (factor between install and real value)
    spring_stiffness_corr = real_spring_stiffness / install_spring_stiffness
    if spring_stiffness_corr != 1:
        logger.info(
            f"Apply spring stiffness correction : {spring_stiffness_corr} (real: {real_spring_stiffness}; install: {install_spring_stiffness})"
        )

    params["drift_mgal_day"] = drift
    params["g_zero"] = g_zero
    params["g_zero_date"] = g_zero_date
    drift = drift / (24 * 60 * 60)  # pass drift in mgal/day to mgal/sec

    # process gravity for each file
    for i_path, o_path in zip(i_paths, o_paths):
        # Copy input file to output path"
        if not overwrite and os.path.exists(o_path):
            logger.warning(f"File {o_path} already exists, skipping it")
            continue
        if o_path != i_path:
            shutil.copy(i_path, o_path)
        process_gravity(
            gravity_file_path=o_path,
            spring_stiffness_corr=spring_stiffness_corr,
            filter_delay=filter_delay,
            drift=drift,
            g_zero=g_zero,
            g_zero_date=g_zero_date,
            params_dict=params,
        )


def compute_corr_gravity_and_faa_files(i_paths: List[str], o_paths: List[str], overwrite: bool = False):
    for i_path, o_path in zip(i_paths, o_paths):
        # Copy input file to output path"
        if not overwrite and os.path.exists(o_path):
            logger.warning(f"File {o_path} already exists, skipping it")
            continue
        if o_path != i_path:
            shutil.copy(i_path, o_path)
        compute_corr_gravity_and_faa(gravity_file_path=o_path)


def compute_corr_gravity_and_faa(gravity_file_path: str):
    logger.info(f"Compute corrected gravity and Free Air Anomaly for file : {gravity_file_path}...")
    with xr.open_dataset(gravity_file_path) as ds:
        if DRIFTED_GRAVITY_NAME not in ds or EOTVOS_CORRECTION_NAME not in ds or THEORETIC_GRAVITY_NAME not in ds:
            logger.error(
                f"File must have been already processed (and contain drifted gravity, eotvos and theororetic gravity."
            )

        result_ds = xr.Dataset()

        # corrected_gravity = drifted_gravity + eotvos_correction
        logger.info(f"Compute corrected gravity...")
        result_ds[GRAVITY_NAME] = ds[DRIFTED_GRAVITY_NAME] + ds[EOTVOS_CORRECTION_NAME]
        result_ds[GRAVITY_NAME].attrs = {
            LONG_NAME: GRAVITY_NAME,
            UNITS: MILLIGAL,
        }

        # free_air_anomaly = measured_gravity - theoretic_gravity
        logger.info(f"Compute free air anomaly...")
        result_ds[FREE_AIR_ANOMALY_NAME] = result_ds[GRAVITY_NAME] - ds[THEORETIC_GRAVITY_NAME]
        result_ds[FREE_AIR_ANOMALY_NAME].attrs = {
            LONG_NAME: FREE_AIR_ANOMALY_LONG_NAME,
            UNITS: MILLIGAL,
        }

        # update history attribute
        add_history(
            origin_ds=ds,
            new_ds=result_ds,
            module_name=os.path.basename(__file__),
            history_info=f"Recompute corrected gravity and free air anomaly",
        )

    logger.info(f"Save in {gravity_file_path}...")
    result_ds.to_netcdf(path=gravity_file_path, mode="a")

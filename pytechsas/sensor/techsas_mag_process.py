import logging as log
import os
import shutil
from datetime import datetime
from typing import List

import numpy as np
import pyIGRF
import pyproj
import xarray as xr

from pytechsas.sensor.techsas_constant import (
    TECHSAS_LATITUDE,
    TECHSAS_LONGITUDE,
    TECHSAS_MAG,
    TECHSAS_TIME,
    UNITS,
    LONG_NAME,
)
from pytechsas.sensor.techsas_file import add_history

# CONSTANTS
THEORETIC_MAGNETISM_NAME = "computed_theoretic_magnetism"
THEORETIC_MAGNETISM_LONG_NAME = "Theoretic magnetism computed with pyIGRF"
MAGNETIC_ANOMALY_NAME = "computed_magnetic_anomaly"
MAGNETIC_ANOMALY_LONG_NAME = "Magnetic anomaly (difference between measured and theoretic)"
GAMMA = "gamma"

logger = log.getLogger()


def __apply_cable_out__(longitudes: np.ndarray, latitudes: np.ndarray, cable_out: float) -> (np.ndarray, np.ndarray):
    """
    Computes shift position with the provided cable out distance.
    :param longitudes: origin longitudes
    :param latitudes: origin latitudes
    :param cable_out: cable length in meters
    :return: longitudes and latitudes shifted with the cable length
    """
    # initialize new position
    new_longitudes = np.empty(longitudes.size)
    new_longitudes[:] = np.nan
    new_latitudes = np.empty(latitudes.size)
    new_latitudes[:] = np.nan

    # compute dist between positions
    dists = __compute_distance__(longitudes, latitudes)

    for i in range(len(longitudes) - 1, -1, -1):
        # get position before and after cable out distance
        index_after = i  # start from current index
        index_before = i
        distance_before = 0
        distance_after = 0
        # go back until the distance is exceeded
        while distance_after < cable_out and index_after > 0:
            distance_before = distance_after
            distance_after += dists[index_after - 1]
            index_before = index_after
            index_after -= 1

        # compute position at cable out distance by interpolation
        if distance_after > cable_out:
            coeff = (cable_out - distance_before) / (distance_after - distance_before)
            new_longitudes[i] = longitudes[index_before] + coeff * (longitudes[index_after] - longitudes[index_before])
            new_latitudes[i] = latitudes[index_before] + coeff * (latitudes[index_after] - latitudes[index_before])

    return new_longitudes, new_latitudes


def __compute_distance__(longitudes: np.ndarray, latitudes: np.ndarray) -> np.ndarray:
    """
    Returns an array with the distance between two consecutive positions
    """
    start_longitudes = longitudes[:-1]
    end_longitudes = longitudes[1:]
    start_latitudes = latitudes[:-1]
    end_latitudes = latitudes[1:]
    geodesic = pyproj.Geod(ellps="WGS84")
    _, _, distances = geodesic.inv(start_longitudes, start_latitudes, end_longitudes, end_latitudes)
    return distances


def __compute_theoretic_magnetism__(latitude: xr.DataArray, longitude: xr.DataArray, year: int) -> xr.DataArray:
    """
    Computes theoretic magnetism with pyIGRF.
    """
    return xr.apply_ufunc(
        lambda lat, lon: pyIGRF.igrf_value(lat, lon, year=year)[6], latitude, longitude, vectorize=True
    )


def process_mag_ds(mag_ds: xr.Dataset, cable_out: float = 0) -> xr.Dataset:
    """
    Processes magnetism on a TECHSAS mag file:
        - apply cable out
        - compute theoretic magnetism
        - compute magnetic anomaly
    :param mag_ds: input dataset to process
    :param cable_out: cable length to apply
    """
    result_ds = xr.Dataset()
    lon = mag_ds[TECHSAS_LONGITUDE]
    lat = mag_ds[TECHSAS_LATITUDE]

    # apply cable out : update longitude and latitude
    if cable_out is not None and cable_out != 0:
        logger.info(f"Apply cable out : {cable_out} ...")
        lon.data, lat.data = __apply_cable_out__(lon.data, lat.data, cable_out)
        result_ds[TECHSAS_LONGITUDE] = lon
        result_ds[TECHSAS_LATITUDE] = lat
        result_ds.attrs["cable_out_in_meters"] = cable_out

    # compute theoretic magnetism with pyIGRF
    # first, get mean date as float year
    mean_datetime = datetime.fromisoformat(np.datetime_as_string(mag_ds[TECHSAS_TIME].mean(), unit="s"))
    start_of_year = datetime(mean_datetime.year, 1, 1)
    end_of_year = datetime(mean_datetime.year + 1, 1, 1)
    year_float = mean_datetime.year + (mean_datetime - start_of_year) / (end_of_year - start_of_year)
    logger.info(f"Compute theoretic magnetism with pyIGRF (year : {year_float})...")
    result_ds[THEORETIC_MAGNETISM_NAME] = __compute_theoretic_magnetism__(lat, lon, year_float)
    result_ds[THEORETIC_MAGNETISM_NAME].attrs = {LONG_NAME: THEORETIC_MAGNETISM_LONG_NAME, UNITS: GAMMA}

    # compute magnetic anomaly
    logger.info(f"Compute magnetic anomaly...")
    result_ds[MAGNETIC_ANOMALY_NAME] = mag_ds[TECHSAS_MAG] - result_ds[THEORETIC_MAGNETISM_NAME]
    result_ds[MAGNETIC_ANOMALY_NAME].attrs = {LONG_NAME: MAGNETIC_ANOMALY_LONG_NAME, UNITS: GAMMA}

    # update history attribute
    add_history(
        origin_ds=mag_ds,
        new_ds=result_ds,
        module_name=os.path.basename(__file__),
        history_info=f"Process magnetism with cable_out : {cable_out}",
    )

    return result_ds


def process_mag(
    mag_file_path: str,
    cable_out: float = 0,
) -> None:
    """
    Processes magnetism on a TECHSAS mag file:
        - apply cable out
        - compute theoretic magnetism
        - compute magnetic anomaly
    :param mag_file_path: file to process
    :param cable_out: cable length to apply
    """
    logger.info(f"Start magnetism file processing : {mag_file_path}...")
    with xr.open_dataset(mag_file_path) as ds:
        result_ds = process_mag_ds(mag_ds=ds, cable_out=cable_out)

    logger.info(f"Save in {mag_file_path}...")
    result_ds.to_netcdf(path=mag_file_path, mode="a")


def process_mag_files(
    i_paths: List[str],
    o_paths: List[str],
    cable_out: float = None,
    overwrite: bool = False,
) -> None:
    """
    Processes a list of TECHSAS magnetism files.
    :param i_paths: input file paths
    :param o_paths: ouput file paths
    :param cable_out: cable length to apply
    :param overwrite: if true, allows to overwrite output files
    """
    # process magnetism for each file
    for i_path, o_path in zip(i_paths, o_paths):
        # Copy input file to output path"
        if not overwrite and os.path.exists(o_path):
            logger.warning(f"File {o_path} already exists, skipping it")
            continue
        if o_path != i_path:
            shutil.copy(i_path, o_path)
        process_mag(mag_file_path=o_path, cable_out=cable_out)

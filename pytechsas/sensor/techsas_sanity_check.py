import csv
import logging as log
import os
from datetime import datetime
from typing import List, Union

import numpy as np
import pandas as pd
import xarray as xr

from pytechsas.sensor.techsas_constant import TECHSAS_LONGITUDE, TECHSAS_LATITUDE, TECHSAS_STATUS
from pytechsas.sensor.techsas_file import set_status

logger = log.getLogger()


class Anomaly:
    """
    This class represents an anomaly (error, invalid data...) in TECHSAS file.
    """

    def __init__(
        self,
        anomaly: str,
        index: int,
        time: datetime,
        description: str,
        latitude: float = None,
        longitude: float = None,
        file: str = None,
    ):
        self.anomaly = anomaly
        self.index = index
        self.time = time
        self.description = description
        self.latitude = latitude
        self.longitude = longitude
        self.file = file

    def __str__(self) -> str:
        return str(self.__dict__)


def __check_duplicates__(time_arr: np.array) -> List[Anomaly]:
    """
    Identifies duplicated time values.
    @param time_arr: time values
    @return: a list of Anomaly
    """
    logger.info("Check duplicates...")
    result = []
    unique_ar, count = np.unique(time_arr, return_counts=True)
    duplicates = unique_ar[count > 1]
    for duplicated_time in duplicates:
        dup_indexes = np.where(time_arr == duplicated_time)[0]
        for i in dup_indexes:
            desc = f"Duplicated time {duplicated_time} at indexes : {dup_indexes}"
            result.append(Anomaly("duplicate", i, duplicated_time, desc))
    logger.info("No duplicate found." if len(result) == 0 else f"{len(result)} duplicate(s) found.")
    return result


def __check_leaps_back__(time_arr: np.array) -> List[Anomaly]:
    """
    Intentifies leaps back in time array.
    @param time_arr: time values
    @return: a list of Anomaly
    """
    logger.info("Check leaps back...")
    result = []
    back_arr = np.where(np.diff(time_arr) < pd.Timedelta(0))[0]
    for i in back_arr:
        desc = f"leaps back between indexes {i} and {i + 1} :  {time_arr[i]} => {time_arr[i + 1]}"
        result.append(Anomaly("leaps_back", i + 1, time_arr[i + 1], desc))
    logger.info("No leaps back found." if len(result) == 0 else f"{len(result)} leaps back found.")
    return result


def __check_interruptions__(time_arr: np.array, threshold: np.timedelta64) -> List[Anomaly]:
    """
    Identifies interruptions from time files.
    @param time_arr: time values
    @param threshold: an interruption is detected if the duration between two time values is over this delay
    @return: a list of Anomaly
    """
    logger.info(f"Check interruptions (threshold : {threshold})...")
    result = []
    interruptions = np.where(np.diff(time_arr) > threshold)[0]
    for i in interruptions:
        desc = f"Interruption between indexes {i} and {i + 1} :  {time_arr[i]} => {time_arr[i + 1]} : {pd.Timedelta(time_arr[i + 1] - time_arr[i])}"
        result.append(Anomaly("interruption", i, time_arr[i], desc))
    logger.info("No interruption found." if len(result) == 0 else f"{len(result)} interruption(s) found.")
    return result


def __check_time__(time_arr: np.ndarray, interruption_threshold_in_sec: int = 30) -> List[Anomaly]:
    """
    Finds anomalies in times.
    @return: a list of Anomaly
    """
    logger.info(f"Check anomalies on {len(time_arr)} time values...")
    result = []
    result.extend(__check_duplicates__(time_arr))
    result.extend(__check_leaps_back__(time_arr))
    result.extend(__check_interruptions__(time_arr, np.timedelta64(int(interruption_threshold_in_sec), "s")))
    return result


def __check_position__(lat_arr: np.ndarray, lon_arr: np.ndarray, time_arr: np.ndarray) -> List[Anomaly]:
    """
    Finds invalid positions.
    @param lat_arr: latitude values
    @param lon_arr: longitude values
    @return: a list of Anomaly
    """
    logger.info(f"Check anomalies on {len(lat_arr)} positions...")
    result: List[Anomaly] = []
    invalid_lat_indexes = np.where((~np.isfinite(lat_arr)) | (-90 > lat_arr) | (lat_arr > 90))[0]
    invalid_lon_indexes = np.where((~np.isfinite(lon_arr)) | (-180 > lon_arr) | (lon_arr > 180))[0]
    invalid_positions = np.sort(np.unique(np.concatenate((invalid_lat_indexes, invalid_lon_indexes))))
    for i in invalid_positions:
        lat = lat_arr[i]
        lon = lon_arr[i]
        desc = f"Invalid position at index {i} :  latitude = {lat_arr[i]} ; longitude = {lon_arr[i]}"
        result.append(Anomaly("invalid_position", i, time_arr[i], desc, lat, lon))
    logger.info("No invalid position found." if len(result) == 0 else f"{len(result)} invalid(s) position.")
    return result


def __write_output_file__(anomalies: List[Anomaly], o_path: str, overwrite: bool = False):
    """
    Writes anomalies in CSV file.
    """
    if not overwrite and os.path.exists(o_path):
        logger.warning(f"Can't write output file : {o_path} already exists and overwrite option is false.")
        return
    with open(o_path, "w", newline="", encoding="utf-8") as csvfile:
        fieldnames = ["file", "index", "time", "latitude", "longitude", "anomaly", "description"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, extrasaction="ignore", delimiter=";")
        writer.writeheader()
        writer.writerows([a.__dict__ for a in anomalies])


def __check_anomalies__(
    i_path: str, interruption_threshold: int = 30, ignore_invalid_status=False, edit_input_file_status=False
) -> List[Anomaly]:
    """
    Researches anomalies in a TECHSAS file (duplicates, leap back, interruption, invalid position)
    @param i_path: file to check
    @param interruption_threshold: interruption delay
    @param ignore_invalid_status: if true, ignore data with invalid status
    @param edit_input_file_status: if true, edit input file status variable
    @return: Anomaly list
    """
    anomalies: List[Anomaly] = []

    with xr.open_dataset(i_path) as ds:
        # get time array (filter invalid status if asked)
        time_arr = (
            ds.time.values[ds.status == 0] if (ignore_invalid_status and TECHSAS_STATUS in ds) else ds.time.values
        )

        # search anomalies in times
        anomalies.extend(__check_time__(time_arr, interruption_threshold))

        if TECHSAS_LATITUDE in ds and TECHSAS_LONGITUDE in ds:
            lat_arr = (
                ds[TECHSAS_LATITUDE].values[ds.status == 0]
                if (ignore_invalid_status and TECHSAS_STATUS in ds)
                else ds[TECHSAS_LATITUDE].values
            )
            lon_arr = (
                ds[TECHSAS_LONGITUDE].values[ds.status == 0]
                if (ignore_invalid_status and TECHSAS_STATUS in ds)
                else ds[TECHSAS_LONGITUDE].values
            )
            # add position to other anomalies
            for anomaly in anomalies:
                anomaly.latitude = lat_arr[anomaly.index]
                anomaly.longitude = lon_arr[anomaly.index]
            # search anomalies in positions
            anomalies.extend(__check_position__(lat_arr, lon_arr, time_arr))

    # add position to other anomalies
    for anomaly in anomalies:
        anomaly.file = os.path.basename(i_path)

    logger.info(f"Result : {len(anomalies)} anomalies on {len(time_arr)}")
    for anomaly in anomalies:
        logger.info(f"[{anomaly.index}] {anomaly.anomaly} ({anomaly.description})")

    # edit status variable
    if edit_input_file_status:
        status_values = np.zeros(len(time_arr), dtype=np.int8)
        status_values[[a.index for a in anomalies]] = 1
        set_status(i_path, status_values)

    return anomalies


def sanity_check(
    i_paths: Union[List[str], str],
    o_path: str = None,
    overwrite: bool = False,
    interruption_threshold: int = 30,
    ignore_invalid_status=False,
    edit_input_file_status=False,
):
    """
    Applies sanity check : researches anomalies in time and position variables.
    @param i_paths: input file(s) to check.
    @param o_path: output CSV file path (.csv) (filled with result anomalies)
    @param overwrite: option to overwrite output file if already exist
    @param interruption_threshold: minimum delay to consider an interruption
    @param ignore_invalid_status: if true, ignore data with invalid status
    @param edit_input_file_status: if true, edit input file status variable
    @return: dictionary with anomalies
    """
    if isinstance(i_paths, str):
        i_paths = [i_paths]

    anomalies: List[Anomaly] = []
    for i_path in i_paths:
        anomalies.extend(
            __check_anomalies__(
                i_path=i_path,
                interruption_threshold=interruption_threshold,
                ignore_invalid_status=ignore_invalid_status,
                edit_input_file_status=edit_input_file_status,
            )
        )

    # save anomalies in output CSV file
    if o_path is not None and anomalies:
        __write_output_file__(anomalies, o_path, overwrite)

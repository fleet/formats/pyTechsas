"""
Provide some numpy basic functions and wrapper
"""

from datetime import datetime

import numpy as np


def to_utc(in_datetime: datetime) -> np.datetime64:
    """Convert a naive or aware datetime to np datetime64 in UTC timezone
    @:param in_datetime: the naive/aware datetime to convert
    @:return datetime64 object
    """
    return np.datetime64(datetime.utcfromtimestamp(in_datetime.timestamp()))

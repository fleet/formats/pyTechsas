import logging as log
import math

import numpy as np

logger = log.getLogger()


def apply_qtinars_bessel(input_arr: np.array, fs2=1) -> np.array:
    """
    Applies 3 Bessel filters (one of the first order, and two of second order).
    """
    sensor_delay = 100  # Config. KSS31 26
    fc_from_sensor = 1 / (2 * math.pi * sensor_delay)
    ss2_fc = 1 / 175  # fc for Sea State 2

    logger.info(f"Apply 3 Bessel filters (one 1st-order, and two 2nd-order)... (sensor_delay = {sensor_delay})")
    result = input_arr
    result = bessel_first_order(result, fs=1, fc=fc_from_sensor, a=1)
    result = bessel_second_order(result, fs=1, fc=ss2_fc, a=1.3397, b=0.4889)
    result = bessel_second_order(result, fs=fs2, fc=ss2_fc, a=0.7743, b=0.3890)
    return result


def bessel_first_order(input_arr: np.array, fs, fc, a) -> np.array:
    """
    Applies first order Bessel filter : Y(t) = X(t)*A0*V + Y(t-1)*B
    """
    logger.info(f"Apply 1st-order Bessel filter with parameters : fs = {fs} ;  fc = {fc} ;  a = {a} ;")
    fc2pi = fc * 2 * math.pi  # fc in rad/s
    V = 1
    A = 1 / (1 + (fs * a) / fc2pi)
    B = 1 / (1 + fc2pi / (fs * a))

    output = np.empty(len(input_arr))
    output[0] = input_arr[0]
    for i in range(1, len(input_arr)):
        output[i] = input_arr[i] * A * V + output[i - 1] * B
    return output


def bessel_second_order(input_arr: np.array, fs, fc, a, b) -> np.array:
    """
    Applies second order Bessel filter: Y(t) = X(t)*A1*V1+Y(t-1)*B2+Y(t-2)*B3
    """
    logger.info(f"Apply 2nd-order Bessel filter with parameters : fs = {fs} ;  fc = {fc} ;  a = {a} ; b = {b} ;")
    fc2pi = fc * 2 * math.pi
    V = 1
    A = 1 / ((1 + (fs * a) / fc2pi) + (fs**2 * b) / (fc2pi**2))
    B1 = A * (fs * a / fc2pi + fs**2 * b * 2 / fc2pi**2)
    B2 = -A * fs**2 * b / fc2pi**2
    logger.info(f"A = {A} ;  B1 = {B1} ;  B2 = {B2} ")

    output = np.empty(len(input_arr))
    output[:2] = input_arr[:2]
    for i in range(2, len(output)):
        output[i] = input_arr[i] * A * V + output[i - 1] * B1 + output[i - 2] * B2

    return output

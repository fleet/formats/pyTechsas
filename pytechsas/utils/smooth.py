import logging as log
import math

import numpy as np

from scipy import signal

from .qtinars_bessel_filter import apply_qtinars_bessel

logger = log.getLogger()


def apply(
    i_path: str,
    i_type: str,
    o_path: str,
    algorithm: str,
    window_size: int,
    order: int,
    critical_freq: float,
    sampling_freq: float = None,
) -> dict:
    logger.info(f"Input file path : {i_path}")

    # get input data
    arr_type = np.float32 if i_type == "float" else np.float64
    input_arr = np.fromfile(i_path, arr_type)

    output_arr = None
    nan_count = len(np.flatnonzero(np.isnan(input_arr)))
    if nan_count > 0:
        logger.info(f"Use interpolation to handle NaN values ({nan_count} / {len(input_arr)})")
        output_arr = __apply_filter_with_interpolation__(
            input_arr, algorithm, window_size, order, critical_freq, sampling_freq
        )
    else:
        output_arr = __apply_filter__(input_arr, algorithm, window_size, order, critical_freq, sampling_freq)

    logger.info(f"Save in output file path {o_path} : {output_arr}")
    output_arr.tofile(o_path)
    return {"result": o_path}


def __apply_filter_with_interpolation__(
    i_arr: np.ndarray,
    algorithm: str,
    window_size: int,
    order: int,
    critical_freq: float = None,
    sampling_freq: float = None,
) -> np.ndarray:
    result = i_arr
    # fill NaN by interpolation
    mask = np.isnan(i_arr)
    nan_indexes = np.flatnonzero(mask)
    result[mask] = np.interp(nan_indexes, np.flatnonzero(~mask), i_arr[~mask])

    # apply filter
    result = __apply_filter__(result, algorithm, window_size, order, critical_freq, sampling_freq)

    # re-insert NaN
    result[nan_indexes] = np.nan
    return result


def __apply_filter_by_section__(
    i_arr: np.ndarray, algorithm: str, window_size: int, order: int, critical_freq: float, sampling_freq: float
) -> np.ndarray:
    # split array at each NaN
    nan_mask = np.isnan(i_arr)
    nan_indexes = np.flatnonzero(nan_mask)
    section_list = [x[~np.isnan(x)] for x in np.split(i_arr, nan_indexes)]

    # apply filter for each section
    filtered_section = [
        __apply_filter__(s, algorithm, window_size, order, critical_freq, sampling_freq) for s in section_list
    ]

    # concatenates sections in one array
    filtered_arr = np.concatenate(filtered_section)

    # build result array with filtered data
    result = i_arr
    result[~nan_mask] = filtered_arr
    return result


def __apply_filter__(
    i_arr: np.ndarray,
    algorithm: str,
    window_size: int,
    order: int = 2,
    critical_freq: float = 0.2,
    sampling_freq: float = 1.0,
) -> np.ndarray:
    output_arr = None
    if algorithm == "savgol":
        output_arr = savgol_filter(i_arr, window_size, order)
    elif algorithm == "butter":
        output_arr = butterworth_filter(i_arr, critical_freq, order, sampling_freq)
    elif algorithm == "bessel":
        output_arr = bessel_filter(i_arr, critical_freq, order, sampling_freq)
    elif algorithm == "bessel_q":
        logger.info(f"Apply qtinars bessel filter")
        output_arr = apply_qtinars_bessel(i_arr)
    else:
        return f"error : unknown algorithm {algorithm}"
    return output_arr


def savgol_filter(data: np.ndarray, window_size: int = 10, polynomial_order: int = 2) -> np.ndarray:
    logger.info(f"Apply savgol filter with : polynomial_order = {polynomial_order} ; window_size = {window_size}")
    return signal.savgol_filter(data, window_size, polynomial_order)


def butterworth_filter(data: np.ndarray, critical_freq, order: int, sampling_frequency: float) -> np.ndarray:
    logger.info(f"Apply butterworth filter with : order = {order} ; fc = {critical_freq}; fs = {sampling_frequency}")
    b, a = signal.butter(N=order, Wn=critical_freq, fs=sampling_frequency)
    return signal.filtfilt(b, a, data)


def bessel_filter(data: np.ndarray, critical_freq, order: int, sampling_frequency: float) -> np.ndarray:
    logger.info(f"Apply bessel filter with : order = {order} ; fc = {critical_freq}; fs = {sampling_frequency}")
    b, a = signal.bessel(N=order, Wn=critical_freq, fs=sampling_frequency)
    return signal.filtfilt(b, a, data)

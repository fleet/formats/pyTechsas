from datetime import datetime
import logging as log
import math
from typing import Optional

import numpy as np
import pandas as pd
from pandas.core.window import Rolling
import xarray as xr

from pytechsas.navigation.abstract_techsas_navigation import AbstractTechsasNavigation
from pytechsas.navigation.techsas_navigation import get_gnd_course_as_series, get_heading_as_series, get_speed_as_series
from pytechsas.sensor.techsas_constant import TECHSAS_STATUS, TECHSAS_TIME, TECHSAS_GND_COURSE, TECHSAS_HEADING

# CONSTANTS
TURN_FILTER_REJECTED_FLAG = 2
START_VAR = "start"
END_VAR = "end"
DURATION_VAR = "duration"

logger = log.getLogger("turn_filter")


def apply_filter_on_file(
    nav: AbstractTechsasNavigation,
    filter_method: str = "std",
    heading_period: Optional[int] = None,
    heading_threshold: Optional[float] = None,
    speed_period: Optional[int] = None,
    speed_threshold: Optional[float] = None,
    minimum_duration: Optional[int] = None,
    o_cut_file: Optional[str] = None,
    save_in_input_file: bool = False,
) -> (pd.Series, pd.DataFrame, np.array):
    #  get course over ground or heading
    heading_series = None
    if (heading_period is not None) and (heading_threshold is not None):
        if nav.get_courses_over_ground() is not None:
            heading_series = get_gnd_course_as_series(nav)
        elif nav.get_headings() is not None:
            heading_series = get_heading_as_series(nav)
            logger.warning(f"'{TECHSAS_GND_COURSE}' variable not found, '{TECHSAS_HEADING}' used instead.")
        else:
            logger.error(f"No {TECHSAS_GND_COURSE} or {TECHSAS_HEADING} variable found.")
            return None
    # get speed
    speed_series = get_speed_as_series(nav) if (speed_period is not None) and (speed_threshold is not None) else None

    # use 'unwrap' method on heading to correctly compute gap or standard deviation (avoid period errors)
    if heading_series is not None:
        heading_series[~np.isnan(heading_series)] = np.unwrap(heading_series[~np.isnan(heading_series)], period=360)

    # apply filter
    logger.info("Apply filter...")
    filter_result = __filter__(
        filter_method=filter_method,
        heading=heading_series,
        heading_period=heading_period,
        heading_threshold=heading_threshold,
        speed=speed_series,
        speed_period=speed_period,
        speed_threshold=speed_threshold,
    )
    if False not in filter_result.value_counts():
        logger.warning("WARNING: no data filtered.")

    # compute straight lines, and filter with minimum duration
    lines = __compute_straight_lines__(straight_line_mask=filter_result, minimum_duration=minimum_duration)
    logger.info(f"Result: {len(lines)} lines")
    lines_as_string = __print_cut_lines__(lines)
    logger.info(f'{lines_as_string[:1162]} {"..." if len(lines_as_string) > 1162 else ""}')

    # save in input file if asked
    lines_mask = None
    if save_in_input_file:
        if isinstance(nav, AbstractTechsasNavigation):
            # recompute mask from line (enables to apply minimum duration filter)
            lines_mask = __get_mask_from_lines__(filter_result.index, lines)
            logger.info(f"Save result in input file status variable : {nav.get_file_path()}")
            __save_in_file__(nav.get_file_path(), lines_mask)
        else:
            logger.error(f"Only TECHSAS file can be edited with turn_filter result.")

    # save in cut file if asked
    if o_cut_file:
        logger.info(f"Save in file : {o_cut_file}")
        with open(o_cut_file, "w", encoding="utf-8") as f:
            f.write(lines_as_string)
            f.flush()

    return filter_result, lines, lines_mask


def __filter__(
    filter_method: str = "std",
    heading: pd.Series = None,
    heading_period: int = None,
    heading_threshold: float = None,
    speed: pd.Series = None,
    speed_period: int = None,
    speed_threshold: float = None,
) -> pd.Series:
    """
    Applies a filter on input variables (heading or speed) to find straight lines.
    :param heading: heading data
    :param heading_period: window ......................
    :param heading_threshold:
    :param speed:
    :param speed_period:
    :param speed_threshold:
    :return: mask : True if window stdev is under thresholds (= straight line); else False (= turn)
    """
    # get filter method
    straight_line_mask: pd.Series = None

    # find points on straight line (mask : 1 == straight line; 0 == turn)
    if (heading is not None) and (heading_period is not None) and (heading_threshold is not None):
        logger.info(
            f"Filter on heading : method : '{filter_method}'; period: {heading_period} seconds; threshold: {heading_threshold} degrees"
        )
        straight_line_mask = __apply_filter__(filter_method, heading, heading_period, heading_threshold)
    if (speed is not None) and (speed_period is not None) and (speed_threshold is not None):
        logger.info(
            f"Filter on speed : method : '{filter_method}'; period: {speed_period} seconds; threshold: {speed_threshold} m/s"
        )
        speed_threshold *= 1.94384  # m/s to knots (speed data is saved in knots in TECHSAS files)
        filtered_from_speed = __apply_filter__(filter_method, speed, speed_period, speed_threshold)
        straight_line_mask = (
            filtered_from_speed if straight_line_mask is None else (straight_line_mask & filtered_from_speed)
        )
    if straight_line_mask is None:
        logger.error("Bad parameters : heading or speed period/threshold must be specified.")

    return straight_line_mask


def __apply_filter__(filter_method: str, variable: pd.Series, period_in_seconds: int, threshold: float):
    filter_fct = __gap_filter__
    if filter_method == "gap":
        filter_fct = __gap_filter__
    elif filter_method == "mean":
        filter_fct = __mean_filter__
    elif filter_method == "std":
        filter_fct = __std_filter__
    elif filter_method == "weighted_std":
        filter_fct = __weighted_std_filter__

    variable.sort_index(inplace=True)
    # centre = True and closed = "both" to get a symmetric window
    rolling_window = variable.rolling(f"{period_in_seconds}S", center=True, min_periods=1, closed="both")

    result = filter_fct(rolling_window, threshold)
    __print_filter_result__(result)
    return result


def __gap_filter__(rolling_window: Rolling, threshold: float) -> np.array:
    return rolling_window.apply(lambda x: abs(x[0] - x[-1])) < threshold


def __mean_filter__(rolling_window: Rolling, threshold: float) -> np.array:
    return (
        rolling_window.apply(lambda x: abs(x[: math.ceil(len(x) / 2)].mean() - x[math.floor(len(x) / 2) :].mean()))
        < threshold
    )


def __std_filter__(rolling_window: Rolling, threshold: float):
    return rolling_window.std() < threshold


def __weighted_std_filter__(rolling_window: Rolling, threshold: float):
    threshold = threshold**2
    return rolling_window.apply(__weighted_variance__) < threshold


def __weighted_variance__(values: np.array) -> float:
    l = len(values)
    # indexes = np.arange(length)
    # middle_index = (length - 1) / 2
    # weights = 1 - (abs(indexes - middle_index) / middle_index)
    # compute weights to get more value from the center of window
    # weights = [(n+1 if n < l/2 else (l-n))/math.ceil(l/2) for n in range(l)]
    a = np.arange(1, 1 + l / 2)
    weights = np.concatenate([a, np.flip(a)]) if l % 2 == 0 else np.concatenate([a, np.flip(a[:-1])])
    average = np.average(values, weights=weights)
    variance = np.average((values - average) ** 2, weights=weights)
    return variance


def __print_filter_result__(result: np.array) -> None:
    result_count = result.value_counts()
    if False not in result_count:
        logger.info(f"No data filtered")
    else:
        logger.info(f"{result_count[False]}/{len(result)} rejected data")


def __compute_straight_lines__(straight_line_mask: pd.Series, minimum_duration: float = None) -> pd.DataFrame:
    logger.info(f"Compute straight lines...")
    # get line starts
    diff_with_previous = straight_line_mask.diff()
    diff_with_previous[0] = True
    starts = straight_line_mask[diff_with_previous & straight_line_mask]

    # get line ends
    diff_with_next = straight_line_mask.diff(-1)
    diff_with_next[-1] = True
    ends = straight_line_mask[diff_with_next & straight_line_mask]

    # build DataFrame, and compute lines duration
    df = pd.DataFrame(data={START_VAR: starts.index, END_VAR: ends.index, DURATION_VAR: ends.index - starts.index})

    # apply minimum duration filter
    if minimum_duration:
        len_before = len(df)
        df = df[df.duration > np.timedelta64(minimum_duration, "s")]
        logger.info(f"Keep lines with minimum duration of {minimum_duration} seconds : {len(df)}/{len_before}")

    return df


def __get_mask_from_lines__(indexes: pd.DatetimeIndex, lines: pd.DataFrame) -> np.array:
    mask = np.full(len(indexes), False)
    for line in lines.itertuples():
        mask = mask | ((line.start <= indexes) & (indexes <= line.end))
    return mask


def __print_cut_lines__(lines: pd.DataFrame) -> str:
    # create column with index
    lines["index"] = range(1, len(lines) + 1)
    # compute string rows
    return lines.to_string(
        header=False,
        index=False,
        columns=[START_VAR, END_VAR, "index"],
        formatters={
            START_VAR: lambda start: "> " + __date_formatter__(start),
            END_VAR: __date_formatter__,
            "index": lambda idx: f"Line_{idx}",
        },
    )


def __date_formatter__(date_time: datetime):
    return date_time.strftime("%d/%m/%Y %H:%M:%S.%f")[:-3]


def __save_in_file__(file_path: str, filter_result: np.array):
    # compute status flags (merge with existing variable if exists)
    status_values = np.zeros(len(filter_result), dtype=np.int8)
    with xr.open_dataset(file_path) as ds:
        if TECHSAS_STATUS in ds:
            status_values = ds[TECHSAS_STATUS].values
    status_values = np.where(filter_result, status_values, status_values | TURN_FILTER_REJECTED_FLAG)

    # write status variable
    status_da = xr.DataArray(
        data=status_values,
        dims=TECHSAS_TIME,
        attrs={
            "long_name": "status",
            "flag_meanings": "rejected_by_used, rejected_by_turn_filter",
            "flag_mask": "1b,2b,4b,8b,16b,32b,64b",
            "comment": "status flag : if one of the byte field is set (value != 0) the data are considered as invalid",
        },
    )
    status_ds = xr.Dataset(data_vars={TECHSAS_STATUS: status_da})
    status_ds.to_netcdf(path=file_path, mode="a")

import numpy as np
import xarray as xr
import pandas as pd


def dataset_to_code(ds: xr.Dataset, time_var_name='time'):
    """
    Generates code to setup a dataset from dataset.
    Useful to avoid file dependencies in tests, and build test datasets from code.
    """
    datasetCode = f"xr.Dataset({{"
    for var in ds.data_vars:
        datasetCode += f"'{var}' : (['time'], {np.array2string(ds[var].data, separator=', ')}), \n"
    datasetCode += f"}}, coords={{'time' : pd. to_datetime({np.array2string(ds[time_var_name].data, separator=', ')}) }})"
    return print(datasetCode)


def __build_dataset_SALSA_LEG1_gravi__() -> xr.Dataset:
    """
    :return: xarray Dataset filled with data of "SALSA LEG 1"
    """
    return xr.Dataset({'freeair': (['time'], [-473.3, -473.3, -473.3, -473.2, -473.2, -473.2, -473.2, -473.2, -473.2,
                                              -473.2, -473.2, -473.2, -473.1, -473.2, -473.2, -473.2, -473.2, -473.1,
                                              -473.1, -473.1, -473.1, -473.1, -473.2, -473.2, -473.2, -473.2, -473.2,
                                              -473.2, -473.2, -473.2, -473.2, -473.2, -473.2, -473.2, -473.2, -473.2,
                                              -473.2, -473.2, -473.2, -473.2, -473.1, -473.1, -473.1, -473.2, -473.2,
                                              -473.2, -473.1, -473.1, -473.1, -473.1, -473.1, -473.1, -473.1, -473.1,
                                              -473.1, -473., -473., -473.1, -473.1, -473.1]),
                       'eoetvoes': (['time'], [-2877.8, -2877.8, -2877.8, -2877.7, -2877.7, -2877.7, -2877.7, -2877.7,
                                               -2877.8, -2877.8, -2877.7, -2877.7, -2877.7, -2877.7, -2877.7, -2877.7,
                                               -2877.7, -2877.7, -2877.7, -2877.6, -2877.7, -2877.7, -2877.7, -2877.7,
                                               -2877.7, -2877.7, -2877.7, -2877.7, -2877.7, -2877.7, -2877.8, -2877.8,
                                               -2877.8, -2877.8, -2877.8, -2877.7, -2877.7, -2877.7, -2877.7, -2877.7,
                                               -2877.7, -2877.7, -2877.7, -2877.8, -2877.8, -2877.8, -2877.7, -2877.7,
                                               -2877.7, -2877.7, -2877.7, -2877.7, -2877.7, -2877.7, -2877.7, -2877.6,
                                               -2877.7, -2877.7, -2877.7, -2877.7]),
                       'gravity': (['time'], [-2902.2, -2902.21, -2902.19, -2902.17, -2902.17, -2902.18, -2902.18,
                                              -2902.18, -2902.2, -2902.2, -2902.17, -2902.13, -2902.13, -2902.15,
                                              -2902.17, -2902.18, -2902.16, -2902.14, -2902.13, -2902.13, -2902.14,
                                              -2902.17, -2902.2, -2902.22, -2902.23, -2902.24, -2902.24, -2902.24,
                                              -2902.24, -2902.25, -2902.26, -2902.27, -2902.27, -2902.28, -2902.28,
                                              -2902.27, -2902.26, -2902.26, -2902.26, -2902.25, -2902.23, -2902.21,
                                              -2902.24, -2902.29, -2902.32, -2902.31, -2902.25, -2902.21, -2902.21,
                                              -2902.24, -2902.26, -2902.25, -2902.21, -2902.2, -2902.19, -2902.18,
                                              -2902.18, -2902.2, -2902.21, -2902.21]),
                       'lon': (['time'], [-37.15220131, -37.15217236, -37.15214028, -37.15210789, -37.1520777,
                                          -37.15204746, -37.15201531, -37.15198468, -37.15195352, -37.15192304,
                                          -37.15189231, -37.15185974, -37.1518277, -37.15179819, -37.15176715,
                                          -37.151736, -37.1517039, -37.15167219, -37.15164124, -37.15160919,
                                          -37.15157785, -37.15154621, -37.15151434, -37.15148339, -37.15145219,
                                          -37.15142079, -37.15138853, -37.15135773, -37.15132588, -37.15129463,
                                          -37.15126217, -37.15123105, -37.15119892, -37.15116747, -37.15113736,
                                          -37.15110525, -37.15107303, -37.15104106, -37.15100924, -37.15098003,
                                          -37.15094865, -37.15091648, -37.15088395, -37.15085284, -37.1508227,
                                          -37.15079259, -37.15076021, -37.15072769, -37.15069643, -37.15066582,
                                          -37.15063426, -37.15060366, -37.15057104, -37.15054016, -37.15050929,
                                          -37.15047658, -37.15044515, -37.150414, -37.15038251, -37.15035064]),
                       'lat': (['time'], [-12.14443828, -12.1443892, -12.14434108, -12.14429653, -12.14424955,
                                          -12.14420079, -12.14415299, -12.14410714, -12.14405923, -12.14401014,
                                          -12.14396264, -12.1439175, -12.14387233, -12.14382519, -12.14377515,
                                          -12.14372783, -12.14368181, -12.14363566, -12.14358803, -12.14354192,
                                          -12.14349557, -12.14344828, -12.14340013, -12.14335266, -12.14330745,
                                          -12.14326072, -12.14321225, -12.14316632, -12.14312062, -12.14307314,
                                          -12.14302496, -12.1429788, -12.14293259, -12.14288542, -12.14283786,
                                          -12.14279206, -12.14274479, -12.14269745, -12.14264869, -12.14260116,
                                          -12.1425565, -12.1425106, -12.14246397, -12.14241542, -12.14236652,
                                          -12.14231915, -12.14227363, -12.14222985, -12.14218296, -12.14213402,
                                          -12.14208483, -12.14203812, -12.14199349, -12.14194702, -12.1418982,
                                          -12.14185158, -12.14180563, -12.14175899, -12.14171052, -12.14166386]),
                       'heading': (['time'], [30.63302307, 30.68616915, 33.888256, 35.27958976, 31.17013568,
                                              31.63370688, 33.71438208, 32.73498816, 32.41208592, 30.7540768,
                                              33.29019471, 35.33066235, 34.35695031, 30.84117894, 31.77782912,
                                              33.18404206, 34.5, 33.33996404, 32.30785408, 34.53684009,
                                              32.9221101, 33.3695776, 32.48600128, 32.62966689, 34.22061799,
                                              32.70569669, 33.19421948, 33.46237779, 34.35469883, 32.28902895,
                                              33.58240537, 33.45472174, 34.40978225, 32.4578046, 31.83245952,
                                              35.132256, 33.25161344, 33.84960406, 31.64688384, 31.61876544,
                                              34.95968256, 34.34204542, 34.16681548, 31.46138048, 31.04440006,
                                              32.45569713, 35.40163639, 35.62743702, 32.25321536, 31.5752032,
                                              32.11757236, 32.85224, 36.14424599, 31.63168846, 32.35281005,
                                              34.79559622, 33.60095232, 32.8019512, 32.6710412, 33.874848]),
                       'speed': (['time'], [6.08196983, 6.10845822, 6.17638342, 5.91138802, 6.03378341, 6.21672962,
                                            5.96693547, 5.93609839, 6.22130827, 6.15912433, 6.01999998, 5.88280095,
                                            5.96800303, 6.02162752, 6.29716964, 5.99037802, 6., 6.03468068,
                                            6.047857, 6.03225572, 5.94386855, 6.1067583, 6.16781894, 6.0723971,
                                            5.93258797, 6.06759552, 6.16674853, 5.82241893, 6.02164956, 6.13576599,
                                            6.1435306, 5.98274688, 6.05090499, 6.09277411, 5.89139507, 6.05217571,
                                            6.1202994, 6.14479799, 6.18673861, 5.92634223, 5.93153647, 5.99357925,
                                            6.09111325, 6.23069889, 6.0991999, 5.99096456, 5.91023762, 5.80333448,
                                            6.13072157, 6.21260163, 6.19810672, 5.89063347, 5.94016102, 6.04338614,
                                            6.15922223, 6.09748274, 5.99047593, 6.04238945, 6.17964366, 5.9796909]),
                       },
                      coords={'time': pd.to_datetime(['2014-04-21T12:00:00.049247744', '2014-04-21T12:00:02.052864512',
                                                      '2014-04-21T12:00:04.050432000', '2014-04-21T12:00:06.046272000',
                                                      '2014-04-21T12:00:08.047296000', '2014-04-21T12:00:10.044864000',
                                                      '2014-04-21T12:00:12.051072000', '2014-04-21T12:00:14.046912000',
                                                      '2014-04-21T12:00:16.043615744', '2014-04-21T12:00:18.043776000',
                                                      '2014-04-21T12:00:20.042207744', '2014-04-21T12:00:22.046688256',
                                                      '2014-04-21T12:00:24.039935488', '2014-04-21T12:00:26.040095744',
                                                      '2014-04-21T12:00:28.047168000', '2014-04-21T12:00:30.037824512',
                                                      '2014-04-21T12:00:32.043168256', '2014-04-21T12:00:34.039007744',
                                                      '2014-04-21T12:00:36.035712000', '2014-04-21T12:00:38.037599744',
                                                      '2014-04-21T12:00:40.035168256', '2014-04-21T12:00:42.042240000',
                                                      '2014-04-21T12:00:44.036352000', '2014-04-21T12:00:46.033055744',
                                                      '2014-04-21T12:00:48.032351744', '2014-04-21T12:00:50.031648256',
                                                      '2014-04-21T12:00:52.036128256', '2014-04-21T12:00:54.030240256',
                                                      '2014-04-21T12:00:56.032991744', '2014-04-21T12:00:58.028831744',
                                                      '2014-04-21T12:01:00.028128256', '2014-04-21T12:01:02.034335744',
                                                      '2014-04-21T12:01:04.030175744', '2014-04-21T12:01:06.027744256',
                                                      '2014-04-21T12:01:08.027904000', '2014-04-21T12:01:10.027200000',
                                                      '2014-04-21T12:01:12.029952000', '2014-04-21T12:01:14.026656256',
                                                      '2014-04-21T12:01:16.025088000', '2014-04-21T12:01:18.024384000',
                                                      '2014-04-21T12:01:20.021952000', '2014-04-21T12:01:22.025567744',
                                                      '2014-04-21T12:01:24.022271488', '2014-04-21T12:01:26.023296000',
                                                      '2014-04-21T12:01:28.020000256', '2014-04-21T12:01:30.019295744',
                                                      '2014-04-21T12:01:32.023776256', '2014-04-21T12:01:34.019616256',
                                                      '2014-04-21T12:01:36.018048000', '2014-04-21T12:01:38.017344000',
                                                      '2014-04-21T12:01:40.015775744', '2014-04-21T12:01:42.021120000',
                                                      '2014-04-21T12:01:44.016096256', '2014-04-21T12:01:46.015391744',
                                                      '2014-04-21T12:01:48.012960256', '2014-04-21T12:01:50.013984256',
                                                      '2014-04-21T12:01:52.015872000', '2014-04-21T12:01:54.012576256',
                                                      '2014-04-21T12:01:56.011872256',
                                                      '2014-04-21T12:01:58.010304000'])})


def __build_dataset_SALSA_LEG1_mag__() -> xr.Dataset:
    """
    :return: xarray Dataset filled with mag data of "SALSA LEG 1"
    """
    return xr.Dataset({'mag': (['time'], [25622., 25621.6, 25621.9, 25622., 25621.9, 25621.8, 25622., 25622.1,
                                          25622.4, 25622.1, 25621.7, 25622.2, 25622.5, 25622.2, 25622., 25622.2,
                                          25622.7, 25622.5, 25622.4, 25622.8, 25622.6, 25622.5, 25622.9, 25622.7,
                                          25622.6, 25623., 25622.9, 25622.9, 25623., 25623., 25623., 25623.4,
                                          25623.2, 25623.3, 25623.3, 25623.1, 25623.6, 25623.5, 25623.6, 25623.6,
                                          25623.7, 25623.8, 25623.7, 25624., 25623.7, 25623.8, 25624.2, 25624.,
                                          25624., 25624., 25624., 25624.3, 25624.3, 25624.1, 25624.4, 25624.3,
                                          25624.4, 25624.3, 25624.4, 25624.4, 25624.7, 25624.4, 25624.5, 25624.8,
                                          25624.6, 25624.5, 25624.7, 25624.8, 25624.9, 25625., 25625., 25625.,
                                          25625.2, 25624.9, 25625.1, 25625.5, 25625.1, 25625.4, 25625.5, 25625.1]),
                       'depth': (['time'], [44.7, 44.9, 44.9, 44.9, 44.9, 44.9, 44.9, 44.9, 44.7, 44.7, 44.7, 44.7,
                                            44.9, 45.1, 45.1, 45.2, 45.2, 44.9, 45., 44.9, 44.9, 44.9, 45., 44.9,
                                            44.9, 45.1, 44.7, 44.9, 44.9, 44.7, 44.9, 44.9, 44.7, 45., 45.2, 45.1,
                                            45.2, 45.2, 45.2, 44.9, 45., 44.7, 45., 44.9, 44.9, 45.1, 45.2, 45.1,
                                            45.2, 45.2, 45.1, 45.1, 44.9, 44.7, 44.9, 44.6, 44.4, 44.2, 44.3, 44.2,
                                            44.3, 44., 44., 44.3, 44.3, 44.3, 44.3, 44.3, 44.5, 44.9, 44.9, 45.1,
                                            45.2, 45.2, 45.2, 45.2, 45., 45.1, 45., 45.]),
                       'lon': (['time'], [-36.13009654, -36.13015013, -36.13020408, -36.13026344, -36.13031782,
                                          -36.13037128, -36.13042894, -36.13048387, -36.13053995, -36.1305956,
                                          -36.13064987, -36.13070647, -36.13076276, -36.13081701, -36.13087268,
                                          -36.13092817, -36.13098594, -36.13103992, -36.13110168, -36.13115374,
                                          -36.13120761, -36.13126248, -36.13131926, -36.13137459, -36.13143113,
                                          -36.13148779, -36.13154088, -36.13159864, -36.13165437, -36.13171068,
                                          -36.13176559, -36.13182196, -36.13187749, -36.13193414, -36.13199097,
                                          -36.1320461, -36.13210018, -36.13215735, -36.1322135, -36.13226748,
                                          -36.13232494, -36.13237949, -36.13243486, -36.13249137, -36.13254718,
                                          -36.13260288, -36.13265849, -36.13271341, -36.13277045, -36.13282628,
                                          -36.13288257, -36.13293764, -36.13299298, -36.13305021, -36.13310668,
                                          -36.13316128, -36.13321766, -36.13327376, -36.13332965, -36.13338536,
                                          -36.13344066, -36.1334978, -36.13355248, -36.13360859, -36.13366507,
                                          -36.13371985, -36.13377537, -36.13383275, -36.13388699, -36.13394298,
                                          -36.13399769, -36.13405358, -36.13410986, -36.13416525, -36.1342194,
                                          -36.1342774, -36.13433324, -36.13438608, -36.13444723, -36.13449997]),
                       'lat': (['time'], [-10.69536804, -10.69542347, -10.69548037, -10.69553413, -10.69558606,
                                          -10.69564371, -10.69570407, -10.69575265, -10.69580636, -10.69586626,
                                          -10.69592102, -10.6959738, -10.69602908, -10.69608581, -10.69614022,
                                          -10.69619506, -10.69625048, -10.69630695, -10.69636835, -10.69641726,
                                          -10.69647201, -10.69652766, -10.69658606, -10.69664089, -10.69669518,
                                          -10.69675106, -10.69680935, -10.69686401, -10.69691719, -10.69697446,
                                          -10.69703148, -10.697086, -10.69714167, -10.69719816, -10.69725466,
                                          -10.69731049, -10.69736506, -10.69742265, -10.6974784, -10.69753455,
                                          -10.69759196, -10.69764638, -10.69770315, -10.69776062, -10.69781647,
                                          -10.69787197, -10.69792897, -10.69798456, -10.69804067, -10.69809727,
                                          -10.69815367, -10.69820933, -10.69826452, -10.69832271, -10.69837888,
                                          -10.69843126, -10.69848963, -10.69854746, -10.69860156, -10.69865517,
                                          -10.69871241, -10.69877187, -10.69882541, -10.69887881, -10.69893665,
                                          -10.69899253, -10.69904887, -10.69910379, -10.69915837, -10.69921517,
                                          -10.69927001, -10.69932428, -10.69938293, -10.69943648, -10.6994899,
                                          -10.6995488, -10.69960566, -10.69965732, -10.69971547, -10.69977063]),
                       'heading': (['time'], [224.48241772, 222.47321545, 224.12400768, 228.08883648, 224.08561344,
                                              221.849584, 225.4612064, 227.87788722, 224.83732505, 221.23969453,
                                              226.63615843, 225.4724512, 225.1041824, 222.49831872, 226.21827392,
                                              224.2022016, 226.13916743, 221.74743391, 226.74245485, 225.16439338,
                                              224.4691264, 223.45947584, 224.4649216, 224.8037332, 226.2875648,
                                              223.19118545, 223.05729392, 227.08835189, 225.00180405, 223.75775232,
                                              223.84013632, 226.03791744, 223.60911085, 225.37048, 223.90779636,
                                              224.33416796, 223.90133081, 224.48822072, 224.6657888, 223.27603794,
                                              225.36115776, 224.05467616, 223.7396064, 224.47098419, 224.19583204,
                                              224.21422448, 224.024503, 224.47660848, 225.10620536, 223.8741536,
                                              224.5, 224.335392, 223.91940269, 224.34092792, 224.65,
                                              225.9266432, 222.759216, 224.94060257, 225.28, 225.44354248,
                                              222.6167648, 223.44113472, 226.43317251, 225.13950196, 223.734503,
                                              223.72540004, 224.48215912, 226.18884864, 223.7317504, 224.05032664,
                                              225.3950634, 224.68654744, 223.43616624, 225.95928385, 224.51234944,
                                              224.46926464, 223.09629248, 226.39318208, 224.8434336, 222.65726381]),
                       'speed': (['time'], [5.86843702, 5.40999985, 5.6572984, 5.5712473, 5.3219485, 5.85020766,
                                            5.75023338, 5.17878735, 5.79833586, 5.80156258, 5.45196303, 5.59845623,
                                            5.6884487, 5.5907792, 5.44274181, 5.62763804, 5.76651808, 5.57650069,
                                            5.59292678, 5.7519632, 5.45077165, 5.70457026, 5.72272812, 5.48195838,
                                            5.74291519, 5.67883921, 5.7068896, 5.62507922, 5.55450295, 5.7403857,
                                            5.54193915, 5.57116898, 5.69463729, 5.74724479, 5.759226, 5.50622084,
                                            5.65296694, 5.78117767, 5.50501695, 5.75185522, 5.8084409, 5.52647458,
                                            5.70881923, 5.72425545, 5.62961084, 5.62765534, 5.5651043, 5.57113155,
                                            5.7772849, 5.63194845, 5.63352481, 5.56122874, 5.65845234, 5.88818545,
                                            5.56820501, 5.49534975, 5.96539179, 5.56506016, 5.53072837, 5.54047229,
                                            5.89094729, 5.74567337, 5.30093284, 5.6851453, 5.71431909, 5.55927994,
                                            5.66077554, 5.62304117, 5.58647457, 5.68099919, 5.4819344, 5.64650087,
                                            5.85765535, 5.34214816, 5.50652648, 6., 5.47705388, 5.38412466,
                                            5.85080287, 5.6135229]),
                       'altitude': (['time'], [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
                                               0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
                                               0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
                                               0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
                                               0., 0., 0., 0., 0., 0., 0., 0.]),
                       'computed_theoretic_magnetism': (
                       ['time'], [25283.14348738, 25283.1287803, 25283.11380278, 25283.09870199,
                                  25283.08445133, 25283.06941348, 25283.05347726, 25283.03968541,
                                  25283.02496432, 25283.00932709, 25282.9946474, 25282.98001255,
                                  25282.96502291, 25282.95003938, 25282.93525566, 25282.92042384,
                                  25282.90524491, 25282.8903329, 25282.87377026, 25282.86025206,
                                  25282.84562135, 25282.83073456, 25282.81520247, 25282.80039261,
                                  25282.78553016, 25282.77040547, 25282.75530987, 25282.74025084,
                                  25282.72565265, 25282.71035141, 25282.69524656, 25282.68036703,
                                  25282.6654043, 25282.65018537, 25282.63494606, 25282.6200029,
                                  25282.60537511, 25282.5899267, 25282.57488064, 25282.56001863,
                                  25282.54456452, 25282.52990777, 25282.51479081, 25282.49943633,
                                  25282.48441278, 25282.46945776, 25282.4542779, 25282.43939722,
                                  25282.42419394, 25282.40905181, 25282.39388844, 25282.37897899,
                                  25282.36411218, 25282.34856424, 25282.33341644, 25282.31907142,
                                  25282.30359216, 25282.28822878, 25282.27346903, 25282.25880636,
                                  25282.24362613, 25282.22789088, 25282.21335675, 25282.19868166,
                                  25282.18327229, 25282.16836247, 25282.15329525, 25282.13824036,
                                  25282.12359555, 25282.10840379, 25282.0936636, 25282.07887925,
                                  25282.06336737, 25282.04874965, 25282.03429556, 25282.0185495,
                                  25282.00336759, 25281.9893333, 25281.9733494, 25281.95878247]),
                       'computed_magnetic_anomaly': (
                       ['time'], [338.85651262, 338.47082908, 338.78658785, 338.90129801, 338.8159393,
                                  338.73136777, 338.94652274, 339.05992397, 339.37542631, 339.09028229,
                                  338.70457135, 339.2192062, 339.53497709, 339.24917937, 339.06474434,
                                  339.27879491, 339.79397384, 339.6096671, 339.52662036, 339.94052919,
                                  339.75398803, 339.66926544, 340.08518815, 339.89882614, 339.81407922,
                                  340.22959453, 340.14508075, 340.16013979, 340.27434735, 340.28964859,
                                  340.30475344, 340.7200236, 340.53381445, 340.65059588, 340.66583519,
                                  340.47960647, 340.99423426, 340.9100733, 341.02472873, 341.03959075,
                                  341.15465423, 341.27087348, 341.18442794, 341.50056367, 341.21480597,
                                  341.33132349, 341.74494085, 341.56060278, 341.57580606, 341.59094819,
                                  341.60611156, 341.92180226, 341.93666907, 341.75104513, 342.06697418,
                                  341.98170983, 342.09679847, 342.01255247, 342.12692159, 342.14158426,
                                  342.45559262, 342.17249974, 342.28664325, 342.60209959, 342.41633708,
                                  342.33163753, 342.5459235, 342.66254089, 342.77679507, 342.89159621,
                                  342.9063364, 342.92112075, 343.13585138, 342.85164098, 343.06531381,
                                  343.4814505, 343.09624179, 343.41105732, 343.5266506, 343.14082691]),
                       },
                      coords={'time': pd.to_datetime(['2014-04-11T00:00:02.960928256', '2014-04-11T00:00:05.960735744',
                                                      '2014-04-11T00:00:08.961408000', '2014-04-11T00:00:11.961216000',
                                                      '2014-04-11T00:00:14.961024000', '2014-04-11T00:00:17.960832000',
                                                      '2014-04-11T00:00:20.960640000', '2014-04-11T00:00:23.959583744',
                                                      '2014-04-11T00:00:26.961119744', '2014-04-11T00:00:29.960928256',
                                                      '2014-04-11T00:00:32.960735744', '2014-04-11T00:00:35.961408000',
                                                      '2014-04-11T00:00:38.961216000', '2014-04-11T00:00:41.961024000',
                                                      '2014-04-11T00:00:44.960832000', '2014-04-11T00:00:47.960640000',
                                                      '2014-04-11T00:00:50.961312256', '2014-04-11T00:00:53.961119744',
                                                      '2014-04-11T00:00:57.292703744', '2014-04-11T00:00:59.960735744',
                                                      '2014-04-11T00:01:02.961408000', '2014-04-11T00:01:05.961216000',
                                                      '2014-04-11T00:01:08.961024000', '2014-04-11T00:01:11.960832512',
                                                      '2014-04-11T00:01:14.960640000', '2014-04-11T00:01:17.961312256',
                                                      '2014-04-11T00:01:20.961119744', '2014-04-11T00:01:23.960928256',
                                                      '2014-04-11T00:01:26.960735744', '2014-04-11T00:01:29.961408000',
                                                      '2014-04-11T00:01:32.961216000', '2014-04-11T00:01:35.961024000',
                                                      '2014-04-11T00:01:38.961696256', '2014-04-11T00:01:41.960640000',
                                                      '2014-04-11T00:01:44.961312256', '2014-04-11T00:01:47.961119744',
                                                      '2014-04-11T00:01:50.960928256', '2014-04-11T00:01:53.960735744',
                                                      '2014-04-11T00:01:56.961408000', '2014-04-11T00:01:59.961216512',
                                                      '2014-04-11T00:02:02.961024000', '2014-04-11T00:02:05.960831488',
                                                      '2014-04-11T00:02:08.960640000', '2014-04-11T00:02:11.961312256',
                                                      '2014-04-11T00:02:14.961119744', '2014-04-11T00:02:17.960928256',
                                                      '2014-04-11T00:02:20.960735744', '2014-04-11T00:02:23.962271744',
                                                      '2014-04-11T00:02:26.961216512', '2014-04-11T00:02:29.961024000',
                                                      '2014-04-11T00:02:32.960831488', '2014-04-11T00:02:35.969280000',
                                                      '2014-04-11T00:02:38.961312256', '2014-04-11T00:02:41.969759744',
                                                      '2014-04-11T00:02:44.960928256', '2014-04-11T00:02:47.966784000',
                                                      '2014-04-11T00:02:50.976960000', '2014-04-11T00:02:53.975903744',
                                                      '2014-04-11T00:02:56.975712256', '2014-04-11T00:02:59.976383488',
                                                      '2014-04-11T00:03:02.960640000', '2014-04-11T00:03:05.962176000',
                                                      '2014-04-11T00:03:08.976671744', '2014-04-11T00:03:11.962655744',
                                                      '2014-04-11T00:03:14.960735744', '2014-04-11T00:03:17.963999744',
                                                      '2014-04-11T00:03:20.961215488', '2014-04-11T00:03:23.966208000',
                                                      '2014-04-11T00:03:26.960832000', '2014-04-11T00:03:29.966688256',
                                                      '2014-04-11T00:03:32.961312256', '2014-04-11T00:03:35.961119744',
                                                      '2014-04-11T00:03:38.960928256', '2014-04-11T00:03:41.964192256',
                                                      '2014-04-11T00:03:44.961408000', '2014-04-11T00:03:47.964672000',
                                                      '2014-04-11T00:03:50.967936000', '2014-04-11T00:03:53.960832000',
                                                      '2014-04-11T00:03:57.091968000',
                                                      '2014-04-11T00:03:59.964767744'])})

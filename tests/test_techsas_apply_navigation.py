import os
import tempfile
from datetime import datetime, timedelta
from unittest.mock import create_autospec
import numpy as np
import pytest
import xarray as xr
from numpy.ma.testutils import assert_equal

from pytechsas.sensor.techsas_apply_navigation import apply_navigation, ApplyNavigationData
from pytechsas.sensor.techsas_constant import (
    TECHSAS_LONGITUDE,
    TECHSAS_LATITUDE,
    TECHSAS_HEADING,
    TECHSAS_GND_SPEED,
    TECHSAS_GND_COURSE,
    TECHSAS_LATITUDE_STANDARD_NAME,
    TECHSAS_LATITUDE_LONG_NAME,
    TECHSAS_LATITUDE_UNITS,
    TECHSAS_LATITUDE_VALID_RANGE,
    TECHSAS_LONGITUDE_STANDARD_NAME,
    TECHSAS_LONGITUDE_LONG_NAME,
    TECHSAS_LONGITUDE_UNITS,
    TECHSAS_LONGITUDE_VALID_RANGE,
    TECHSAS_HEADING_STANDARD_NAME,
    TECHSAS_HEADING_LONG_NAME,
    TECHSAS_HEADING_UNITS,
    TECHSAS_HEADING_VALID_RANGE,
    TECHSAS_GND_SPEED_STANDARD_NAME,
    TECHSAS_GND_SPEED_LONG_NAME,
    TECHSAS_GND_SPEED_UNITS,
    TECHSAS_GND_SPEED_VALID_MIN,
    TECHSAS_GND_COURSE_STANDARD_NAME,
    TECHSAS_GND_COURSE_LONG_NAME,
    TECHSAS_GND_COURSE_UNITS,
    TECHSAS_GND_COURSE_VALID_RANGE,
)


@pytest.fixture
def mock_navigation_data():
    """Create a mock ApplyNavigationData instance with sample data."""
    mock_data = create_autospec(ApplyNavigationData, instance=True)
    mock_data.get_name.return_value = "TestNavigation"
    base_time = datetime(2023, 1, 1)
    mock_data.get_times.return_value = np.array([base_time + timedelta(seconds=i) for i in range(4)])
    mock_data.get_latitudes.return_value = np.array([10.0, 10.1, 10.2, 10.3])
    mock_data.get_longitudes.return_value = np.array([20.0, 20.1, 20.2, 20.3])
    mock_data.get_headings.return_value = np.array([0.0, 45.0, 90.0, 135.0])
    mock_data.get_speeds.return_value = np.array([5.0, 5.2, 5.4, 5.6])
    mock_data.get_courses_over_ground.return_value = np.array([180.0, 190.0, 200.0, 210.0])
    return mock_data


@pytest.fixture
def mock_techsas_file():
    """Create a sample xarray.Dataset representing existing data and save it to a temporary file."""
    base_time = datetime(2023, 1, 1)
    times = np.array([base_time + timedelta(seconds=i) for i in range(4)])
    ds = xr.Dataset(
        {
            "temperature": ("time", [15.1, 15.2, 15.3, 15.4]),
        },
        coords={"time": times},
    )

    with tempfile.NamedTemporaryFile(delete=False, suffix=".nc") as tmp_file:
        ds.to_netcdf(tmp_file.name)
        ds.close()
        return tmp_file.name


def test_apply_navigation(mock_techsas_file, mock_navigation_data):
    """Test that apply_navigation correctly updates the dataset file."""
    apply_navigation(mock_techsas_file, mock_navigation_data)

    # Reopen the updated dataset
    with xr.open_dataset(mock_techsas_file) as updated_ds:
        # Check that new variables exist in dataset
        assert TECHSAS_LATITUDE in updated_ds
        assert TECHSAS_LONGITUDE in updated_ds
        assert TECHSAS_HEADING in updated_ds
        assert TECHSAS_GND_SPEED in updated_ds
        assert TECHSAS_GND_COURSE in updated_ds

        # Validate data content
        np.testing.assert_array_equal(updated_ds[TECHSAS_LATITUDE].values, mock_navigation_data.get_latitudes())
        np.testing.assert_array_equal(updated_ds[TECHSAS_LONGITUDE].values, mock_navigation_data.get_longitudes())
        np.testing.assert_array_equal(updated_ds[TECHSAS_HEADING].values, mock_navigation_data.get_headings())
        np.testing.assert_array_equal(updated_ds[TECHSAS_GND_SPEED].values, mock_navigation_data.get_speeds())
        np.testing.assert_array_equal(
            updated_ds[TECHSAS_GND_COURSE].values, mock_navigation_data.get_courses_over_ground()
        )

        # Validate attributes
        assert updated_ds[TECHSAS_LATITUDE].attrs["standard_name"] == TECHSAS_LATITUDE_STANDARD_NAME
        assert updated_ds[TECHSAS_LATITUDE].attrs["long_name"] == TECHSAS_LATITUDE_LONG_NAME
        assert updated_ds[TECHSAS_LATITUDE].attrs["units"] == TECHSAS_LATITUDE_UNITS
        assert_equal(updated_ds[TECHSAS_LATITUDE].attrs["valid_range"], TECHSAS_LATITUDE_VALID_RANGE)

        assert updated_ds[TECHSAS_LONGITUDE].attrs["standard_name"] == TECHSAS_LONGITUDE_STANDARD_NAME
        assert updated_ds[TECHSAS_LONGITUDE].attrs["long_name"] == TECHSAS_LONGITUDE_LONG_NAME
        assert updated_ds[TECHSAS_LONGITUDE].attrs["units"] == TECHSAS_LONGITUDE_UNITS
        assert_equal(updated_ds[TECHSAS_LONGITUDE].attrs["valid_range"], TECHSAS_LONGITUDE_VALID_RANGE)

        assert updated_ds[TECHSAS_HEADING].attrs["standard_name"] == TECHSAS_HEADING_STANDARD_NAME
        assert updated_ds[TECHSAS_HEADING].attrs["long_name"] == TECHSAS_HEADING_LONG_NAME
        assert updated_ds[TECHSAS_HEADING].attrs["units"] == TECHSAS_HEADING_UNITS
        assert_equal(updated_ds[TECHSAS_HEADING].attrs["valid_range"], TECHSAS_HEADING_VALID_RANGE)

        assert updated_ds[TECHSAS_GND_SPEED].attrs["standard_name"] == TECHSAS_GND_SPEED_STANDARD_NAME
        assert updated_ds[TECHSAS_GND_SPEED].attrs["long_name"] == TECHSAS_GND_SPEED_LONG_NAME
        assert updated_ds[TECHSAS_GND_SPEED].attrs["units"] == TECHSAS_GND_SPEED_UNITS
        assert updated_ds[TECHSAS_GND_SPEED].attrs["valid_min"] == TECHSAS_GND_SPEED_VALID_MIN

        assert updated_ds[TECHSAS_GND_COURSE].attrs["standard_name"] == TECHSAS_GND_COURSE_STANDARD_NAME
        assert updated_ds[TECHSAS_GND_COURSE].attrs["long_name"] == TECHSAS_GND_COURSE_LONG_NAME
        assert updated_ds[TECHSAS_GND_COURSE].attrs["units"] == TECHSAS_GND_COURSE_UNITS
        assert_equal(updated_ds[TECHSAS_GND_COURSE].attrs["valid_range"], np.array(TECHSAS_GND_COURSE_VALID_RANGE))

    # Clean up temporary file
    os.remove(mock_techsas_file)

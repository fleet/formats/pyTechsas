from datetime import datetime
import pandas as pd
import numpy as np
import xarray as xr

import pytechsas.sensor.techsas_gravi_process as gravi
import tests.techsas_test_utils as utils


def test_compute_drift():
    """
    Test compute_drift method
    """
    ref_date_1 = datetime.fromisoformat("2019-12-04")
    g_zero_date, g_zero, drift = gravi.__compute_drift__(
        ref_date_1=ref_date_1,
        ref_value_1=50,
        ref_measure_1=10,
        ref_date_2=datetime.fromisoformat("2019-12-14"),
        ref_value_2=50,
        ref_measure_2=10,
    )
    assert g_zero_date == ref_date_1
    assert g_zero == 40
    assert drift == 0

    ref_date_1 = datetime.fromisoformat("2019-12-04")
    g_zero_date, g_zero, drift = gravi.__compute_drift__(
        ref_date_1=ref_date_1,
        ref_value_1=50,
        ref_measure_1=10,
        ref_date_2=datetime.fromisoformat("2019-12-14"),
        ref_value_2=25,
        ref_measure_2=5,
    )
    assert g_zero_date == ref_date_1
    assert g_zero == 40
    assert drift == -2 / (24 * 60 * 60)


def test_compute_drifted_gravity():
    """
    Test "compute_drifted_gravity" method
    """
    ref_date_1 = datetime.fromisoformat("2023-06-01")
    time_index = pd.date_range(start="2023-06-01", periods=10, freq="D")
    data_array = xr.DataArray(np.arange(0, 10), coords={"time": time_index}, dims=["time"])
    print(data_array)

    drifted_grav = gravi.__compute_drifted_gravity__(
        g_observed_values=data_array, g_zero=10, g_zero_date=ref_date_1, drift=1.0 / (24 * 60 * 60)
    )

    assert np.array_equal(drifted_grav.data, np.arange(10, 30, 2))


def test_process_gravity():
    test_dataset = utils.__build_dataset_SALSA_LEG1_gravi__()

    # apply gravity with official values
    spring_stiffness_corr = 7671 / 8338  # real_spring_stiffness / install_spring_stiffness
    g_zero_date, g_zero, drift = gravi.__compute_drift__(
        ref_date_1=datetime.fromisoformat("2014-04-02"),
        ref_value_1=978150.487,
        ref_measure_1=-2978.594,
        ref_date_2=datetime.fromisoformat("2014-05-04"),
        ref_value_2=978311.012,
        ref_measure_2=-2807.100,
        spring_stiffness_corr=spring_stiffness_corr,
    )

    result_ds = gravi.process_gravity_ds(
        ds=test_dataset,
        g_zero_date=g_zero_date,
        g_zero=g_zero,
        drift=drift,
        spring_stiffness_corr=spring_stiffness_corr,
        params_dict={},
    )

    # check drift values
    assert g_zero_date == datetime.fromisoformat("2014-04-02")
    assert g_zero == 980890.8077692492
    assert drift == 9.945375034526984e-07

    # check expected variables have been computed
    assert gravi.THEORETIC_GRAVITY_NAME in result_ds
    assert gravi.EOTVOS_CORRECTION_NAME in result_ds
    assert gravi.DRIFTED_GRAVITY_NAME in result_ds
    assert gravi.GRAVITY_NAME in result_ds
    assert gravi.FREE_AIR_ANOMALY_NAME in result_ds

    assert (
        result_ds[gravi.FREE_AIR_ANOMALY_NAME]
        == result_ds[gravi.GRAVITY_NAME] - result_ds[gravi.THEORETIC_GRAVITY_NAME]
    ).all()

    # compare with value got from Caraibes MGD77
    caraibes_value = -14.4
    assert (abs(result_ds[gravi.FREE_AIR_ANOMALY_NAME].mean() - caraibes_value) < 0.01).all()

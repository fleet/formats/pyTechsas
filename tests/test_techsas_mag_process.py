import numpy as np
import pyproj

import tests.techsas_test_utils as utils
import pytechsas.sensor.techsas_mag_process as mag


def test_apply_cable_out():
    # read test dataset
    test_ds = utils.__build_dataset_SALSA_LEG1_mag__()
    lon = test_ds[mag.TECHSAS_LONGITUDE]
    lat = test_ds[mag.TECHSAS_LATITUDE]

    # apply cable out
    cable_out = 342
    lon_shifted, lat_shifted = mag.__apply_cable_out__(lon.data, lat.data, cable_out)

    # check distance between input and output data
    geodesic = pyproj.Geod(ellps="WGS84")
    _, _, distances = geodesic.inv(lon, lat, lon_shifted, lat_shifted)
    distances = distances[~np.isnan(distances)]  # first distances are obviously NaN due to the shift
    # check with tolerance
    assert (abs(distances - cable_out) < 0.1).all()


def test_process_mag():
    test_ds = utils.__build_dataset_SALSA_LEG1_mag__()

    result_ds = mag.process_mag_ds(mag_ds=test_ds)

    # check expected variables have been computed
    assert mag.THEORETIC_MAGNETISM_NAME in result_ds
    assert mag.MAGNETIC_ANOMALY_NAME in result_ds

    # check anomaly
    assert (
        result_ds[mag.MAGNETIC_ANOMALY_NAME] == test_ds[mag.TECHSAS_MAG] - result_ds[mag.THEORETIC_MAGNETISM_NAME]
    ).all()

import numpy as np
from pytechsas.sensor import techsas_sanity_check

# time data with anomalies
test_time_arr = np.array(
    [
        "2003-03-12T12:44:34.476576256",
        "2003-03-12T12:44:40.485696000",  # duplicate
        "2003-03-12T12:44:40.485696000",  # duplicate
        "2003-03-12T12:44:46.486175744",
        "2003-03-12T12:44:52.485791744",
        "2003-03-12T12:44:58.476767744",
        "2003-03-12T12:48:34.327296512",  # interruption
        "2003-03-12T12:48:40.317407744",
        "2003-03-12T12:48:46.417247744",
        "2003-03-12T12:48:52.307999744",
        "2003-03-12T12:48:58.298975744",
        "2003-03-12T12:49:34.300127744",  # interruption
        "2003-03-12T12:49:40.290240000",
        "2003-03-12T12:49:38.290240000",  # leap back
        "2003-03-12T12:49:46.289856000",
        "2003-03-12T12:49:52.280832000",  # duplicate
        "2003-03-12T12:49:52.280832000",  # duplicate
        "2003-03-12T12:49:58.30118400",
    ],
    dtype="datetime64[ns]",
)


def test_duplicate_check():
    anomalies = techsas_sanity_check.__check_duplicates__(test_time_arr)
    assert len(anomalies) == 4
    assert anomalies[0].index == 1
    assert anomalies[0].anomaly == "duplicate"
    assert anomalies[1].index == 2
    assert anomalies[1].anomaly == "duplicate"
    assert anomalies[2].index == 15
    assert anomalies[2].anomaly == "duplicate"
    assert anomalies[3].index == 16
    assert anomalies[3].anomaly == "duplicate"


def test_interruption_check():
    anomalies = techsas_sanity_check.__check_interruptions__(test_time_arr, threshold=np.timedelta64(30, "s"))
    assert len(anomalies) == 2
    assert anomalies[0].index == 5
    assert anomalies[0].anomaly == "interruption"
    assert anomalies[1].index == 10
    assert anomalies[1].anomaly == "interruption"


def test_leaps_back():
    anomalies = techsas_sanity_check.__check_leaps_back__(test_time_arr)
    assert len(anomalies) == 1
    assert anomalies[0].index == 13
    assert anomalies[0].anomaly == "leaps_back"


# position data with anomalies
lat_arr = np.array(
    [
        -9.679902,
        -9.6799017,
        1000,  # invalid
        -9.6799015,
        -9.67990125,
        -9.6799028,
        -9.6799028,
        150,  # invalid
        -9.6799029,
        np.nan,  # invalid
        -9.67990309,
        -9.67990259,
        -9.67990209,
        -9.67990159,
        -9.6799011,
    ]
)

lon_arr = np.array(
    [
        -35.72447571,
        -35.7244762,
        250,  # invalid
        -35.7244766,
        np.nan,  # invalid
        -35.72447909,
        -35.72447869,
        -35.7244782,
        np.nan,  # invalid
        -182,
        -35.7244744,
        -35.7244743,
        -35.72447441,
        -35.72447491,
        -35.7244754,
    ]
)


def test_position_check():
    anomalies = techsas_sanity_check.__check_position__(lat_arr=lat_arr, lon_arr=lon_arr,time_arr=test_time_arr)
    assert len(anomalies) == 5
    assert anomalies[0].index == 2
    assert anomalies[1].index == 4
    assert anomalies[2].index == 7
    assert anomalies[3].index == 8
    assert anomalies[4].index == 9
